-- 
-- ia.lua for ia in /home/payen_a//Project/C++/bomberman
-- 
-- Made by alexandre payen
-- Login   <payen_a@epitech.net>
-- 
-- Started on  Tue May 14 18:57:39 2013 alexandre payen
-- 

-- Touch This File at Your Own Risk !!!!!

function	start(addr)
   chooseStrat(addr);
end

function	chooseStrat(addr)
   if amISafe(addr, 5) == true
   then
      aggressive(addr, 3)
   else
      defensive(addr, 15)
   end
end

function	amISafe(addr, size)
   cCall(addr, "updateMap", size)
   cCall(addr, "updateUnsafe")
   y = arg[1]
   x = arg[2]
   for i=1, #unsafe, 2 do
      if unsafe[i] == x and unsafe[i+1] == y
      then
   	 return false
      end
   end
   return true
end

function	aggressive(addr, length)
   cCall(addr, "calcMap", length, 1)
end

function	defensive(addr, length)
   cCall(addr, "calcMap", length, 3)
end
