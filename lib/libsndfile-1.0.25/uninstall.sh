#!/bin/sh
## uninstall.sh for uninstall in /home/payen_a//Project/C++/bomberman/lib/libsndfile-1.0.25
## 
## Made by alexandre payen
## Login   <payen_a@epitech.net>
## 
## Started on  Thu May 16 00:43:50 2013 alexandre payen
## Last update Thu May 16 00:46:05 2013 alexandre payen
##

make distclean;
rm -f /usr/local/lib/libsndfile.*;