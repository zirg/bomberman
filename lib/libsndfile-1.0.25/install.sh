#!/bin/sh
## install.sh for libsndfile-1.0.25 in /home/payen_a//Project/C++/bomberman/lib
## 
## Made by alexandre payen
## Login   <payen_a@epitech.net>
## 
## Started on  Wed May 15 23:55:57 2013 alexandre payen
## Last update Wed May 15 23:57:28 2013 alexandre payen
##

sh configure && make && make install;