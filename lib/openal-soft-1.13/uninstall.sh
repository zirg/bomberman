#!/bin/sh
## script.sh for install openal in /home/payen_a//Project/C++/bomberman/lib/openal-soft-1.13
## 
## Made by alexandre payen
## Login   <payen_a@epitech.net>
## 
## Started on  Wed May 15 23:31:13 2013 alexandre payen
## Last update Mon Jun  3 17:09:21 2013 alexandre payen
##

cd build && make clean && cd ..;
rm -f /usr/local/lib/libopenal.so*;
rm -rf build/
mkdir build