##
## Makefile for  in /home/grizet_j//malloc
## 
## Made by jean grizet
## Login   <grizet_j@epitech.net>
## 
## Started on  Wed Jan 30 20:40:31 2013 jean grizet
## Last update Sun Jun  9 17:27:34 2013 alexandre payen
## Last update Sun May 26 15:42:37 2013 jean grizet
##

NAME=		bomberman
IA=		ia

SRC_DIR= ./src/
SRC__M=	main.cpp
SRC_AL=	Model.cpp		\
	Option.cpp		\
	GameSound.cpp		\
	Intro.cpp		\
	LetterIntro.cpp		\
	UserInterface.cpp	\
	Box.cpp			\
	Bonus.cpp		\
	Case.cpp		\
	Event.cpp		\
	Thread.cpp		\
	EventQueue.cpp		\
	CondVar.cpp		\
	Mutex.cpp		\
	Consumer.cpp		\
	Bomber.cpp		\
	Pool.cpp		\
	Map.cpp			\
	Bomb.cpp		\
	Explosion.cpp		\
	BagOfBomb.cpp		\
	Camera.cpp		\
	IA.cpp			\
	CaseEditor.cpp		\
	LoopEdit.cpp		\
	MapEditor.cpp		\
	Game.cpp		\
	Viseur.cpp		\
	Global_IA.cpp		\
	MenuCamera.cpp		\
	Menu.cpp		\
	MenuNewGame.cpp		\
	Plan.cpp		\
	ShowNumber.cpp		\
	ShowString.cpp		\
	MenuSettings.cpp	\
	MenuLoadGame.cpp	\
	MenuScores.cpp		\
	MenuHelp.cpp		\
	MenuCredit.cpp		\
	SaveMap.cpp		\
	MenuCreateMap.cpp	\
	MenuQuit.cpp		\
	SaveGame.cpp		\
	LoadGame.cpp		\
	MenuInGame.cpp		\
	Texture.cpp		\
	LinkTexture.cpp		\
	Readdir.cpp

SRCS=    $(foreach srcs, $(SRC_AL), $(SRC_DIR)$(srcs))
OBJ_DIR= ./obj/
OBJ__M=	$(foreach src, $(SRC__M), $(OBJ_DIR)$(src:.cpp=.o))
OBJ_AL=	$(foreach src, $(SRC_AL), $(OBJ_DIR)$(src:.cpp=.o))
OBJ1=	${OBJ__M} ${OBJ_AL}
OBJ2=	${OBJ_IA} ${OBJ_AL}

LIB_DIR=	./lib/
INC_DIR= ./inc/
IFLAGS=  -I$(INC_DIR) -I$(INC_DIR)include/ -I$(INC_DIR)lib/lua-5.2.2/src/

LFLAGS= $(IFLAGS) -L./lib/gdl/ -Wl,--rpath=./lib/gdl/,--rpath=./lib/ -lgdl_gl -lsfml-graphics -lsfml-window -lsfml-system -lsfml-audio -lGL -lGLU -llua -lpthread -ldl
CFLAGS=  -W -Wall -Wextra -Werror
CC=	 g++ -g

all:		$(NAME)

$(NAME):	$(OBJ1)
		$(CC) $(OBJ1) -o $(NAME) $(LFLAGS)

clean:
		rm -f $(OBJ1)
		rm -f $(OBJ2)

fclean:
		rm -f $(OBJ1)
		rm -f $(OBJ2)
		rm -f $(NAME)
		rm -f $(IA)

re:		fclean all

ins:
			cd ${LIB_DIR} && make install && cd ..

unins:
			cd ${LIB_DIR} && make uninstall && cd ..

install:		ins all

uninstall:		unins fclean

$(OBJ_DIR)main.o:	$(SRC_DIR)main.cpp
		 	$(CC) -c $(SRC_DIR)main.cpp $(CFLAGS) \
			$(IFLAGS) -o $(OBJ_DIR)main.o $(LFLAGS)
$(OBJ_DIR)Intro.o:	$(SRC_DIR)Intro.cpp
			$(CC) -c $(SRC_DIR)Intro.cpp $(CFLAGS) \
			$(IFLAGS) -o $(OBJ_DIR)Intro.o $(LFLAGS)
$(OBJ_DIR)LetterIntro.o:	$(SRC_DIR)LetterIntro.cpp
			$(CC) -c $(SRC_DIR)LetterIntro.cpp $(CFLAGS) \
			$(IFLAGS) -o $(OBJ_DIR)LetterIntro.o $(LFLAGS)
$(OBJ_DIR)Game.o:	$(SRC_DIR)Game.cpp
		 	$(CC) -c $(SRC_DIR)Game.cpp $(CFLAGS) \
			$(IFLAGS) -o $(OBJ_DIR)Game.o $(LFLAGS)
$(OBJ_DIR)MenuInGame.o:	$(SRC_DIR)MenuInGame.cpp
		 	$(CC) -c $(SRC_DIR)MenuInGame.cpp $(CFLAGS) \
			$(IFLAGS) -o $(OBJ_DIR)MenuInGame.o $(LFLAGS)
$(OBJ_DIR)Readdir.o:	$(SRC_DIR)Readdir.cpp
		 	$(CC) -c $(SRC_DIR)Readdir.cpp $(CFLAGS) \
			$(IFLAGS) -o $(OBJ_DIR)Readdir.o $(LFLAGS)
$(OBJ_DIR)Texture.o:	$(SRC_DIR)Texture.cpp
		 	$(CC) -c $(SRC_DIR)Texture.cpp $(CFLAGS) \
			$(IFLAGS) -o $(OBJ_DIR)Texture.o $(LFLAGS)
$(OBJ_DIR)LinkTexture.o:	$(SRC_DIR)LinkTexture.cpp
		 	$(CC) -c $(SRC_DIR)LinkTexture.cpp $(CFLAGS) \
			$(IFLAGS) -o $(OBJ_DIR)LinkTexture.o $(LFLAGS)
$(OBJ_DIR)main_IA.o:	$(SRC_DIR)main_IA.cpp
		 	$(CC) -c $(SRC_DIR)main_IA.cpp $(CFLAGS) \
			$(IFLAGS) -o $(OBJ_DIR)main_IA.o $(LFLAGS)
$(OBJ_DIR)Mutex.o:	$(INC_DIR)IMutex.hh $(INC_DIR)Mutex.hh \
			$(SRC_DIR)Mutex.cpp
			$(CC) -c $(SRC_DIR)Mutex.cpp $(CXXFLAGS) $(LFLAGS) \
			$(IFLAGS) -o $(OBJ_DIR)Mutex.o
$(OBJ_DIR)Thread.o:	$(INC_DIR)IThread.hh $(INC_DIR)Thread.hh \
			$(SRC_DIR)Thread.cpp
			$(CC) -c $(SRC_DIR)Thread.cpp $(CXXFLAGS) $(LFLAGS) \
			$(IFLAGS) -o $(OBJ_DIR)Thread.o
$(OBJ_DIR)CondVar.o:	$(INC_DIR)IMutex.hh $(INC_DIR)Mutex.hh \
			$(INC_DIR)ICondVar.hh $(INC_DIR)CondVar.hh \
			$(SRC_DIR)CondVar.cpp
			$(CC) -c $(SRC_DIR)CondVar.cpp $(CXXFLAGS) $(LFLAGS) \
			$(IFLAGS) -o $(OBJ_DIR)CondVar.o
$(OBJ_DIR)EventQueue.o:	$(INC_DIR)IMutex.hh $(INC_DIR)ICondVar.hh \
			$(INC_DIR)IEventQueue.hh $(INC_DIR)EventQueue.hh \
			$(SRC_DIR)EventQueue.cpp
			$(CC) -c $(SRC_DIR)EventQueue.cpp $(CXXFLAGS) \
			$(LFLAGS) $(IFLAGS) -o $(OBJ_DIR)EventQueue.o
$(OBJ_DIR)Pool.o:	$(INC_DIR)IEventQueue.hh $(INC_DIR)EventQueue.hh \
			$(INC_DIR)IThread.hh $(INC_DIR)Thread.hh \
			$(INC_DIR)IPool.hh $(INC_DIR)Pool.hh \
			$(SRC_DIR)EventQueue.cpp $(SRC_DIR)Thread.cpp \
			$(SRC_DIR)Pool.cpp
			$(CC) -c $(SRC_DIR)Pool.cpp $(CXXFLAGS) \
			$(LFLAGS) $(IFLAGS) -o $(OBJ_DIR)Pool.o
$(OBJ_DIR)Bomber.o:	$(SRC_DIR)Bomber.cpp
		 	$(CC) -c $(SRC_DIR)Bomber.cpp $(CFLAGS) \
			$(IFLAGS) -o $(OBJ_DIR)Bomber.o $(LFLAGS)
$(OBJ_DIR)Bomb.o:	$(SRC_DIR)Bomb.cpp
		 	$(CC) -c $(SRC_DIR)Bomb.cpp $(CFLAGS) \
			$(IFLAGS) -o $(OBJ_DIR)Bomb.o $(LFLAGS)
$(OBJ_DIR)BagOfBomb.o:	$(SRC_DIR)BagOfBomb.cpp
		 	$(CC) -c $(SRC_DIR)BagOfBomb.cpp $(CFLAGS) \
			$(IFLAGS) -o $(OBJ_DIR)BagOfBomb.o $(LFLAGS)
$(OBJ_DIR)Consumer.o:	$(SRC_DIR)Consumer.cpp
		 	$(CC) -c $(SRC_DIR)Consumer.cpp $(CFLAGS) \
			$(IFLAGS) -o $(OBJ_DIR)Consumer.o $(LFLAGS)
$(OBJ_DIR)Box.o:	$(SRC_DIR)Box.cpp
		 	$(CC) -c $(SRC_DIR)Box.cpp $(CFLAGS) \
			$(IFLAGS) -o $(OBJ_DIR)Box.o $(LFLAGS)
$(OBJ_DIR)Bonus.o:	$(SRC_DIR)Bonus.cpp
		 	$(CC) -c $(SRC_DIR)Bonus.cpp $(CFLAGS) \
			$(IFLAGS) -o $(OBJ_DIR)Bonus.o $(LFLAGS)
$(OBJ_DIR)Explosion.o:	$(SRC_DIR)Explosion.cpp
		 	$(CC) -c $(SRC_DIR)Explosion.cpp $(CFLAGS) \
			$(IFLAGS) -o $(OBJ_DIR)Explosion.o $(LFLAGS)
$(OBJ_DIR)Option.o:	$(SRC_DIR)Option.cpp
		 	$(CC) -c $(SRC_DIR)Option.cpp $(CFLAGS) \
			$(IFLAGS) -o $(OBJ_DIR)Option.o $(LFLAGS)
$(OBJ_DIR)Case.o:	$(SRC_DIR)Case.cpp
		 	$(CC) -c $(SRC_DIR)Case.cpp $(CFLAGS) \
			$(IFLAGS) -o $(OBJ_DIR)Case.o $(LFLAGS)
$(OBJ_DIR)Map.o:	$(SRC_DIR)Map.cpp
		 	$(CC) -c $(SRC_DIR)Map.cpp $(CFLAGS) \
			$(IFLAGS) -o $(OBJ_DIR)Map.o $(LFLAGS)
$(OBJ_DIR)Event.o:	$(SRC_DIR)Event.cpp
		 	$(CC) -c $(SRC_DIR)Event.cpp $(CFLAGS) \
			$(IFLAGS) -o $(OBJ_DIR)Event.o $(LFLAGS)
$(OBJ_DIR)Model.o:	$(SRC_DIR)Model.cpp
		 	$(CC) -c $(SRC_DIR)Model.cpp $(CFLAGS) \
			$(IFLAGS) -o $(OBJ_DIR)Model.o $(LFLAGS)
$(OBJ_DIR)GameSound.o:	$(SRC_DIR)GameSound.cpp
		 	$(CC) -c $(SRC_DIR)GameSound.cpp $(CFLAGS) \
			$(IFLAGS) -o $(OBJ_DIR)GameSound.o $(LFLAGS)
$(OBJ_DIR)Camera.o:	$(SRC_DIR)Camera.cpp
		 	$(CC) -c $(SRC_DIR)Camera.cpp $(CFLAGS) \
			$(IFLAGS) -o $(OBJ_DIR)Camera.o $(LFLAGS)
$(OBJ_DIR)UserInterface.o:	$(SRC_DIR)UserInterface.cpp
		 	$(CC) -c $(SRC_DIR)UserInterface.cpp $(CFLAGS) \
			$(IFLAGS) -o $(OBJ_DIR)UserInterface.o $(LFLAGS)
$(OBJ_DIR)IA.o:		$(SRC_DIR)IA.cpp
		 	$(CC) -c $(SRC_DIR)IA.cpp $(CFLAGS) \
			$(IFLAGS) -o $(OBJ_DIR)IA.o $(LFLAGS)
$(OBJ_DIR)MapEditor.o:		$(SRC_DIR)MapEditor.cpp
		 	$(CC) -c $(SRC_DIR)MapEditor.cpp $(CFLAGS) \
			$(IFLAGS) -o $(OBJ_DIR)MapEditor.o $(LFLAGS)

$(OBJ_DIR)Viseur.o:	$(SRC_DIR)Viseur.cpp
		 	$(CC) -c $(SRC_DIR)Viseur.cpp $(CFLAGS) \
			$(IFLAGS) -o $(OBJ_DIR)Viseur.o $(LFLAGS)

$(OBJ_DIR)LoopEdit.o:	$(SRC_DIR)LoopEdit.cpp
		 	$(CC) -c $(SRC_DIR)LoopEdit.cpp $(CFLAGS) \
			$(IFLAGS) -o $(OBJ_DIR)LoopEdit.o $(LFLAGS)

$(OBJ_DIR)CaseEditor.o:	$(SRC_DIR)CaseEditor.cpp
		 	$(CC) -c $(SRC_DIR)CaseEditor.cpp $(CFLAGS) \
			$(IFLAGS) -o $(OBJ_DIR)CaseEditor.o $(LFLAGS)

$(OBJ_DIR)Global_IA.o:	$(SRC_DIR)Global_IA.cpp
		 	$(CC) -c $(SRC_DIR)Global_IA.cpp $(CFLAGS) \
			$(IFLAGS) -o $(OBJ_DIR)Global_IA.o $(LFLAGS)

$(OBJ_DIR)Menu.o:	$(SRC_DIR)Menu.cpp
			$(CC) -c $(SRC_DIR)Menu.cpp $(CFLAGS) \
			$(IFLAGS) -o $(OBJ_DIR)Menu.o $(LFLAGS)

$(OBJ_DIR)MenuCamera.o: $(SRC_DIR)MenuCamera.cpp
			$(CC) -c $(SRC_DIR)MenuCamera.cpp $(CFLAGS) \
			$(IFLAGS) -o $(OBJ_DIR)MenuCamera.o $(LFLAGS)

$(OBJ_DIR)MenuNewGame.o:	$(SRC_DIR)MenuNewGame.cpp
			$(CC) -c $(SRC_DIR)MenuNewGame.cpp $(CFLAGS) \
			$(IFLAGS) -o $(OBJ_DIR)MenuNewGame.o $(LFLAGS)
$(OBJ_DIR)Plan.o:	$(SRC_DIR)Plan.cpp
			$(CC) -c $(SRC_DIR)Plan.cpp $(CFLAGS) \
			$(IFLAGS) -o $(OBJ_DIR)Plan.o $(LFLAGS)

$(OBJ_DIR)ShowNumber.o:	$(SRC_DIR)ShowNumber.cpp
			$(CC) -c $(SRC_DIR)ShowNumber.cpp $(CFLAGS) \
			$(IFLAGS) -o $(OBJ_DIR)ShowNumber.o $(LFLAGS)

$(OBJ_DIR)ShowString.o:$(SRC_DIR)ShowString.cpp
			$(CC) -c $(SRC_DIR)ShowString.cpp $(CFLAGS) \
			$(IFLAGS) -o $(OBJ_DIR)ShowString.o $(LFLAGS)

$(OBJ_DIR)MenuSettings.o:$(SRC_DIR)MenuSettings.cpp
			$(CC) -c $(SRC_DIR)MenuSettings.cpp $(CFLAGS) \
			$(IFLAGS) -o $(OBJ_DIR)MenuSettings.o $(LFLAGS)

$(OBJ_DIR)MenuLoadGame.o:$(SRC_DIR)MenuLoadGame.cpp
			$(CC) -c $(SRC_DIR)MenuLoadGame.cpp $(CFLAGS) \
			$(IFLAGS) -o $(OBJ_DIR)MenuLoadGame.o $(LFLAGS)

$(OBJ_DIR)MenuScores.o:	$(SRC_DIR)MenuScores.cpp
			$(CC) -c $(SRC_DIR)MenuScores.cpp $(CFLAGS) \
			$(IFLAGS) -o $(OBJ_DIR)MenuScores.o $(LFLAGS)

$(OBJ_DIR)MenuHelp.o:	$(SRC_DIR)MenuHelp.cpp
			$(CC) -c $(SRC_DIR)MenuHelp.cpp $(CFLAGS) \
			$(IFLAGS) -o $(OBJ_DIR)MenuHelp.o $(LFLAGS)

$(OBJ_DIR)MenuCredit.o:	$(SRC_DIR)MenuCredit.cpp
			$(CC) -c $(SRC_DIR)MenuCredit.cpp $(CFLAGS) \
			$(IFLAGS) -o $(OBJ_DIR)MenuCredit.o $(LFLAGS)
$(OBJ_DIR)MenuCreateMap.o:$(SRC_DIR)MenuCreateMap.cpp
			$(CC) -c $(SRC_DIR)MenuCreateMap.cpp $(CFLAGS) \
			$(IFLAGS) -o $(OBJ_DIR)MenuCreateMap.o $(LFLAGS)

$(OBJ_DIR)MenuQuit.o:	$(SRC_DIR)MenuQuit.cpp
			$(CC) -c $(SRC_DIR)MenuQuit.cpp $(CFLAGS) \
			$(IFLAGS) -o $(OBJ_DIR)MenuQuit.o $(LFLAGS)


$(OBJ_DIR)SaveMap.o:	$(SRC_DIR)SaveMap.cpp
			$(CC) -c $(SRC_DIR)SaveMap.cpp $(CFLAGS) \
			$(IFLAGS) -o $(OBJ_DIR)SaveMap.o $(LFLAGS)
$(OBJ_DIR)SaveGame.o:	$(SRC_DIR)SaveGame.cpp
			$(CC) -c $(SRC_DIR)SaveGame.cpp $(CFLAGS) \
			$(IFLAGS) -o $(OBJ_DIR)SaveGame.o $(LFLAGS)
$(OBJ_DIR)LoadGame.o:	$(SRC_DIR)LoadGame.cpp
			$(CC) -c $(SRC_DIR)LoadGame.cpp $(CFLAGS) \
			$(IFLAGS) -o $(OBJ_DIR)LoadGame.o $(LFLAGS)
