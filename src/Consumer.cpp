//
// Consumer.cpp for  in /home/grizet_j//C++/plazza/src
// 
// Made by jean grizet
// Login   <grizet_j@epitech.net>
// 
// Started on  Mon May 13 20:02:57 2013 jean grizet
// Last update Sat Jun  8 20:51:39 2013 jean grizet
//

#include "Consumer.hh"
#include "Box.hh"
#include <cstdlib>

Consumer::Consumer(IEventQueue<IEvent>& t, IEventQueue<IEvent>& d)  :
  _task(t), _done(d) {}

Consumer::~Consumer() {}

void	*Consumer::run(void *v)
{
  IEvent	*event;

  while (42)
    {
      event = _task.pop();
      if (event != NULL)
	{
	  event->run();
	  event = NULL;
	}
      usleep(150);
    }
  return (v);
}
