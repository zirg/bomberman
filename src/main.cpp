//
// main.cpp for  in /home/grizet_j//C++/bomberman/src
// 
// Made by jean grizet
// Login   <grizet_j@epitech.net>
// 
// Started on  Mon Apr 29 11:54:22 2013 jean grizet
// Last update Sat Jun  8 17:20:13 2013 jean grizet
//

#include "Game.hh"

int	main()
{
  Game	game;

  game.run();
  return (0);
}
