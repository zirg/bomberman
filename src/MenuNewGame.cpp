//
// MenuNewGame.cpp for MenuNewGame in /home/lecoq_j//Tech2/ProjetC++/Bomberman/src/menu
//
// Made by jean-charles lecoq
// Login   <lecoq_j@epitech.net>
// 
// Started on  Wed May  8 22:51:46 2013 jean-charles lecoq
// Last update Sun Jun  9 17:32:00 2013 jean-charles lecoq
//

#include	"MenuNewGame.hh"
#include	<iostream>

MenuNewGame::MenuNewGame(Option& _opt, Texture& texture,
			 std::vector<std::string>& listMap,
			 std::vector<std::string>::iterator& posMap)
  :_texture(texture), _option(_opt), _listMap(listMap), _posListMap(posMap)
{ }

MenuNewGame::~MenuNewGame()
{ }

void	MenuNewGame::initialize()
{  
  typeGame_ = false;
  timeGame_ = 1;
  onePlayer_ = true;
  numberOfIa_ = 0;
  levelIa_ = 0;
  heightMap_ = 15;
  widthMap_ = 15;
  _option.setSizeY(heightMap_);
  _option.setSizeX(widthMap_);
  _option.setPlayer(1);
  _option.setIA(numberOfIa_);
  _option.setMode(false);
  _option.setTime(timeGame_);

  posInMenu_ = 0;  
  time_ = 0.0;

  _fctNg[0] = &MenuNewGame::typeGame;
  _fctNg[1] = &MenuNewGame::timeGame;
  _fctNg[2] = &MenuNewGame::players;
  _fctNg[3] = &MenuNewGame::nbIa;
  _fctNg[4] = &MenuNewGame::levelIa;
  _fctNg[5] = &MenuNewGame::typeMap;
  _fctNg[6] = &MenuNewGame::height;
  _fctNg[7] = &MenuNewGame::width;

  showGameMode_ = new Plan(_texture);
  showTimeGame_ = new ShowNumber(_texture);
  showPlayer_ = new Plan(_texture);
  showNbIa_ = new ShowNumber(_texture);
  showLevelIa_ = new Plan(_texture);
  showTypeMap_ = new ShowString(_texture);
  showHeight_ = new ShowNumber(_texture);
  showWidth_ = new ShowNumber(_texture);

  showGameMode_->initialize(-235, 250, 150, 280, -3480, -3480);
  showTimeGame_->initialize(2, 85, 100, -3480, 60, 60, 0, 1);  
  showPlayer_->initialize(0, 410, -30, 110, -3480, -3480);  
  showNbIa_->initialize(2, 360, -80, -3480, 60, 60, 0, 1);
  showLevelIa_->initialize(250, 580, -210, -80, -3480, -3480);
  showTypeMap_->initialize(9, 140, -255, -3480, 50, 50, 0, 1);
  showHeight_->initialize(3, 520, -345, -3480, 60, 60, 0, 1);  
  showWidth_->initialize(3, 610, -430, -3480, 60, 60, 0, 1);

  showGameMode_->update(Texture::SURVIVAL);
  showTimeGame_->update(timeGame_);
  showPlayer_->update(Texture::ONEPLAYER);
  showNbIa_->update(numberOfIa_);
  showLevelIa_->update(Texture::IANORMAL);
  showTypeMap_->update((*_posListMap));
  showHeight_->update(heightMap_);
  showWidth_->update(widthMap_);

  /* coordonne title*/
  x1Title_ = -700;
  x2Title_ = 700;
  y1Title_ = 100.5;
  y2Title_ = 430.5;
  z1Title_ = -1280;
  z2Title_ = -1280;
  /* coordonne background*/
  x1_ = -990;
  x2_ = 990;
  y1_ = -600;
  y2_ = 600;
  z1_ = -3480;
  z2_ = -3480;
}

void	MenuNewGame::upDown(int di, gdl::GameClock const& gameClock)
{
  posInMenu_ += 1 * di;
  if (posInMenu_ <= -1)
    posInMenu_ = 7;
  if (posInMenu_ >= 8)
    posInMenu_ = 0;
  if (typeGame_ == 0)
    {
      if (posInMenu_ == 1)
	posInMenu_ += 1 * di;
    }

  if (_posListMap != _listMap.begin())
    {
      if (posInMenu_ == 6)
	posInMenu_ += 1 * di;
      if (posInMenu_ == 7)
	{
	  posInMenu_ += 1 * di;
	  if (posInMenu_ == 6)
	    posInMenu_ += 1 * di;
	}
    }
  if (posInMenu_ <= -1)
    posInMenu_ = 7;
  if (posInMenu_ >= 8)
    posInMenu_ = 0;
  time_ = gameClock.getTotalGameTime() + 0.15;
}

void		MenuNewGame::typeGame(int dir)
{
  (void)dir;
  if (typeGame_ == false)
    {
      typeGame_ = true;
      _option.setMode(typeGame_);
      showGameMode_->update(Texture::TIMEATTACK);
    }
  else
    {
      typeGame_ = false;
      _option.setMode(typeGame_);
      showGameMode_->update(Texture::SURVIVAL);
    }
}

void		MenuNewGame::timeGame(int dir)
{
  timeGame_ += 1 * dir;
  if (timeGame_ == 16)
    timeGame_ = 1;
  else
    if (timeGame_ == 0)
      timeGame_ = 15;
  _option.setTime(timeGame_);
  showTimeGame_->update(timeGame_);
}

void		MenuNewGame::players(int dir)
{
  (void)dir;
  if (onePlayer_ == true)
    {
      onePlayer_ = false;
      _option.setPlayer(2);
      showPlayer_->update(Texture::TWOPLAYERS);
    }
  else
    {
      onePlayer_ = true;
      _option.setPlayer(1);
      showPlayer_->update(Texture::ONEPLAYER);
    }
}

void		MenuNewGame::nbIa(int dir)
{
  numberOfIa_ += 1 * dir;
  if (numberOfIa_ == 21)
    numberOfIa_ = 0;
  else
    if (numberOfIa_ == -1)
      numberOfIa_ = 20;
  _option.setIA(numberOfIa_);
  showNbIa_->update(numberOfIa_);
}

void		MenuNewGame::levelIa(int dir)
{
  (void)dir;
}

void		MenuNewGame::typeMap(int dir)
{
  if (_listMap.size() == 1)
    return;
  if (dir == -1)
    {
      if (_posListMap == _listMap.begin())
        _posListMap = _listMap.end();
      --_posListMap;
    }
  else
    {
      ++_posListMap;
      if (_posListMap == _listMap.end())
	_posListMap = _listMap.begin();
    }
  _option.setTypeMap((*_posListMap));
  showTypeMap_->update((*_posListMap));
}

void		MenuNewGame::height(int dir)
{
  heightMap_ += 1 * dir;
  if (heightMap_ == 121)
    heightMap_ = 15;
  else
    if (heightMap_ == 14)
      heightMap_ = 120;
  _option.setSizeY(heightMap_);
  showHeight_->update(heightMap_);
}

void		MenuNewGame::width(int dir)
{
  widthMap_ += 1 * dir;
  if (widthMap_ == 121)
    widthMap_ = 15;
  else
    if (widthMap_ == 14)
      widthMap_ = 120;
  _option.setSizeX(widthMap_);
  showWidth_->update(widthMap_);
}


void	MenuNewGame::rightLeft(int di, gdl::GameClock const& gameClock)
{
  (this->*_fctNg[posInMenu_])(di);
  time_ = gameClock.getTotalGameTime() + 0.15;
}

void	MenuNewGame::update(gdl::GameClock const & gameClock, gdl::Input &input)
{
  if (time_ > gameClock.getTotalGameTime())
    return;
  if (input.isKeyDown(gdl::Keys::Down) == true)
    upDown(1, gameClock);
  if (input.isKeyDown(gdl::Keys::Up) == true)
    upDown(-1, gameClock);
  if (input.isKeyDown(gdl::Keys::Right) == true)
    rightLeft(1, gameClock);
  if (input.isKeyDown(gdl::Keys::Left) == true)
    rightLeft(-1, gameClock);
}

void	MenuNewGame::draw()
{
  glPushMatrix();
  glLoadIdentity();
  glEnable(GL_BLEND); 
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
  switch (posInMenu_)
    {
    case 0:
      {
	_texture.bindTexture(Texture::NGGM);
	break;
      }
    case 1:
      {
	_texture.bindTexture(Texture::NGGT);
	break;
      }
    case 2:
      {
	_texture.bindTexture(Texture::NGPL);
	break;
      }
    case 3:
      {
	_texture.bindTexture(Texture::NGNBIA);
	break;
      }
    case 4:
      {
	_texture.bindTexture(Texture::NGLVLIA);
	break;
      }
    case 5:
      {
	_texture.bindTexture(Texture::NGTM);
	break;
      }
    case 6:
      {
	_texture.bindTexture(Texture::NGHE);
	break;
      }
    case 7:
      {
	_texture.bindTexture(Texture::NGWI);
	break;
      }
    }
  glBegin(GL_QUADS);
  glTexCoord2f(0.0, 0.0);
  glVertex3f(x1_, y2_, z1_);
  glTexCoord2f(0.0, 1.0f);
  glVertex3f(x1_, y1_, z1_);
  glTexCoord2f(1.0f, 1.0f);
  glVertex3f(x2_, y1_, z2_);
  glTexCoord2f(1.0f, 0.0);
  glVertex3f(x2_, y2_, z2_);
  glEnd();
  glDisable(GL_BLEND);
  glDisable(GL_TEXTURE_2D);
  glPopMatrix();
  showGameMode_->draw();
  showPlayer_->draw();
  showLevelIa_->draw();
  showNbIa_->draw();
  showTypeMap_->draw();
  /* draw element of new game */
  if (typeGame_ == 1)
    showTimeGame_->draw();
  if (_posListMap == _listMap.begin())
    {
      showHeight_->draw();
      showWidth_->draw(); 
    }
}

void    MenuNewGame::zoomTitle(float x1, float x2, float y1, float y2, float z1, float z2)
{
  x1Title_ += x1;
  x2Title_ += x2;
  y1Title_ += y1;
  y2Title_ += y2;
  z1Title_ += z1;
  z2Title_ += z2;
}


void    MenuNewGame::drawTitle()
{
  glPushMatrix();
  glLoadIdentity();
  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
  _texture.bindTexture(Texture::TITLENG);
  glBegin(GL_QUADS);
  glTexCoord2f(0.0, 0.0);
  glVertex3f(x1Title_, y2Title_, z1Title_);
  glTexCoord2f(0.0, 1.0f);
  glVertex3f(x1Title_, y1Title_, z1Title_);
  glTexCoord2f(1.0f, 1.0f);
  glVertex3f(x2Title_, y1Title_, z2Title_);
  glTexCoord2f(1.0f, 0.0);
  glVertex3f(x2Title_, y2Title_, z2Title_);
  glEnd();
  glDisable(GL_BLEND);
  glDisable(GL_TEXTURE_2D);
}


std::vector<std::string>::iterator&     MenuNewGame::getPosListMap() const
{
  return (_posListMap);
}

std::vector<std::string>&               MenuNewGame::getListMap() const
{
  return (this->_listMap);
}
