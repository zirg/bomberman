//
// IA.cpp for  in /home/payen_a//Project/C++/bomberman/src
// 
// Made by alexandre payen
// Login   <payen_a@epitech.net>
// 
// Started on  Wed May 22 10:53:21 2013 alexandre payen
// Last update Sun Jun  9 17:35:18 2013 alexandre payen
//

#include "IA.hh"

extern "C"
{
#include "lua.h"
#include "lualib.h"
#include "lauxlib.h"
}

#include <sstream>

pthread_mutex_t                         mIA = PTHREAD_MUTEX_INITIALIZER;

int                                     cCall(lua_State *state)
{
  pthread_mutex_lock(&mIA);
  IA                                    *ia;
  const char*                           func;

  ia = (IA*)strtoul(lua_tostring(state, 1), NULL, 16);
  func = lua_tostring(state, 2);
  pthread_mutex_unlock(&mIA);
  ia->assignThis(state, func);
  return (0);
}

IA::IA(int play, int posX, int posY,
       Map *map,
       Global_IA *Glob,
       std::vector<Model::Bomberman *> *player,
       GameSound &gs,
       gdl::GameClock const *gameClock)
  : Bomberman(play, posX, posY, map, gs),
    _map(map),
    _glob(Glob),
    _play(player),
    _gameClock(gameClock)
{
  _count = 150;
  _l = luaL_newstate();
  luaL_openlibs(_l);  
  _lua_status = luaL_loadfile(_l, "IA/ia.lua");
  lua_register(_l, "cCall", cCall);
  lua_pcall(_l, 0, 0, 0);
  _tabMap = NULL;
  _sizeTab = 0;
  _alea = new std::vector<int>[8];
  int           f = 0;
  while (f < 8)
    {
      _alea->push_back(0);
      f++;
    }
  _way = new std::stack<int>();
  _best = new std::stack<int>;
  _best->push(NIL);
  _escape = -1;
  _escapeB = -1;
  _j = 0;
  _dir = NIL;
}

IA::~IA()
{
  delete(_way);
  delete(_best);
  delete(_alea);
  for (int i = 0; i < ((_sizeTab * 2)); i++)
    delete [] _tabMap[i];
  delete [] _tabMap;
  lua_close(_l);
}

void                                    IA::initTab()
{
  _tab["updateUnsafe"] = &IA::updateUnsafe;
  _tab["updateMap"] = &IA::updateMap;
  _tab["calcMap"] = &IA::calcMap;
}

void                                    IA::assignThis(lua_State *state, const char *func)
{
  ((*this).* _tab[func])(state);
}

void                                    IA::allocTab(int x)
{
  _tabMap = new int*[(x * 2) + 1];
  for (int i = 0; i < ((x * 2) + 1); i++)
    _tabMap[i] = new int[(x * 2) + 1];
}

void                                    IA::updateMap(lua_State *state)
{
  int                                   size = lua_tointeger(state, 3);
  int                                   x = 0;
  int                                   y;

  if (_sizeTab == 0 || _tabMap == NULL)
    {
      allocTab(size);
      _sizeTab = size;
    }
  while (x < ((_sizeTab * 2) + 1))
    {
      y = 0;
      while (y < ((_sizeTab * 2) + 1))
        {
          _tabMap[x][y] = -43;
          y++;
        }
      x++;
    }
  _glob->getRiskMap(this->getPosY(), this->getPosX(), _sizeTab, &_tabMap);
  lua_newtable(state);
  lua_pushnumber(state, 1);
  lua_pushinteger(state, this->getPosX());
  lua_rawset(state, -3);
  lua_pushnumber(state, 2);
  lua_pushinteger(state, this->getPosY());
  lua_rawset(state, -3);
  lua_setglobal(state, "arg");
}

void                                    IA::updateUnsafe(lua_State *state)
{
  int                                   x = 0;
  int                                   y;
  int                                   j = 1;

  lua_newtable(state);
  while (x < (_sizeTab * 2) + 1)
    {
      y = 0;
      while (y < (_sizeTab * 2) + 1)
        {
          if (_tabMap[x][y] == -42)
            {
              lua_pushnumber(state, j);
              lua_pushinteger(state, (this->getPosY() + y - _sizeTab));
              lua_rawset(state, -3);
              lua_pushnumber(state, j+1);
              lua_pushinteger(state, (this->getPosX() + x - _sizeTab));
              lua_rawset(state, -3);
              j = j + 2;
            }
          y++;
        }
      x++;
    }
  lua_setglobal(state, "unsafe");
}

void					IA::setAlea(int x, int y)
{
  (*_alea)[0] = x;
  (*_alea)[2] = x;
  (*_alea)[4] = x;
  (*_alea)[6] = x;
  (*_alea)[1] = y;
  (*_alea)[3] = y;
  (*_alea)[5] = y;
  (*_alea)[7] = y;
}

void                                    IA::becomeRand(int x, int y)
{
  int   s;
  int   c;
  int   cTmp;
  int   sTmp;
  
  setAlea(x, y);
  c = static_cast<int>(_gameClock->getTotalGameTime()) % 2;
  s = (((static_cast<int>(_gameClock->getTotalGameTime() * 10) % 2) == 0) ? 1 : -1);
  (*_alea)[c] += s;
  cTmp = (((static_cast<int>(_gameClock->getTotalGameTime() * 100) % 3) != c) ? !c : c);
  sTmp = (((static_cast<int>(_gameClock->getTotalGameTime() * 1000) % 3) != s) ? -s : s);
  (*_alea)[2+cTmp] += sTmp;
  if (sTmp == s)
    {
      s = -s;
      sTmp = -sTmp;
    }
  else
    {
      s = (((static_cast<int>(_gameClock->getTotalGameTime() * 10000) % 2) == 0) ? 1 : -1);
      sTmp = -s;
    }
  if (c == cTmp)
    {
      c = !s;
      cTmp = !sTmp;
    }
  else
    {
      c = static_cast<int>(_gameClock->getTotalGameTime() * 100000) % 2;
      cTmp = !c;
    }
  (*_alea)[4+c] += s;
  (*_alea)[6+cTmp] += sTmp;
}

int					IA::pushWay(int x, int y, int xTmp, int yTmp)
{
  _way->push(((xTmp != x) ? ((xTmp > x) ? RIGHT : LEFT) :
	      ((yTmp > y) ? DOWN : UP)));  
  return (1);
}

int					IA::verifRecurs(int u, int flag)
{
  if (u == -42 && flag != 3)
    return (0);
  if (u < 0 && u != -42  && u != -43 && flag == 3)
    return (1);
  if (u == getPlayer())
    return (0);
  if (u > 2 && u < 6 && flag == 2)
    return (1);
  if (flag == 4 && u == 2)
    return (1);
  if (u > 7 && u < 21 && flag == 1)
    return (1);
  return (0);
}

int                                     IA::recurs(int max, int acc, int x, int y, int flag)
{
  int                                   u;
  std::vector<int>                      tab;
  int                                   escapeTmp;

  if ((x < 0 || x > ((_sizeTab * 2) + 1)) ||
      (y < 0 || y > ((_sizeTab * 2) + 1)) ||
      (-max == acc))
    return ((flag == 3) ? 1 : 0);
  u = _tabMap[x][y];
  if (verifRecurs(u, flag) == 1)
    return (1);
  if ((u == 0 || u < acc) && (u != -43 && (u != -42 || flag == 3)))
    {
      if (u == -42)
        _escape++;
      _tabMap[x][y] = acc;
      becomeRand(x, y);
      tab = (*_alea);
      escapeTmp = _escape;
      if (recurs(max, (acc -1), tab[0], tab[1], flag) == 1)
	return (pushWay(x, y, tab[0], tab[1]));
      _escape = escapeTmp;
      if (recurs(max, (acc -1), tab[2], tab[3], flag) == 1)
	return (pushWay(x, y, tab[2], tab[3]));
      _escape = escapeTmp;
      if (recurs(max, (acc -1), tab[4], tab[5], flag) == 1)
	return (pushWay(x, y, tab[4], tab[5]));
      _escape = escapeTmp;
      if (recurs(max, (acc -1), tab[6], tab[7], flag) == 1)
	return (pushWay(x, y, tab[6], tab[7]));
      _escape = escapeTmp;
    }
  if (flag == 3 && (u == 0 || u == 3 || u == 4 || u == 5))
    return (1);
  return (0);
}

int                                     IA::haveBetterStrat(int flag) {
  int           x, y = 0;

  if (flag == 1)
    {
      while (y != ((_sizeTab * 2) + 1)) {
	x = 0;
	while (x != ((_sizeTab * 2) + 1))
	  if (_tabMap[y][x] != getPlayer() && _tabMap[y][x] > 7 && _tabMap[y][x] < 21)
	    return (1);
	  else
	    x++;
	y++;
      }
      return (2);
    }
  return (flag);
}

int					IA::canMoove(){
  int					x, y = 0;
  int					Cx, Cy;

  while (y != ((_sizeTab * 2) + 1)) {
    x = 0;
    while (x != ((_sizeTab * 2) + 1)) {
      if (_tabMap[y][x] == getPlayer())
	break;
      x++;
    }
    if (x != _sizeTab * 2 + 1)
      break;
    y++;
  }
  Cx = x;
  Cy = y;
  y = 0;
  while (y != ((_sizeTab * 2) + 1)) {
    x = 0;
    while (x != ((_sizeTab * 2) + 1) && Cy != y)
      {
	if (Cx != x && _tabMap[y][x] == 0 && (y + 1 == y || y - 1 == y || x + 1 == x || x - 1 == x))
	  return (1);
	else
	  x++;
      }
    y++;
  }
  return (0);
}

int					IA::verifLoop(int flag, int x, int y, int xTmp, int yTmp)
{
  pushWay(x, y, xTmp, yTmp);
  if ((flag == 3 && _way->size() > _best->size()) ||
      ((flag != 3) && (_best->size() > _way->size() || _best->size() == 0)) ||
      (_best->empty()))
    {
      if ((_escape <= _escapeB || _escapeB == -1) || flag != 3)
	{
	  (*_best) = (*_way);
	  if (flag == 3)
	    _escapeB = _escape;
	}
      if (flag != 3)
	return (1);
    }
  return (0);
}

void					IA::movePlease()
{
  if (this->_count <= 15)
    this->_count++;
  else
    {
      if (!_best->empty())
	{
	  if (_j == 0)
	    _best->push(RIGHT);
	  this->_dir = static_cast<APlayer::eDirection>(_best->top());
	  if (_best->size() == 1 && _j > 4 && canMoove() == 0)
	    {
	      this->putBomb(_gameClock->getTotalGameTime(), false);
	      _j = 1;
	    }
	  _best->pop();
	  _j++;
	}
      this->_count = 0;
    }
  if (_best->empty())
    _escapeB = -1;
  while (!_way->empty())
    _way->pop();
  this->goMove(this->_dir, *_gameClock);
}

void                                    IA::calcMap(lua_State *state)
{
  int                                   t = lua_tointeger(state, 3);
  int                                   flag = lua_tointeger(state, 4);
  std::vector<int>                      tab;
  int                                   i = 0;

  while (i < 2)
    {
      becomeRand(_sizeTab, _sizeTab);
      tab = (*_alea);
      if (flag <= 2 && i == 0)
        flag = haveBetterStrat(flag);
      else if (flag != 3)
        flag = 4;
      if (i > 1 && flag != 3)
	break ;
      _escape = 0;
      if ((recurs(t, -1, tab[0], tab[1], flag) == 1) &&
	  (verifLoop(flag, _sizeTab, _sizeTab, tab[0], tab[1]) == 1))
	break ;
      _escape = 0;
      if ((recurs(t, -1, tab[2], tab[3], flag) == 1) &&
	  (verifLoop(flag, _sizeTab, _sizeTab, tab[2], tab[3]) == 1))
	break ;
      _escape = 0;
      if ((recurs(t, -1, tab[4], tab[5], flag) == 1) &&
	  (verifLoop(flag, _sizeTab, _sizeTab, tab[4], tab[5]) == 1))
	break ;
      _escape = 0;
      if ((recurs(t, -1, tab[6], tab[7], flag) == 1) &&
	  (verifLoop(flag, _sizeTab, _sizeTab, tab[6], tab[7]) == 1))
	break ;
      i++;
    }
  movePlease();
}

void                                    IA::update(gdl::GameClock const & gameClock, 
                                                   gdl::Input & input)
{
  std::string                           addr;
  std::stringstream                     tmp;

  tmp << this;
  addr = tmp.str();  
  (void)input;
  if (_tab.empty())
    this->initTab();
  if (this->Death() == false)
    return;
  this->model_->update(gameClock);
  lua_getglobal(_l, "start");
  lua_pushstring(_l, addr.data());
  if (_lua_status == 0)
    _lua_status = lua_pcall(_l, 1, LUA_MULTRET, 0);
  if (_lua_status != 0)
    this->makeDeath();
  while (lua_gettop(_l))
    lua_pop(_l, 1);
}
