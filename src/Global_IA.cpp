//
// Global_ia.cpp for morine_n in /home/morine_n//bomberman/inc
// 
// Made by nanouck morineau
// Login   <morine_n@epitech.net>
// 
// Started on  Fri May 31 15:40:18 2013 nanouck morineau
// Last update Sun Jun  9 15:07:17 2013 jean grizet
//

#include "Global_IA.hh"
#include <stack>

pthread_mutex_t				mGIA = PTHREAD_MUTEX_INITIALIZER;

Global_IA::Global_IA(Map *map)
{
  this->_map = map;
  this->_case = map->getMap();
  _riskMap = new int*[_map->getsizeX() + 1];
  for (int i = 0; i < (_map->getsizeX() + 1); i++)
    {
      _riskMap[i] = new int[_map->getsizeY() + 1];
      for (int j = 0; j < _map->getsizeY() + 1; j++)
	_riskMap[i][j] = 0;
    }
  _sizeX = _map->getsizeX();
  _sizeY = _map->getsizeY();
  _count = 0;
  _nbBox = 1;
}

Global_IA::~Global_IA()
{
  for (int i = 0; i < _sizeX; i++)
    delete [] _riskMap[i];
  delete [] _riskMap;
}

void	Global_IA::setRisk(std::vector< std::pair<int, int> > unsafe)
{
  std::vector< std::pair<int, int> >::iterator	tmp = unsafe.begin();

  while (tmp != unsafe.end())
    {
      _riskMap[(*tmp).second][(*tmp).first] = -42;
      ++tmp;
    }
}

void	Global_IA::update()
{
  int	x;
  int	sizeX = _map->getsizeX();
  int	sizeY = _map->getsizeY();
  std::stack<Bomb *>	tmp;

  pthread_mutex_lock(&mGIA);
  _nbBox = 0;
  for (int y = 0; y <= sizeY; y++)
    {
      x = 0;
      for (; x <= sizeX; x++)
	{
	  if (_case[y][x]->canWalk() == 2)
	    _nbBox++;
	  if (_case[y][x]->getDeath() == true)
	    _riskMap[x][y] = -42;
	  else if (_case[y][x]->canWalk() == 0)
	    _riskMap[x][y] = _case[y][x]->getIdBonus();
	  if (_case[y][x]->canWalk() == 6)
	    {
	      _riskMap[x][y] = -42;
	      tmp.push(dynamic_cast<Bomb*>(_case[y][x]->getObject()));
	    }
	  else if (_riskMap[x][y] != -42)
	    {
	      _riskMap[x][y] = _case[y][x]->canWalk();
	      if (_case[y][x]->getIdBonus() != 0)
		_riskMap[x][y] = _case[y][x]->getIdBonus();
	    }
	}
    }
  while (!tmp.empty())
    {
      if (tmp.top() != NULL)
	setRisk(tmp.top()->getUnsafe());
      tmp.pop();
    }
  pthread_mutex_unlock(&mGIA);
}

void	Global_IA::getRiskMap(int y, int x, int rayon, int ***tab)
{
  int	cx = 0;
  int	cy = 0;
  int	i = 0;
  
  pthread_mutex_lock(&mGIA);
  _y = ((y - rayon) < 0) ? 0 : y - rayon;
  cy = (-(y - rayon) < 0) ? 0 : (-(y -rayon));
  while (_y != (y + rayon) && _y <= _sizeY)
    {
      cx = (-(x - rayon) < 0) ? 0 : (-(x -rayon));
      _x = ((x - rayon) < 0) ? 0 : x - rayon;
      while (_x != (x + rayon) && _x <= _sizeX)
	{
	  i++;
	  if (cx >= 0 && cy >= 0)
	    (*tab)[cx][cy] = _riskMap[_x][_y];
	  cx++;
	  _x++;
	}
      _y++;
      cy++;
    }
  pthread_mutex_unlock(&mGIA);
}

int	Global_IA::getNbBox() const{
  return _nbBox;
}
