 //
// ShowNumber.cpp for ShowNumber in /home/lecoq_j//Dropbox/ProjetTech2/Bomberman/src
// 
// Made by jean-charles lecoq
// Login   <lecoq_j@epitech.net>
// 
// Started on  Sat May 25 14:32:31 2013 jean-charles lecoq
// Last update Sun Jun  9 15:38:08 2013 jean grizet
//

#include	"ShowString.hh"

ShowString::ShowString(Texture& texture)
  :_texture(texture)
{
  str_.assign("");
}

ShowString::~ShowString()
{}

void	ShowString::initialize(int nbMax, float xFirst, float yFirst,	\
			       float zFirst, float lenx, float leny,	\
			       float lenz, int dir)
{
  i_ = 0;

  nbMax_ = nbMax;
  while (i_ < nbMax_)
    {
      plan_.push_back(new Plan(_texture));
      i_++;
    }


  std::vector<Plan*>::iterator	it;

  for (it = plan_.begin(); it != plan_.end(); ++it)
    {
      (*it)->initialize(xFirst, xFirst + lenx, yFirst, yFirst + leny, zFirst, zFirst + lenz);
      if (dir == 1)
	xFirst += lenx;
      if (dir == 2)
	yFirst += leny;
      if (dir == 3)
	zFirst += lenz;
    }
}

void	ShowString::update(std::string const& str)
{
  str_.assign(str);
  int	a;

  std::vector<Plan*>::iterator  it;
  a = 0;

  for (it = plan_.begin(); it != plan_.end(); ++it)
    {
      if (a < (int)str_.size())
	{
	  (*it)->update(_linkTexture.getChar(str_[a]));
	  a++;
	}
      else
	(*it)->update(_linkTexture.getChar('|'));
    }
}

void	ShowString::draw()
{
  std::vector<Plan*>::iterator	it;
  int	a;

  a = 0;
  for (it = plan_.begin(); it != plan_.end(); ++it)
    {
      if (a < (int)str_.size())
	{
	  (*it)->draw();
	  a++;
	}
    }
}
