//
// BagOfBomb.cpp for bomberman in /home/grilla_b//Documents/epi/projet_tek2/boomberman/real4
// 
// Made by brian grillard
// Login   <grilla_b@epitech.net>
// 
// Started on  Wed May 22 15:13:23 2013 brian grillard
// Last update Sat Jun  8 15:18:34 2013 jean grizet
//

#include "BagOfBomb.hh"
#include "Bomb.hh"

BagOfBomb::BagOfBomb(int & id_player, GameSound& gs) :
  _sound(gs)
{
  _end = 0;
  _nbon = 0;
  _max = 1;
  _points = 0;
  _idBomber = id_player;
}

BagOfBomb::~BagOfBomb() 
{
  for (std::vector<Bomb *>::iterator it = _bag.begin(); it != _bag.end(); ++it)
    {
      Bomb *tmp;
      tmp = *it;
      if (tmp != NULL)
	delete(tmp);
    }
}

std::vector<Bomb*>	BagOfBomb::getBag() const { return (this->_bag); }

std::vector<bool>	BagOfBomb::getIdBomb() const { return (this->_idBomb); }

int			BagOfBomb::getPosFree()
{
  int			pos = 0;

  std::vector<bool>::iterator it = this->_idBomb.begin();
  for (; (it != this->_idBomb.end()) && (*it != false); ++it);
  if (it == this->_idBomb.end())
    return (-1);
  pos = it - this->_idBomb.begin();
  return (pos);
}

void			BagOfBomb::increaseNbon(int nbr)
{
  _mu.lock();
  this->_nbon += nbr;
  _mu.unlock();
}

void			BagOfBomb::decreaseNbon(int nbr)
{
  this->_nbon -= nbr;
}

int			BagOfBomb::getNbon() const
{
  return (this->_nbon);
}

int			BagOfBomb::getMax() const
{
  return (this->_max);
}

void			BagOfBomb::addBomb(int nbr)
{
  _mu.lock();
  for (int i = 0; i != nbr; i++)
    {
      this->_bag.push_back(new Bomb(_sound));
      this->_idBomb.push_back(false);
    }
  _mu.unlock();
}

int			BagOfBomb::getMaxBomb() const
{
  return (this->_idBomb.size());
}

int			BagOfBomb::getNbrBombOnMap()
{
  int			temp = 0;

  std::vector<bool>::iterator it = this->_idBomb.begin();
  for (; it != this->_idBomb.end(); ++it);
  {
    if ((*it) == true)
      temp++;
  }
  return (temp);
}

void          BagOfBomb::changeStatePosBomb(int pos)
{
  if (this->_idBomb[pos] == true)
    this->_idBomb[pos] = false;
  else
    this->_idBomb[pos] = true;
}

void		BagOfBomb::initialize(){}

void		BagOfBomb::update(gdl::GameClock const & gameClock,	\
				  gdl::Input & input)
{
  std::vector<bool>::iterator it = this->_idBomb.begin();
  for (; it != this->_idBomb.end(); ++it)
    {
      _mu.lock();
      if ((*it) == true)
	this->_bag[it - this->_idBomb.begin()]->update(gameClock, input);
      _mu.unlock();
    }
  (void)gameClock;
  (void)input;
}

void		BagOfBomb::draw()
{
  _mu.lock();
  std::vector<bool>::iterator it = this->_idBomb.begin();

  for (; it != this->_idBomb.end(); ++it)
    {
      if ((*it) == true)
	this->_bag[it - this->_idBomb.begin()]->draw();
    }
  _mu.unlock();
}

void		BagOfBomb::addPower(int nbr)
{
  _mu.lock();
  std::vector<Bomb*>::iterator it = this->_bag.begin();
  for (; it != this->_bag.end(); ++it)
    {
      (*it)->addExplosion(nbr * 4);
      (*it)->addPower(nbr);
    }
  _mu.unlock();
}

std::vector<Bomb*>	BagOfBomb::getBombOnMap()
{
  std::vector<Bomb*>	temp;
  std::vector<bool>::iterator it = this->_idBomb.begin();  
  for (; it != this->_idBomb.end(); ++it)
    {
      if ((*it) == true)
	temp.push_back(this->_bag[it - this->_idBomb.begin()]);
    }
  return (temp);
}

AObject		*BagOfBomb::getReplace() const
{ return (NULL); }

IEvent		*BagOfBomb::createEvent(gdl::GameClock const & gameClock, \
					gdl::Input & input)
{
  return (new Event(this, gameClock, input));
}

int		BagOfBomb::getEnd() const { return this->_end; }
void		BagOfBomb::setEnd(int end) { this->_end = end; }

void		BagOfBomb::addPoints(int nbr) 
{ 
  this->_points += nbr; 
}

int		BagOfBomb::getPoints() const { return (this->_points); }

int		BagOfBomb::getIdBomber() const
{
  return (this->_idBomber);
}

int		BagOfBomb::getPower() const
{
  return (this->_bag[0]->getPower());
}

void		BagOfBomb::setPower(int power)
{
  std::vector<Bomb*>::iterator it = this->_bag.begin();

  for (; it != this->_bag.end(); ++it)
    (*it)->setPower(power);
}

void		BagOfBomb::setPoints(int points)
{
  this->_points = points;
}
