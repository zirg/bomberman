//
// Ui.cpp for  in /home/grizet_j//C++/bomberman/src
// 
// Made by jean grizet
// Login   <grizet_j@epitech.net>
// 
// Started on  Fri Jun  7 09:00:08 2013 jean grizet
// Last update Fri Jun  7 10:35:29 2013 jean grizet
//

#include "UserInterface.hh"

UserInterface::UserInterface(Option& opt) :
  _option(opt)
{
  _ui = new gdl::Image(gdl::Image::load("data/assets/ui.png"));
}

UserInterface::~UserInterface()
{
  delete(_ui);
}

void	UserInterface::drawPlayer(const Model::Bomberman *player,	\
				  int offx, int offy)
{
  gdl::Text     t;
  std::stringstream ss;

  t.setSize(30);
  ss << player->getPoints();
  t.setText(ss.str());
  t.setPosition(offx, offy);
  t.draw();

  ss.str("");
  ss << player->getSpeed();
  t.setText(ss.str());
  t.setPosition(offx + 40, offy + 50);
  t.draw();

  ss.str("");
  ss << player->getMaxBomb();
  t.setText(ss.str());
  t.setPosition(offx + 100, offy + 50);
  t.draw();

  ss.str("");
  ss << player->getPower();
  t.setText(ss.str());
  t.setPosition(offx + 150, offy + 50);
  t.draw();
}

void	UserInterface::draw()
{
  double        offx  = _option.getScreenX() / 4.0;
  double        offy  = _option.getScreenY() / 10.0;
  
  glMatrixMode(GL_PROJECTION);
  glViewport(0, 0, _option.getScreenX(), _option.getScreenY());
  gluPerspective(70.0f, _option.getScreenX() / _option.getScreenY(),	\
                 1.0f, 10000.0f);
  gluLookAt(_option.getScreenX() / 2.0, 1000.0f, _option.getScreenY() / 2.0,
            _option.getScreenX() / 2.0, 0.0, _option.getScreenY() / 2.0,
            0.0f, 1.0f, 0.0f);
  glPushMatrix();
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  glDisable(GL_DEPTH_TEST);
  glOrtho(0.0f, _option.getScreenX(), _option.getScreenY(), 0.0, 0.0, 1.0);
  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);
  _ui->bind();
  glBegin(GL_QUADS);
  glTexCoord2f(0.0, 0.0);
  glVertex3f(0.0, 0.0, 0.0);
  glTexCoord2f(1.0, 0.0);  
  glVertex3f(offx, 0.0, 0.0);
  glTexCoord2f(1.0, 1.0);
  glVertex3f(offx, offy, 0.0);
  glTexCoord2f(0.0, 1.0);
  glVertex3f(0.0, offy, 0.0);
  if (_option.getPlayer() == 2)
    {
      offx = _option.getScreenX() - offx;
      offy = _option.getScreenY() - offy;
      glTexCoord2f(0.0, 0.0);
      glVertex3f(offx, offy, 0.0);
      glTexCoord2f(1.0, 0.0);
      glVertex3f(_option.getScreenX(), offy, 0.0);
      glTexCoord2f(1.0, 1.0);
      glVertex3f(_option.getScreenX(), _option.getScreenY(), 0.0);
      glTexCoord2f(0.0, 1.0);
      glVertex3f(offx, _option.getScreenY(), 0.0);
    }
  glEnd();
  glPopMatrix();
}

void		UserInterface::drawText(Model::Bomberman *j1,	\
					Model::Bomberman *j2)
{
  if (j1 != NULL)
    drawPlayer(j1, 100, 10);
  if (j2 != NULL && _option.getPlayer() == 2)
    drawPlayer(j2, 1000, 910);
}

void		UserInterface::drawMenu()
{
  gdl::Text     t;

  t.setText("");
  t.setPosition(0, 0);
  t.draw();
}
