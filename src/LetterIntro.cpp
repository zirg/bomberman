//
// LetterIntro.cpp for bomberman7 in /home/grilla_b//Documents/epi/projet_tek2/boomberman/repo/bomberman/src
// 
// Made by brian grillard
// Login   <grilla_b@epitech.net>
// 
// Started on  Sat Jun  8 00:21:10 2013 brian grillard
// Last update Sat Jun  8 17:48:32 2013 jean grizet
//

#include "LetterIntro.hh"

LetterIntro::LetterIntro(gdl::Image* let, float posX,	\
			 float posY, float posZ)
{
  _let = let;
  _position.x = posX;
  _position.y = posY;
  _position.z = posZ;
}

LetterIntro::~LetterIntro()
{
}

void		LetterIntro::initialize()
{
}

void		LetterIntro::update(gdl::GameClock const & gameClock)
{
  (void)gameClock;
  _position.y += 170;
}

void		LetterIntro::draw()
{
  glPushMatrix();
  glLoadIdentity();
  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

  _let->bind();
  glBegin(GL_QUADS);
  glTexCoord2f(0.0, 0.0);
  glVertex3f(_position.x, _position.y + 350.0f, _position.z + 350.0f);
  glTexCoord2f(0.0, 1.0f);
  glVertex3f(_position.x, _position.y, _position.z + 350.0f);
  glTexCoord2f(1.0f, 1.0f);
  glVertex3f(_position.x + 350.0f, _position.y, _position.z + 350.0f);
  glTexCoord2f(1.0f, 0.0);
  glVertex3f(_position.x + 350.0f, _position.y + 350.0f, _position.z + 350.0f);
  glEnd();
 
  glDisable(GL_BLEND);
  glDisable(GL_TEXTURE_2D);
  glPopMatrix();
}

void		LetterIntro::drawEnter()
{
  glPushMatrix();
  glLoadIdentity();
  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

  _let->bind();
  glBegin(GL_QUADS);
  glTexCoord2f(0.0, 0.0);
  glVertex3f(_position.x, _position.y + 350.0f, _position.z + 1350.0f);
  glTexCoord2f(0.0, 1.0f);
  glVertex3f(_position.x, _position.y, _position.z + 1350.0f);
  glTexCoord2f(1.0f, 1.0f);
  glVertex3f(_position.x + 1500.0f, _position.y, _position.z + 1350.0f);
  glTexCoord2f(1.0f, 0.0);
  glVertex3f(_position.x + 1500.0f, _position.y + 350.0f, _position.z + 1350.0f);
  glEnd();
 
  glDisable(GL_BLEND);
  glDisable(GL_TEXTURE_2D);
  glPopMatrix();
}

float		LetterIntro::getPosX() const
{
  return (this->_position.x);
}

void		LetterIntro::addPosX(float val)
{
  _position.x += val;
}
