//
// SaveMap.cpp for bomberman in /home/grilla_b//Documents/epi/projet_tek2/boomberman/repo/bomberman
// 
// Made by brian grillard
// Login   <grilla_b@epitech.net>
// 
// Started on  Wed Jun  5 21:22:03 2013 brian grillard
// Last update Sun Jun  9 12:40:27 2013 brian grillard
//

#include "SaveMap.hh"
#include <fstream>

SaveMap::SaveMap()
{
}

SaveMap::~SaveMap()
{
}

void	SaveMap::recordMap(std::string const & path, MapEditor* map)
{
  std::ofstream file;

  file.open(path.c_str(), std::ios::out);
  if (file.bad())
    {
      std::cerr << "fail openning" << std::endl;
      return;
    }
  file << map->getsizeX() << std::endl;
  file << map->getsizeY() << std::endl;
  for (int ty = 0; ty != map->getsizeY(); ty++)
    {
      for (int tx = 0; tx != map->getsizeX(); tx++)
	{
	  file << map->getMap()[ty][tx]->getType();
	  if (tx != map->getsizeX())
	    file << ".";
	}
      file << std::endl;
    }
  file.close();
}

void	SaveMap::recordMap(std::string const & path, Map* map)
{
  std::ofstream file;

  file.open(path.c_str(), std::ios::out);
  if (file.bad())
    {
      std::cerr << "fail openning" << std::endl;
      return;
    }
  file << map->getsizeX() << std::endl;
  file << map->getsizeY() << std::endl;
  for (int ty = 0; ty != map->getsizeY(); ty++)
    {
      for (int tx = 0; tx != map->getsizeX(); tx++)
	{
	  Case*	       cs = map->getMap()[ty][tx];
	  if (cs->getIdBonus() != 0)
	    file << cs->getIdBonus();
	  else if (cs->canWalk() != 6)
	    file << cs->canWalk();
	  else
	    file << "0";
	  if (tx != map->getsizeX())
	    file << ".";
	}
      file << std::endl;
    }
  file.close();
}
