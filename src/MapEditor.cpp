//
// MapEditor.cpp for bomberman in /home/grilla_b//Documents/epi/projet_tek2/boomberman/repo/bomberman
// 
// Made by brian grillard
// Login   <grilla_b@epitech.net>
// 
// Started on  Sun Jun  2 14:27:38 2013 brian grillard
// Last update Fri Jun  7 06:31:08 2013 jean grizet
//

#include "MapEditor.hh"
#include "Viseur.hh"

MapEditor::MapEditor()
{
}

MapEditor::~MapEditor()
{
}


void                    MapEditor::initializeMap(int x, int y)
{
  this->_sizeX = x;
  this->_sizeY = y;
  this->_rsizeX = x * 350.0f;
  this->_rsizeZ = y * 350.0f;

  for (int ty = 0; ty <= this->_sizeY; ty++)
    {
      this->_map.push_back(*(new std::vector<CaseEditor*>));
      for (int tx = 0; tx <= this->_sizeX; tx++)
        {
          this->_map[ty].push_back(new CaseEditor(tx, ty, 0, NULL));
	  this->_map[ty][tx]->setViseur(NULL);
	}
    }
}

void			MapEditor::update(gdl::GameClock const & gameClock, gdl::Input & input)
{
  (void)gameClock;
  (void)input;
}

int                     MapEditor::getsizeX() const { return (this->_sizeX); }
int                     MapEditor::getsizeY() const { return (this->_sizeY); }
float                   MapEditor::getrsizeX() const { return (this->_rsizeX); }
float                   MapEditor::getrsizeZ() const { return (this->_rsizeZ); }
std::vector< std::vector<CaseEditor*> >       MapEditor::getMap() const
{ return (this->_map); }

void                    MapEditor::draw()
{
  for (int ty = 0; ty <= _sizeY; ty++)
    {
      for (int tx = 0; tx <= _sizeX; tx++)
        {
          if (_map[ty][tx]->getObject() != NULL)
            _map[ty][tx]->getObject()->draw();
          if (_map[ty][tx]->getViseur() != NULL)
            _map[ty][tx]->getViseur()->draw();
        }
    }
}
