//
// EventQueue.cpp for  in /home/grizet_j//C++/plazza/src
// 
// Made by jean grizet
// Login   <grizet_j@epitech.net>
// 
// Started on  Mon Apr 15 15:17:02 2013 jean grizet
// Last update Mon May 13 20:11:17 2013 jean grizet
//

#include "EventQueue.hh"

template <typename T>
EventQueue<T>::EventQueue(IMutex& mu, ICondVar& var) :
  _finished(false), _mutex(mu), _cond(var)
{}

template <typename T>
EventQueue<T>::~EventQueue(void) 
{
  _finished = true;
  _cond.broadcast();
}

template <typename T>
void  EventQueue<T>::push(T* elem)
{
  if (_finished == false)
    {
      _mutex.lock();
      _queue.push(elem);
      _mutex.unlock();
    }
}

template <typename T>
bool  EventQueue<T>::tryPop(T** store)
{
  if (_queue.empty() == true)
    return (false);
  if (_mutex.trylock() != 0)
    return (false);
  *store = _queue.front();
  _queue.pop();
  _mutex.unlock();
  return (true);
}

template <typename T>
T*	EventQueue<T>::pop(void)
{
  T*	elem = NULL;

  _mutex.lock();
  if (this->isFinished() == false && _queue.empty() == false)
    {
      elem = _queue.front();
      _queue.pop();
    }
  _mutex.unlock();
  return (elem);
}

template <typename T>
bool  EventQueue<T>::isFinished(void) const
{
  if (_finished == true && _queue.empty() == true)
    return (true);
  else
    return (false);
}

template <typename T>
void  EventQueue<T>::setFinished(void)
{
  _mutex.lock();
  _finished = true;
  _mutex.unlock();
}

template <typename T>
unsigned int	EventQueue<T>::getSize() const
{
  return (_queue.size());
}


template EventQueue<IEvent>::EventQueue(IMutex&, ICondVar&);
