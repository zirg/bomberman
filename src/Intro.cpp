//
// Bomber.cpp for  in /home/grizet_j//C++/bomberman/src
// 
// Made by jean grizet
// Login   <grizet_j@epitech.net>
// 
// Started on  Mon Apr 29 12:15:19 2013 jean grizet
// Last update Sat Jun  8 19:32:31 2013 jean grizet
//

#include "Intro.hh"

Intro::Intro(Option& o, GameSound& s, gdl::GameClock& gc,	\
	     gdl::Input& i, Texture * t) :
  _opt(o), _sound(s), gameClock_(gc), input_(i), _texture(t)
{
  _updateTime = 0.0;
  _drawTime = 0.0;
  _back = new gdl::Image(gdl::Image::load("data/assets/atomic.png"));
  _end = true;
  _position.x = -2000;
  _position.y = 20.0;
  _position.z = 1000.0;
  _rotation.x = 0.0;
  _rotation.y = 0.1;
  _rotation.z = 0.0;
  _angle = 90;
  _stape = false;
  _run = true;
  _timeInput = 0;
}

Intro::~Intro()
{
  delete (_back);
}

void	Intro::addLetter(int posX, int posY, int posZ)
{
  gdl::Image* im;
  im = new gdl::Image(gdl::Image::load("data/assets/enter.png"));
  _enter = new LetterIntro(im, 5100, posY, 1000);
  im = _texture->getTexture(54);
  _letter.push_back(new LetterIntro(im, posX, posY, posZ));
  posX += 350;
  im = _texture->getTexture(67);
  _letter.push_back(new LetterIntro(im, posX, posY, posZ));
  posX += 350;
  im = _texture->getTexture(65);
  _letter.push_back(new LetterIntro(im, posX, posY, posZ));
  posX += 350;
  im = _texture->getTexture(54);
  _letter.push_back(new LetterIntro(im, posX, posY, posZ));
  posX += 350;
  im = _texture->getTexture(57);
  _letter.push_back(new LetterIntro(im, posX, posY, posZ));
  posX += 350;
  im = _texture->getTexture(70);
  _letter.push_back(new LetterIntro(im, posX, posY, posZ));
  posX += 350;
  im = _texture->getTexture(65);
  _letter.push_back(new LetterIntro(im, posX, posY, posZ));
  posX += 350;
  im = _texture->getTexture(53);
  _letter.push_back(new LetterIntro(im, posX, posY, posZ));
  posX += 350;
  im = _texture->getTexture(66);
  _letter.push_back(new LetterIntro(im, posX, posY, posZ));
}

void  Intro::initialize(void)
{
  int   i = 0;
  float posX = 0;
  float posY = 0;
  float posZ = 400;
  gdl::Color  c(43,128,32);

  window_.setWidth(_opt.getScreenX());
  window_.setHeight(_opt.getScreenY());
  window_.create();

  _sound.setVolumeEffect(100);

  while (i != 9)
    {
      Box*      b = new Box;
      _box.push_back(b);
      b->initBox(posX, posY, posZ);
      posX += 350;
      i++;
    }
  _end = false;
  _cam.initialize(_opt);
  _bomberman = gdl::Model::load("data/assets/marvin.fbx");
  this->_bomberman.set_default_model_color(c);
  gdl::Model::cut_animation(this->_bomberman, "Take 001", 35, 54, "run");
  _bomberman.play("run", gdl::Anim::RUN);
  addLetter(0, 350, 350);
}

bool                  Intro::checkTimeInput(gdl::GameClock const & gameClock,
						double timeMax)
{
  float               time;

  time = gameClock.getTotalGameTime();
  if ((time - this->_timeInput) >= timeMax)
    {
      this->_timeInput = time;
      return (true);
    }
  return (false);
}

void	Intro::playWalk()
{
  if (checkTimeInput(gameClock_, 0.3) == true)
    {
      _sound.setVolumeEffect(10);
      if (_run == true)
	{
	  _run = false;
	  _sound.playEffect(GameSound::WALK1);
	}
      else
	{
	  _run = true;
	  _sound.playEffect(GameSound::WALK2);
	}
      _sound.setVolumeEffect(30);
    }
}

void	Intro::loopUpdate()
{
  playWalk();
  _position.x += 12;
  std::vector<LetterIntro*>::iterator it = this->_letter.begin();
  for (; it != this->_letter.end(); ++it)
    {
      if (_position.x >= (*it)->getPosX())
	{
	  (*it)->update(gameClock_);
	}
    }
  std::vector<Box*>::iterator itb = this->_box.begin();
  for (; itb != this->_box.end(); ++itb)
    {
      if (_position.x >= (*itb)->getPosX())
	{
	  (*itb)->update(gameClock_,  input_);
	  int sous = _position.x - (*itb)->getPosX();
	  if (sous <= 12 && sous > 0)
	    _sound.playEffect(GameSound::EXPLOSION);
	}
    }
}

void  Intro::update(void)
{
  if ((input_.isKeyDown(gdl::Keys::Return) == true) || _end == true)
    {
      _end = true;
      return;
    }
  this->_bomberman.set_anim_speed("run", 1.22);
  _bomberman.update(gameClock_);
  if (_bomberman.anim_is_ended("run"))
    _bomberman.play("run", gdl::Anim::RUN);
  if ((_position.x < 6400) && _stape == false)
    {
      loopUpdate();
    }
  else if (_position.x >= 6400 && _stape == false)
    {
      _stape = true;
      _angle = -90;
      _position.x -= 12;
      _enter->addPosX(-8);
    }
  else
    {
      if (_position.x > - 2000)
	{
	  playWalk();
	  _position.x -= 12;
	}
      if (_enter->getPosX() > 1000)
	_enter->addPosX(-8);
    }
}

void	Intro::drawBack()
{
  glPushMatrix();
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  glDisable(GL_DEPTH_TEST);
  glOrtho(0.0f, _opt.getScreenX(), _opt.getScreenY(), 0.0, 0.0, 1.0);
  _back->bind();
  glBegin(GL_QUADS);
  glTexCoord2f(0.0, 0.0);
  glVertex3f(0.0, 0.0, 0.0);
  glTexCoord2f(1.0, 0.0);
  glVertex3f(1200.0, 0.0, 0.0);
  glTexCoord2f(1.0, 1.0);
  glVertex3f(1200.0, 1000.0, 0.0);
  glTexCoord2f(0.0, 1.0);
  glVertex3f(0.0, 1000.0, 0.0);
  glEnd();
  glPopMatrix();
}

void	Intro::drawBomber()
{
  glPushMatrix();
  glLoadIdentity();

  glTranslatef(_position.x, _position.y, _position.z);
  glRotatef(_angle, _rotation.x, _rotation.y, _rotation.z);

  this->_bomberman.draw();

  glPopMatrix();
}

void  Intro::draw(void)
{
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
  glClearDepth(1.0f);
  _cam.setPosition(1850, 550, 4950);
  _cam.setLook(1850, 550, 50);
  _cam.update(gameClock_, input_, 0);
  drawBack();
  _cam.setMode();
  glPushMatrix();
  glLoadIdentity();
  std::vector<LetterIntro*>::iterator itl = this->_letter.begin();
  for (; itl != this->_letter.end(); ++itl)
    (*itl)->draw();
  drawBomber();
  std::vector<Box*>::iterator it = this->_box.begin();
  for (; it != this->_box.end(); ++it)
    (*it)->draw();
  _enter->drawEnter();
  glPopMatrix();
}

bool	Intro::isEnded() const
{ return (_end); }

void    Intro::unload()
{}
