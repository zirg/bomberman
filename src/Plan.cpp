//
// Bomberman.cpp for  in /home/grizet_j//C++/bomberman/src
// 
// Made by jean grizet
// Login   <grizet_j@epitech.net>
// 
// Started on  Mon Apr 29 12:27:58 2013 jean grizet
// Last update Sun Jun  9 17:58:46 2013 jean grizet
//

#include "Plan.hh"
#include <iostream>

Plan::Plan(Texture& texture)
  :_texture(texture)
{  }

Plan::~Plan(void)
{

}

void	Plan::update(Texture::idTexture id)
{
  _id = id;
}

void Plan::initialize(float x1, float x2,
		      float y1,float y2,
		      float z1, float z2)
{
  x1_ = x1;
  x2_ = x2;
  y1_ = y1;
  y2_ = y2;
  z1_ = z1;
  z2_ = z2;
}

void Plan::draw(void)
{
  glPushMatrix();
  glLoadIdentity();
  glEnable(GL_BLEND); glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
  _texture.bindTexture(_id);
  glBegin(GL_QUADS);
  glTexCoord2f(0.0, 0.0);
  glVertex3f(x1_, y2_, z1_);
  glTexCoord2f(0.0, 1.0f);
  glVertex3f(x1_, y1_, z1_);
  glTexCoord2f(1.0f, 1.0f);
  glVertex3f(x2_, y1_, z2_);
  glTexCoord2f(1.0f, 0.0);
  glVertex3f(x2_, y2_, z2_);
  glEnd();
  glDisable(GL_BLEND);
  glDisable(GL_TEXTURE_2D);
  glPopMatrix();
}
