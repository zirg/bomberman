//
// NewGame.cpp for NewGame in /home/lecoq_j//Tech2/ProjetC++/Bomberman/src/menu
//
// Made by jean-charles lecoq
// Login   <lecoq_j@epitech.net>
// 
// Started on  Wed May  8 22:51:46 2013 jean-charles lecoq
// Last update Sun Jun  9 11:50:05 2013 brian grillard
//

#include	"MenuLoadGame.hh"
#include	<iostream>

MenuLoadGame::MenuLoadGame(Texture& texture, std::vector<std::string>& vec,
			   std::vector<std::string>::iterator& it)
  :_texture(texture), _listGame(vec), _posListGame(it)
{ }

MenuLoadGame::~MenuLoadGame()
{ }

void	MenuLoadGame::initialize()
{
  _showSave = new ShowString(_texture);

  _showSave->initialize(30, 3400, 0, -650, 0, 50, 50, 3);
  if (!_listGame.empty())
    {
      _showSave->update((*_posListGame));
    }
  time_ = 0.0;
  x1Title_ = 3400;
  x2Title_ = 3400;
  y1Title_ = 240.5;
  y2Title_ = 570.5;
  z1Title_ = -700;
  z2Title_ = 700;
  x1_ = 3480;
  x2_ = 3480;
  y1_ = -600;
  y2_ = 600;
  z1_ = -990;
  z2_ = 990;
}

void	MenuLoadGame::update(gdl::GameClock const & gameClock, gdl::Input &input)
{
  if (time_ > gameClock.getTotalGameTime())
    return;
  if (_listGame.empty())
    return;
  if (input.isKeyDown(gdl::Keys::Left) == true)
    {
      if (_posListGame == _listGame.begin())
        _posListGame = _listGame.end();
      --_posListGame;
      time_ = gameClock.getTotalGameTime() + 0.15;
    }
  if (input.isKeyDown(gdl::Keys::Right) == true)
    {
      ++_posListGame;
      if (_posListGame == _listGame.end())
        _posListGame = _listGame.begin();
      time_ = gameClock.getTotalGameTime() + 0.15;
    }
  _showSave->update((*_posListGame));  
}


void	MenuLoadGame::draw()
{
  glPushMatrix();
  glLoadIdentity();
  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
  if (!_listGame.empty())
    _texture.bindTexture(Texture::LGYES);
  else
    _texture.bindTexture(Texture::LGNO);
  glBegin(GL_QUADS);
  glTexCoord2f(0.0, 0.0);
  glVertex3f(x1_, y2_, z1_);
  glTexCoord2f(0.0, 1.0f);
  glVertex3f(x1_, y1_, z1_);
  glTexCoord2f(1.0f, 1.0f);
  glVertex3f(x2_, y1_, z2_);
  glTexCoord2f(1.0f, 0.0);
  glVertex3f(x2_, y2_, z2_);
  glEnd();

  glDisable(GL_BLEND);
  glDisable(GL_TEXTURE_2D);

  if (!_listGame.empty())
    _showSave->draw();
}

void    MenuLoadGame::zoomTitle(float x1, float x2, float y1, float y2, float z1, float z2)
{
  x1Title_ += x1;
  x2Title_ += x2;
  y1Title_ += y1;
  y2Title_ += y2;
  z1Title_ += z1;
  z2Title_ += z2;
}

void    MenuLoadGame::drawTitle()
{

  glPushMatrix();
  glLoadIdentity();
  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

  _texture.bindTexture(Texture::TITLELG);
  glBegin(GL_QUADS);
  glTexCoord2f(0.0, 0.0);
  glVertex3f(x1Title_, y2Title_, z1Title_);
  glTexCoord2f(0.0, 1.0f);
  glVertex3f(x1Title_, y1Title_, z1Title_);
  glTexCoord2f(1.0f, 1.0f);
  glVertex3f(x2Title_, y1Title_, z2Title_);
  glTexCoord2f(1.0f, 0.0);
  glVertex3f(x2Title_, y2Title_, z2Title_);
  glEnd();

  glDisable(GL_BLEND);
  glDisable(GL_TEXTURE_2D);
}

std::vector<std::string>::iterator&	MenuLoadGame::getPosListGame() const
{
  return (_posListGame);
}

std::vector<std::string>&		MenuLoadGame::getListGame() const
{
  return (this->_listGame);
}
