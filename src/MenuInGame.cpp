//
// MenuInGame.cpp for MenuinGame in /home/lecoq_j//BombermanDepot/bomberman/src
// 
// Made by jean-charles lecoq
// Login   <lecoq_j@epitech.net>
// 
// Started on  Fri Jun  7 07:36:21 2013 jean-charles lecoq
// Last update Fri Jun  7 10:30:50 2013 jean grizet
//

#include	"MenuInGame.hh"

MenuInGame::MenuInGame()
{
  _resume = new gdl::Image(gdl::Image::load("./data/assets/Menu/InGame/InGameResum.png"));
  _save = new gdl::Image(gdl::Image::load("./data/assets/Menu/InGame/InGameSaveGame.png"));
  _quit = new gdl::Image(gdl::Image::load("./data/assets/Menu/InGame/InGameMenu.png"));
  _bResume = false;
  _bSave = false;
  _bQuit = false;
  pos = 0;
  _time = 0.0;
}

MenuInGame::~MenuInGame()
{
  delete (_resume);
  delete (_save);
  delete (_quit);
}

void	MenuInGame::initialize()
{
  _bResume = false;
  _bSave = false;
  _bQuit = false;
  pos = 0;
  _time = 0.0;
}

void	MenuInGame::update(gdl::GameClock const & gameClock_, gdl::Input & input_)
{
  if (_time > gameClock_.getTotalGameTime())
    return;

  if (input_.isKeyDown(gdl::Keys::Return) == true)
    {
      if (pos == 0)
	{
	  _bResume = true;
	  _bSave = false;
	  _bQuit = false;
	}
      if (pos == 1)
	{
	  _bResume = false;
	  _bSave = true;
	  _bQuit = false;
	}
      if (pos == 2)
	{
	  _bResume = false;
	  _bSave = false;
	  _bQuit = true;
	}
      return;
    }
  if (input_.isKeyDown(gdl::Keys::Up) == true)
    {
      pos--;
      _time = gameClock_.getTotalGameTime() + 0.15;
    }
  if (input_.isKeyDown(gdl::Keys::Down) == true)
    {
      pos++;
      _time = gameClock_.getTotalGameTime() + 0.15;
    }
  if (pos < 0)
    pos = 2;
  if (pos > 2)
    pos = 0;
}

void	MenuInGame::draw()
{
  glMatrixMode(GL_PROJECTION);
  glViewport(0, 0, 1200, 1000);
  gluPerspective(60.0f, 1980.0f/1200.0f, 1.0f, 4000.0f);
  gluLookAt(600.0, 1000.0, 500.0,
            600.0, 0.0, 500.0,
            0.0f, 1.0f, 0.0f);
  glLoadIdentity();
  glDisable(GL_DEPTH_TEST);
  glOrtho(0.0f, 1200.0f, 1000.0f, 0.0f, 0.0, 1.0);
  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);
  if (pos == 0)
    _resume->bind();
  else if (pos == 1)
    _save->bind();
  else
    _quit->bind();
  glBegin(GL_QUADS);
  glTexCoord2f(0.0, 0.0);
  glVertex3f(0.0, 0.0, 0.0);
  glTexCoord2f(1.0, 0.0);
  glVertex3f(1200.0, 0.0, 0.0);
  glTexCoord2f(1.0, 1.0);
  glVertex3f(1200.0, 1000.0, 0.0);
  glTexCoord2f(0.0, 1.0);
  glVertex3f(0.0, 1000.0, 0.0);
  glEnd();
}

int	MenuInGame::getAction() const
{
  if (_bResume == true)
    return 1;
  if (_bSave == true)
    return 2;
  if (_bQuit == true)
    return 3;
  return 0;
}
