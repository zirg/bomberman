//
// LinkTexture.cpp for LinkTexture in /home/lecoq_j//BombermanDepot/bomberman/src
// 
// Made by jean-charles lecoq
// Login   <lecoq_j@epitech.net>
// 
// Started on  Sat Jun  8 11:19:13 2013 jean-charles lecoq
// Last update Sun Jun  9 16:03:27 2013 jean-charles lecoq
//

#include	"LinkTexture.hh"

LinkTexture::LinkTexture()
{
  _char['a'] = (Texture::idTexture)53;
  _char['b'] = (Texture::idTexture)54;
  _char['c'] = (Texture::idTexture)55;
  _char['d'] = (Texture::idTexture)56;
  _char['e'] = (Texture::idTexture)57;
  _char['f'] = (Texture::idTexture)58;
  _char['g'] = (Texture::idTexture)59;
  _char['h'] = (Texture::idTexture)60;
  _char['i'] = (Texture::idTexture)61;
  _char['j'] = (Texture::idTexture)62;
  _char['k'] = (Texture::idTexture)63;
  _char['l'] = (Texture::idTexture)64;
  _char['m'] = (Texture::idTexture)65;
  _char['n'] = (Texture::idTexture)66;
  _char['o'] = (Texture::idTexture)67;
  _char['p'] = (Texture::idTexture)68;
  _char['q'] = (Texture::idTexture)69;
  _char['r'] = (Texture::idTexture)70;
  _char['s'] = (Texture::idTexture)71;
  _char['t'] = (Texture::idTexture)72;
  _char['u'] = (Texture::idTexture)73;
  _char['v'] = (Texture::idTexture)74;
  _char['w'] = (Texture::idTexture)75;
  _char['x'] = (Texture::idTexture)76;
  _char['y'] = (Texture::idTexture)77;
  _char['z'] = (Texture::idTexture)78;

  _char['0'] = (Texture::idTexture)79;
  _char['1'] = (Texture::idTexture)80;
  _char['2'] = (Texture::idTexture)81;
  _char['3'] = (Texture::idTexture)82;
  _char['4'] = (Texture::idTexture)83;
  _char['5'] = (Texture::idTexture)84;
  _char['6'] = (Texture::idTexture)85;
  _char['7'] = (Texture::idTexture)86;
  _char['8'] = (Texture::idTexture)87;
  _char['9'] = (Texture::idTexture)88;
  _char[' '] = (Texture::idTexture)97;
  _char[':'] = (Texture::idTexture)96;
  _char['-'] = (Texture::idTexture)95;


  _number[0] = (Texture::idTexture)79;
  _number[1] = (Texture::idTexture)80;
  _number[2] = (Texture::idTexture)81;
  _number[3] = (Texture::idTexture)82;
  _number[4] = (Texture::idTexture)83;
  _number[5] = (Texture::idTexture)84;
  _number[6] = (Texture::idTexture)85;
  _number[7] = (Texture::idTexture)86;
  _number[8] = (Texture::idTexture)87;
  _number[9] = (Texture::idTexture)88;



  _keys[gdl::Keys::A] = (Texture::idTexture)53;
  _keys[gdl::Keys::B] = (Texture::idTexture)54;
  _keys[gdl::Keys::C] = (Texture::idTexture)55;
  _keys[gdl::Keys::D] = (Texture::idTexture)56;
  _keys[gdl::Keys::E] = (Texture::idTexture)57;
  _keys[gdl::Keys::F] = (Texture::idTexture)58;
  _keys[gdl::Keys::G] = (Texture::idTexture)59;
  _keys[gdl::Keys::H] = (Texture::idTexture)60;
  _keys[gdl::Keys::I] = (Texture::idTexture)61;
  _keys[gdl::Keys::J] = (Texture::idTexture)62;
  _keys[gdl::Keys::K] = (Texture::idTexture)63;
  _keys[gdl::Keys::L] = (Texture::idTexture)64;
  _keys[gdl::Keys::M] = (Texture::idTexture)65;
  _keys[gdl::Keys::N] = (Texture::idTexture)66;
  _keys[gdl::Keys::O] = (Texture::idTexture)67;
  _keys[gdl::Keys::P] = (Texture::idTexture)68;
  _keys[gdl::Keys::Q] = (Texture::idTexture)69;
  _keys[gdl::Keys::R] = (Texture::idTexture)70;
  _keys[gdl::Keys::S] = (Texture::idTexture)71;
  _keys[gdl::Keys::T] = (Texture::idTexture)72;
  _keys[gdl::Keys::U] = (Texture::idTexture)73;
  _keys[gdl::Keys::V] = (Texture::idTexture)74;
  _keys[gdl::Keys::W] = (Texture::idTexture)75;
  _keys[gdl::Keys::X] = (Texture::idTexture)76;
  _keys[gdl::Keys::Y] = (Texture::idTexture)77;
  _keys[gdl::Keys::Z] = (Texture::idTexture)78;


  _keys[gdl::Keys::Numpad0] = (Texture::idTexture)79;
  _keys[gdl::Keys::Numpad1] = (Texture::idTexture)80;
  _keys[gdl::Keys::Numpad2] = (Texture::idTexture)81;
  _keys[gdl::Keys::Numpad3] = (Texture::idTexture)82;
  _keys[gdl::Keys::Numpad4] = (Texture::idTexture)83;
  _keys[gdl::Keys::Numpad5] = (Texture::idTexture)84;
  _keys[gdl::Keys::Numpad6] = (Texture::idTexture)85;
  _keys[gdl::Keys::Numpad7] = (Texture::idTexture)86;
  _keys[gdl::Keys::Numpad8] = (Texture::idTexture)87;
  _keys[gdl::Keys::Numpad9] = (Texture::idTexture)88;

  _keys[gdl::Keys::Space] = (Texture::idTexture)90;
  _keys[gdl::Keys::Up] = (Texture::idTexture)91;
  _keys[gdl::Keys::Down] = (Texture::idTexture)92;
  _keys[gdl::Keys::Left] = (Texture::idTexture)93;
  _keys[gdl::Keys::Right] = (Texture::idTexture)94;

}

LinkTexture::~LinkTexture() {}

Texture::idTexture	LinkTexture::getChar(char a)
{
  if ((a >= 'A') && (a <= 'Z'))
    a += 32;
  if (((_char[a] >= 53) && (_char[a] <= 88)) || ((_char[a] >= 95) && (_char[a] <= 97)))
    return _char[a];
  return (Texture::idTexture)89;
}

Texture::idTexture	LinkTexture::getNumber(int nb)
{
  if ((_number[nb] < 0) || (_number[nb] > 94))
    return (Texture::idTexture)89;
  return _number[nb];
}

Texture::idTexture	LinkTexture::getKeys(gdl::Keys::Key k)
{
  if ((_keys[k] < 0) || (_keys[k] > 94))
    return (Texture::idTexture)89;
  return _keys[k];
}
