//
// Texture.cpp for texture in /home/lecoq_j//BombermanDepot/bomberman/src
// 
// Made by jean-charles lecoq
// Login   <lecoq_j@epitech.net>
// 
// Started on  Fri Jun  7 20:38:27 2013 jean-charles lecoq
// Last update Sun Jun  9 16:16:12 2013 jean grizet
//

#include	"Texture.hh"

Texture::Texture()
{
  /* Texture Title */
  _bindTexture[(idTexture)0] = new gdl::Image(gdl::Image::load("./data/assets/Menu/Title/TitleNewGamev1.png"));
  _bindTexture[(idTexture)1] = new gdl::Image(gdl::Image::load("./data/assets/Menu/Title/TitleLoadGamev1.png"));
  _bindTexture[(idTexture)2] = new gdl::Image(gdl::Image::load("./data/assets/Menu/Title/TitleCreateMapv1.png"));
  _bindTexture[(idTexture)3] = new gdl::Image(gdl::Image::load("./data/assets/Menu/Title/TitleScoresv1.png"));
  _bindTexture[(idTexture)4] = new gdl::Image(gdl::Image::load("./data/assets/Menu/Title/TitleSettingsv1.png"));
  _bindTexture[(idTexture)5] = new gdl::Image(gdl::Image::load("./data/assets/Menu/Title/TitleHelpv1.png"));
  _bindTexture[(idTexture)6] = new gdl::Image(gdl::Image::load("./data/assets/Menu/Title/TitleCreditv1.png"));
  _bindTexture[(idTexture)7] = new gdl::Image(gdl::Image::load("./data/assets/Menu/Title/TitleQuitv1.png"));
  /* End Texture title*/
  
  /* Texture for New game*/
  _bindTexture[(idTexture)8] = new gdl::Image(gdl::Image::load("./data/assets/Menu/NewGame/BackgroundGM.jpg"));
  _bindTexture[(idTexture)9] = new gdl::Image(gdl::Image::load("./data/assets/Menu/NewGame/BackgroundTIME.jpg"));
  _bindTexture[(idTexture)10] = new gdl::Image(gdl::Image::load("./data/assets/Menu/NewGame/BackgroundPlayers.jpg"));
  _bindTexture[(idTexture)11] = new gdl::Image(gdl::Image::load("./data/assets/Menu/NewGame/BackgroundNbIA.jpg"));
  _bindTexture[(idTexture)12] = new gdl::Image(gdl::Image::load("./data/assets/Menu/NewGame/BackgroundLevelIA.jpg"));
  _bindTexture[(idTexture)13] = new gdl::Image(gdl::Image::load("./data/assets/Menu/NewGame/BackgroundTypeMap.jpg"));
  _bindTexture[(idTexture)14] = new gdl::Image(gdl::Image::load("./data/assets/Menu/NewGame/BackgroundHeight.jpg"));
  _bindTexture[(idTexture)15] = new gdl::Image(gdl::Image::load("./data/assets/Menu/NewGame/BackgroundWidth.jpg"));
  /* end Texture New Game*/

  /*background yes no loadgame*/
  _bindTexture[(idTexture)16] = new gdl::Image(gdl::Image::load("./data/assets/Menu/LoadGame/LoadGameYes.jpg"));
  _bindTexture[(idTexture)17] = new gdl::Image(gdl::Image::load("./data/assets/Menu/LoadGame/LoadGameNo.jpg"));
  /*END background yes no loadgame*/

  /* background create map height width*/
  _bindTexture[(idTexture)18] = new gdl::Image(gdl::Image::load("./data/assets/Menu/CreateMap/createMapHeight.jpg"));
  _bindTexture[(idTexture)19] = new gdl::Image(gdl::Image::load("./data/assets/Menu/CreateMap/createMapWidth.jpg"));
  /*END background CREATE MAP*/

  /* SETTINGS */
  _bindTexture[(idTexture)20] = new gdl::Image(gdl::Image::load("./data/assets/Menu/Option/On.png"));
  _bindTexture[(idTexture)21] = new gdl::Image(gdl::Image::load("./data/assets/Menu/Option/Off.png"));
  _bindTexture[(idTexture)22] = new gdl::Image(gdl::Image::load("./data/assets/Menu/Option/KeyP1.jpg"));
  _bindTexture[(idTexture)23] = new gdl::Image(gdl::Image::load("./data/assets/Menu/Option/KeyP2.jpg"));
  _bindTexture[(idTexture)24] = new gdl::Image(gdl::Image::load("./data/assets/Menu/Option/P1.jpg"));
  _bindTexture[(idTexture)25] = new gdl::Image(gdl::Image::load("./data/assets/Menu/Option/P2.jpg"));
  _bindTexture[(idTexture)26] = new gdl::Image(gdl::Image::load("./data/assets/Menu/Option/P1U.jpg"));
  _bindTexture[(idTexture)27] = new gdl::Image(gdl::Image::load("./data/assets/Menu/Option/P1D.jpg"));
  _bindTexture[(idTexture)28] = new gdl::Image(gdl::Image::load("./data/assets/Menu/Option/P1R.jpg"));
  _bindTexture[(idTexture)29] = new gdl::Image(gdl::Image::load("./data/assets/Menu/Option/P1L.jpg"));
  _bindTexture[(idTexture)30] = new gdl::Image(gdl::Image::load("./data/assets/Menu/Option/P1B.jpg"));
  _bindTexture[(idTexture)31] = new gdl::Image(gdl::Image::load("./data/assets/Menu/Option/P2U.jpg"));
  _bindTexture[(idTexture)32] = new gdl::Image(gdl::Image::load("./data/assets/Menu/Option/P2D.jpg"));
  _bindTexture[(idTexture)33] = new gdl::Image(gdl::Image::load("./data/assets/Menu/Option/P2R.jpg"));
  _bindTexture[(idTexture)34] = new gdl::Image(gdl::Image::load("./data/assets/Menu/Option/P2L.jpg"));
  _bindTexture[(idTexture)35] = new gdl::Image(gdl::Image::load("./data/assets/Menu/Option/P2B.jpg"));
  /* END SETTING KEYS*/

  /*SETTING SOUND*/
  _bindTexture[(idTexture)36] = new gdl::Image(gdl::Image::load("./data/assets/Menu/Option/Sound.jpg"));
  _bindTexture[(idTexture)37] = new gdl::Image(gdl::Image::load("./data/assets/Menu/Option/MusicVolume.jpg"));
  _bindTexture[(idTexture)38] = new gdl::Image(gdl::Image::load("./data/assets/Menu/Option/MuteMusic.jpg"));
  _bindTexture[(idTexture)39] = new gdl::Image(gdl::Image::load("./data/assets/Menu/Option/SoundEffects.jpg"));
  _bindTexture[(idTexture)40] = new gdl::Image(gdl::Image::load("./data/assets/Menu/Option/MuteEffects.jpg"));
  _bindTexture[(idTexture)41] = new gdl::Image(gdl::Image::load("./data/assets/Menu/Option/PressKey.png"));
  /* END SETTINGS SOUND*/

  /*ON OFF */
  _bindTexture[(idTexture)42] = new gdl::Image(gdl::Image::load("./data/assets/Menu/Commun/On.png"));
  _bindTexture[(idTexture)43] = new gdl::Image(gdl::Image::load("./data/assets/Menu/Commun/Off.png"));

  /*QUIT*/
  _bindTexture[(idTexture)44] = new gdl::Image(gdl::Image::load("./data/assets/Menu/Quit/QuitNo.jpg"));
  _bindTexture[(idTexture)45] = new gdl::Image(gdl::Image::load("./data/assets/Menu/Quit/QuitYes.jpg"));

  _bindTexture[(idTexture)46] = new gdl::Image(gdl::Image::load("./data/assets/Menu/BackgroundPrincipal.jpg"));
  _bindTexture[(idTexture)47] = new gdl::Image(gdl::Image::load("./data/assets/Menu/Background/credit.jpg"));

  _bindTexture[(idTexture)48] = new gdl::Image(gdl::Image::load("./data/assets/Menu/NewGame/survival.png"));
  _bindTexture[(idTexture)49] = new gdl::Image(gdl::Image::load("./data/assets/Menu/NewGame/timeAttack.png"));
  _bindTexture[(idTexture)50] = new gdl::Image(gdl::Image::load("./data/assets/Menu/NewGame/onePlayer.png"));
  _bindTexture[(idTexture)51] = new gdl::Image(gdl::Image::load("./data/assets/Menu/NewGame/twoPlayers.png"));
  _bindTexture[(idTexture)52] = new gdl::Image(gdl::Image::load("./data/assets/Menu/NewGame/normal.png"));

  /*char*/
  _bindTexture[(idTexture)53] = new gdl::Image(gdl::Image::load("./data/assets/Menu/police/a.png"));
  _bindTexture[(idTexture)54] = new gdl::Image(gdl::Image::load("./data/assets/Menu/police/b.png"));
  _bindTexture[(idTexture)55] = new gdl::Image(gdl::Image::load("./data/assets/Menu/police/c.png"));
  _bindTexture[(idTexture)56] = new gdl::Image(gdl::Image::load("./data/assets/Menu/police/d.png"));
  _bindTexture[(idTexture)57] = new gdl::Image(gdl::Image::load("./data/assets/Menu/police/e.png"));
  _bindTexture[(idTexture)58] = new gdl::Image(gdl::Image::load("./data/assets/Menu/police/f.png"));
  _bindTexture[(idTexture)59] = new gdl::Image(gdl::Image::load("./data/assets/Menu/police/g.png"));
  _bindTexture[(idTexture)60] = new gdl::Image(gdl::Image::load("./data/assets/Menu/police/h.png"));
  _bindTexture[(idTexture)61] = new gdl::Image(gdl::Image::load("./data/assets/Menu/police/i.png"));
  _bindTexture[(idTexture)62] = new gdl::Image(gdl::Image::load("./data/assets/Menu/police/j.png"));
  _bindTexture[(idTexture)63] = new gdl::Image(gdl::Image::load("./data/assets/Menu/police/k.png"));
  _bindTexture[(idTexture)64] = new gdl::Image(gdl::Image::load("./data/assets/Menu/police/l.png"));
  _bindTexture[(idTexture)65] = new gdl::Image(gdl::Image::load("./data/assets/Menu/police/m.png"));
  _bindTexture[(idTexture)66] = new gdl::Image(gdl::Image::load("./data/assets/Menu/police/n.png"));
  _bindTexture[(idTexture)67] = new gdl::Image(gdl::Image::load("./data/assets/Menu/police/o.png"));
  _bindTexture[(idTexture)68] = new gdl::Image(gdl::Image::load("./data/assets/Menu/police/p.png"));
  _bindTexture[(idTexture)69] = new gdl::Image(gdl::Image::load("./data/assets/Menu/police/q.png"));
  _bindTexture[(idTexture)70] = new gdl::Image(gdl::Image::load("./data/assets/Menu/police/r.png"));
  _bindTexture[(idTexture)71] = new gdl::Image(gdl::Image::load("./data/assets/Menu/police/s.png"));
  _bindTexture[(idTexture)72] = new gdl::Image(gdl::Image::load("./data/assets/Menu/police/t.png"));
  _bindTexture[(idTexture)73] = new gdl::Image(gdl::Image::load("./data/assets/Menu/police/u.png"));
  _bindTexture[(idTexture)74] = new gdl::Image(gdl::Image::load("./data/assets/Menu/police/v.png"));
  _bindTexture[(idTexture)75] = new gdl::Image(gdl::Image::load("./data/assets/Menu/police/w.png"));
  _bindTexture[(idTexture)76] = new gdl::Image(gdl::Image::load("./data/assets/Menu/police/x.png"));
  _bindTexture[(idTexture)77] = new gdl::Image(gdl::Image::load("./data/assets/Menu/police/y.png"));
  _bindTexture[(idTexture)78] = new gdl::Image(gdl::Image::load("./data/assets/Menu/police/z.png"));

  _bindTexture[(idTexture)79] = new gdl::Image(gdl::Image::load("./data/assets/Menu/police/0.png"));
  _bindTexture[(idTexture)80] = new gdl::Image(gdl::Image::load("./data/assets/Menu/police/1.png"));
  _bindTexture[(idTexture)81] = new gdl::Image(gdl::Image::load("./data/assets/Menu/police/2.png"));
  _bindTexture[(idTexture)82] = new gdl::Image(gdl::Image::load("./data/assets/Menu/police/3.png"));
  _bindTexture[(idTexture)83] = new gdl::Image(gdl::Image::load("./data/assets/Menu/police/4.png"));
  _bindTexture[(idTexture)84] = new gdl::Image(gdl::Image::load("./data/assets/Menu/police/5.png"));
  _bindTexture[(idTexture)85] = new gdl::Image(gdl::Image::load("./data/assets/Menu/police/6.png"));
  _bindTexture[(idTexture)86] = new gdl::Image(gdl::Image::load("./data/assets/Menu/police/7.png"));
  _bindTexture[(idTexture)87] = new gdl::Image(gdl::Image::load("./data/assets/Menu/police/8.png"));
  _bindTexture[(idTexture)88] = new gdl::Image(gdl::Image::load("./data/assets/Menu/police/9.png"));
  _bindTexture[(idTexture)89] = new gdl::Image(gdl::Image::load("./data/assets/Menu/police/none.png"));

  _bindTexture[(idTexture)90] = new gdl::Image(gdl::Image::load("./data/assets/Menu/police/57.png"));
  _bindTexture[(idTexture)91] = new gdl::Image(gdl::Image::load("./data/assets/Menu/police/73.png"));
  _bindTexture[(idTexture)92] = new gdl::Image(gdl::Image::load("./data/assets/Menu/police/74.png"));
  _bindTexture[(idTexture)93] = new gdl::Image(gdl::Image::load("./data/assets/Menu/police/71.png"));
  _bindTexture[(idTexture)94] = new gdl::Image(gdl::Image::load("./data/assets/Menu/police/72.png"));

  _bindTexture[(idTexture)95] = new gdl::Image(gdl::Image::load("./data/assets/Menu/police/tiret.png"));
  _bindTexture[(idTexture)96] = new gdl::Image(gdl::Image::load("./data/assets/Menu/police/deux-point.png"));
  _bindTexture[(idTexture)97] = new gdl::Image(gdl::Image::load("./data/assets/Menu/police/empty.png"));

  _bindTexture[(idTexture)98] = new gdl::Image(gdl::Image::load("./data/assets/Menu/Background/Help.jpg"));
  _bindTexture[(idTexture)99] = new gdl::Image(gdl::Image::load("./data/assets/Menu/score19801200.jpg"));
}

Texture::~Texture()
{
  std::cout << "Starting delete Texture" << std::endl;
  std::map<idTexture, gdl::Image*>::iterator	it;

  for (it = _bindTexture.begin(); it != _bindTexture.end(); ++it)
    delete ((*it).second);  
  std::cout << "> END Delete Texture" << std::endl;
}

void		Texture::bindTexture(idTexture id)
{
  _bindTexture[id]->bind();
}

gdl::Image*             Texture::getTexture(int id)
{
  if (id >= 0 && id <= 94)
    return (_bindTexture[(idTexture)id]);
  return (_bindTexture[(idTexture)0]);
}
