//
// Sound.cpp for  in /home/grizet_j//C++/bomberman/src
// 
// Made by jean grizet
// Login   <grizet_j@epitech.net>
// 
// Started on  Sat Jun  1 15:00:58 2013 jean grizet
// Last update Sat Jun  8 19:34:30 2013 jean grizet
//

#include "GameSound.hh"

GameSound::GameMusic::GameMusic(const std::string& file) :
  _file(file), _run(true), _volume(100)
{
  _run = this->OpenFromFile(file);
  this->SetPitch(1);
  this->SetVolume(100);
  this->SetLoop(true);
}

GameSound::GameMusic::~GameMusic() 
{}

void	GameSound::GameMusic::play() 
{
  if (_run)
    {
      this->SetVolume(_volume);
      this->Play();
    }
}

void	GameSound::GameMusic::stop()
{
  if (_run)
    this->Stop();
}
void	GameSound::GameMusic::setVolume(int vol) { _volume = vol; }

GameSound::GameSound() :
  _vol_mu(100), _vol_eff(100)
{
  _run = true;
}

GameSound::~GameSound() 
{
  _effect[0]->Stop();
  delete(_effect[0]);
  _effect[1]->Stop();
  delete(_effect[1]);
  _music[0]->Stop();
  delete(_music[0]);
  _music[1]->Stop();
  delete(_music[1]);
}

void          GameSound::init(void) 
{
  sf::SoundBuffer	*buff;

  buff = new sf::SoundBuffer();
  if (!buff->LoadFromFile("data/sound/effect/explosion.wav"))
    _run =false;
  _effect.push_back(new sf::Sound(*buff));
  buff = new sf::SoundBuffer();
  if (!buff->LoadFromFile("data/sound/effect/bonus.wav"))
    _run = false;
  _effect.push_back(new sf::Sound(*buff));
  buff = new sf::SoundBuffer();
  if (!buff->LoadFromFile("data/sound/effect/walk1.wav"))
    _run = false;
  _effect.push_back(new sf::Sound(*buff));
  buff = new sf::SoundBuffer();
  if (!buff->LoadFromFile("data/sound/effect/walk2.wav"))
    _run = false;
  _effect.push_back(new sf::Sound(*buff));
  buff = new sf::SoundBuffer();
  if (!buff->LoadFromFile("data/sound/effect/gong.wav"))
    _run = false;
  _effect.push_back(new sf::Sound(*buff));
  _music.push_back(new GameMusic("data/sound/music/game.wav"));
  _music.push_back(new GameMusic("data/sound/music/Best Techno 2009.wav"));
}

void          GameSound::playEffect(eEffectID id) 
{ 
  if (_run)
    {
      _effect[id]->SetVolume(_vol_eff);
      _effect[id]->Play();
    }
}

void          GameSound::stopEffect(eEffectID id)
{ _effect[id]->Stop(); }

void          GameSound::playMusic(eMusicID id)
{ 
  _music[id]->setVolume(_vol_mu);
  _music[id]->play();
}

void          GameSound::stopMusic(eMusicID id)
{ _music[id]->Stop(); }

void		GameSound::setVolumeMusic(int vol)
{ 
  _vol_mu = vol; 
}

void		GameSound::setVolumeEffect(int vol)
{  _vol_eff = vol; }

int		GameSound::getVolumeMusic(void) const
{  return (_vol_mu); }

int		GameSound::getVolumeEffect(void) const
{  return (_vol_eff); }
