//
// Bomber.cpp for  in /home/grizet_j//C++/bomberman/src
// 
// Made by jean grizet
// Login   <grizet_j@epitech.net>
// 
// Started on  Mon Apr 29 12:15:19 2013 jean grizet
// Last update Sun Jun  9 16:44:41 2013 brian grillard
//

#include "LoopEdit.hh"

LoopEdit::LoopEdit(Option& opt, std::string const & path, gdl::GameClock& gc, gdl::Input &in) :
  _option(opt), _path(path), gameClock_(gc), input_(in)
{
  _map = new MapEditor;
  _updateTime = 0.0;
  _drawTime = 0.0;
  _back = new gdl::Image(gdl::Image::load("data/assets/mars.jpg"));
  _viseur = NULL;
  _loaded = false;
  _end = true;
}

LoopEdit::~LoopEdit()
{
}

void  LoopEdit::initialize(void)
{
  _loaded = true;
  _end = false;
  _cam.initialize(_option);
  _map->initializeMap(_option.getSizeX(), _option.getSizeY());
  this->_viseur = new Viseur(0, 0, this->_map);
  this->_map->getMap()[0][0]->setViseur(this->_viseur);
}

void  LoopEdit::update(void)
{
  if (input_.isKeyDown(gdl::Keys::Return) == true)
    {
      std::string	file("./data/map/map");
      SaveMap       sm;
      file.append(_path);
      file.append(".map");
      sm.recordMap(file, _map);
    }
  if (input_.isKeyDown(gdl::Keys::Escape) == true)
    _end = true;
  _viseur->update(gameClock_, input_);
}

void	LoopEdit::drawBack()
{
  glPushMatrix();
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  glDisable(GL_DEPTH_TEST);
  glOrtho(0.0f, _option.getScreenX(), _option.getScreenY(), 0.0, 0.0, 1.0);
  _back->bind();
  glBegin(GL_QUADS);
  glTexCoord2f(0.0, 0.0);
  glVertex3f(0.0, 0.0, 0.0);
  glTexCoord2f(1.0, 0.0);
  glVertex3f(1200.0, 0.0, 0.0);
  glTexCoord2f(1.0, 1.0);
  glVertex3f(1200.0, 1000.0, 0.0);
  glTexCoord2f(0.0, 1.0);
  glVertex3f(0.0, 1000.0, 0.0);
  glEnd();
  glPopMatrix();
}

void  LoopEdit::draw(void)
{
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
  glClearDepth(1.0f);

  int	x = (_viseur->position_.x);
  int	y = _viseur->position_.y;
  int	z =  (_viseur->position_.z);

  _cam.setPosition(x, y + 3200.0f, z + 2000.0f);
  _cam.setLook(x, y, z);
  _cam.update(gameClock_, input_, 0);
  drawBack();
  _cam.setMode();
  _map->draw();
}

void  LoopEdit::unload(void)
{
  _loaded = false;
  delete (_viseur);
}

bool	LoopEdit::isLoaded() const
{ return (_loaded); }

bool	LoopEdit::isEnded() const
{ return (_end); }

void	LoopEdit::setName(const std::string& name)
{ _path = name; }
