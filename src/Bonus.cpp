//
// Bomberman.cpp for  in /home/grizet_j//C++/bomberman/src
// 
// Made by jean grizet
// Login   <grizet_j@epitech.net>
// 
// Started on  Mon Apr 29 12:27:58 2013 jean grizet
// Last update Sat Jun  8 15:18:16 2013 jean grizet
//

#include "Bonus.hh"

Bonus::Bonus(void) :
  _texture(new gdl::Image(gdl::Image::load("data/assets/box2.jpg")))
{
  _type = 4;
  _end = false;
}

Bonus::Bonus(gdl::Image *t, int ty) :
  _texture(t),
  _type(ty)
{  
  _end = false;
}

Bonus::~Bonus(void)
{}

void Bonus::initialize(void)
{
  position_.x = 200.0;
  position_.y = 0.0;
  position_.z = 0.0;
}

void Bonus::update(gdl::GameClock const & gameClock, gdl::Input & input)
{
  (void)gameClock;
  (void)input;
}

void	Bonus::setEnd(int end) { this->_end = end; }
int	Bonus::getEnd() const { return (this->_end); }

void	Bonus::drawVertex()
{
  _texture->bind();
  glBegin(GL_TRIANGLES);

  //Front Left Face
  glTexCoord2f(0.5, 0.0);
  glVertex3f(0.0f, 0.0, 0.0);
  glTexCoord2f(0.3, 1.0f);
  glVertex3f(0.0f, 225.0f, 175.0);
  glTexCoord2f(0.6f, 1.0f);
  glVertex3f(-175.0, 225.0f, 0.0f);

  glTexCoord2f(0.5, 0.0);
  glVertex3f(0.0f, 450.0f, 0.0);
  glTexCoord2f(0.3, 1.0);
  glVertex3f(0.0f, 225.0, 175.0f);
  glTexCoord2f(0.6f, 1.0f);
  glVertex3f(-175.0, 225.0, 0.0f);

  //Front Right Face
  glTexCoord2f(0.5, 0.0);
  glVertex3f(0.0f, 0.0, 0.0f);
  glTexCoord2f(0.3, 1.0f);
  glVertex3f(0.0f, 225.0f, 175.0);
  glTexCoord2f(0.6f, 1.0f);
  glVertex3f(175.0f, 225.0f, 0.0);

  glTexCoord2f(0.5, 0.0);
  glVertex3f(0.0f, 450.0f, 0.0f);
  glTexCoord2f(0.3, 1.0f);
  glVertex3f(0.0f, 225.0f, 175.0f);
  glTexCoord2f(0.6f, 1.0f);
  glVertex3f(175.0f, 225.0f, 0.0);

  //Back Left Face
  glTexCoord2f(0.5, 0.0);
  glVertex3f(0.0f, 0.0, 0.0);
  glTexCoord2f(0.3, 1.0f);
  glVertex3f(0.0f, 225.0f, -175.0);
  glTexCoord2f(0.6f, 1.0f);
  glVertex3f(-175.0, 225.0f, 0.0f);

  glTexCoord2f(0.5, 0.0);
  glVertex3f(0.0f, 450.0f, 0.0f);
  glTexCoord2f(0.3, 1.0f);
  glVertex3f(0.0f, 225.0f, -175.0f);
  glTexCoord2f(0.6f, 1.0f);
  glVertex3f(-175.0, 225.0f, 0.0f);

  //Back Right Face
  glTexCoord2f(0.5, 0.0);
  glVertex3f(0.0f, 0.0, 0.0f);
  glTexCoord2f(0.3, 1.0f);
  glVertex3f(0.0f, 225.0f, -175.0);
  glTexCoord2f(0.6f, 1.0f);
  glVertex3f(175.0f, 225.0f, 0.0);

  glTexCoord2f(0.5, 0.0);
  glVertex3f(0.0f, 450.0f, 0.0f);
  glTexCoord2f(0.3, 1.0f);
  glVertex3f(0.0f, 225.0f, -175.0f);
  glTexCoord2f(0.6f, 1.0f);
  glVertex3f(175.0f, 225.0f, 0.0);

  glEnd();
}

void		Bonus::draw(void)
{
  glPushMatrix();
  glLoadIdentity();

  glTranslatef(position_.x + 175.0f, position_.y, position_.z + 175.0f);
  rotation_.y += 1;
  glRotatef(this->rotation_.x, 0.1f, 0.0f, 0.0f);
  glRotatef(this->rotation_.y, 0.0f, 1.0f, 0.0f);
  glRotatef(this->rotation_.z, 0.0f, 0.0f, 1.0f);
  drawVertex();
  glPopMatrix();
}


int		Bonus::getType() const
{ return (_type); }

AObject		*Bonus::getReplace() const
{ return (NULL); }

IEvent        *Bonus::createEvent(gdl::GameClock const & gameClock,	\
				gdl::Input & input)
{
  return (new Event(this, gameClock, input));
}
