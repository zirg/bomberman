//
// Option.cpp for  in /home/grizet_j//C++/bomberman/src
// 
// Made by jean grizet
// Login   <grizet_j@epitech.net>
// 
// Started on  Fri May 31 15:51:37 2013 jean grizet
// Last update Thu Jun  6 08:04:56 2013 jean grizet
//

#include "Option.hh"

Option::Option() :
  _screenX(0), _screenY(0), _sizeX(0), _sizeY(0), _player(0), _ia(0),
  _j1(*new std::map<int, gdl::Keys::Key>),
  _j2(*new std::map<int, gdl::Keys::Key>)
{
  _time = 60;
  this->_j1.insert(std::pair<int, gdl::Keys::Key>(0, gdl::Keys::Z));
  this->_j1.insert(std::pair<int, gdl::Keys::Key>(1, gdl::Keys::S));
  this->_j1.insert(std::pair<int, gdl::Keys::Key>(2, gdl::Keys::D));
  this->_j1.insert(std::pair<int, gdl::Keys::Key>(3, gdl::Keys::Q));
  this->_j1.insert(std::pair<int, gdl::Keys::Key>(4, gdl::Keys::Space));
  this->_j2.insert(std::pair<int, gdl::Keys::Key>(0, gdl::Keys::Up));
  this->_j2.insert(std::pair<int, gdl::Keys::Key>(1, gdl::Keys::Down));
  this->_j2.insert(std::pair<int, gdl::Keys::Key>(2, gdl::Keys::Right));
  this->_j2.insert(std::pair<int, gdl::Keys::Key>(3, gdl::Keys::Left));
  this->_j2.insert(std::pair<int, gdl::Keys::Key>(4, gdl::Keys::Numpad0));
  _typeMap = "default";
}

Option::~Option() {}

void  Option::setScreenX(int v) { _screenX = v; }
void  Option::setScreenY(int v) { _screenY = v; }
void  Option::setSizeX(int v) { _sizeX = v; }
void  Option::setSizeY(int v) { _sizeY = v; }
void  Option::setIA(int v) { _ia = v; }
void  Option::setPlayer(int v) { _player = v; }
void  Option::setMode(int v) { _mode = v; }
void  Option::setTime(int v) { _time = v * 60; }
void  Option::setTypeMap(std::string& typeMap) {_typeMap = typeMap; }

int   Option::getScreenX() const { return (_screenX); }
int   Option::getScreenY() const  { return (_screenY); }
int   Option::getSizeX() const  { return (_sizeX); }
int   Option::getSizeY() const  { return (_sizeY); }
int   Option::getIA() const  { return (_ia); }
int   Option::getPlayer() const  { return (_player); }
int   Option::getMode() const { return (_mode); }
time_t   Option::getTime() const { return (_time); }
std::string	Option::getTypeMap() const {return _typeMap; }
std::map<int, gdl::Keys::Key>&  Option::getKeyJ1() { return (_j1); }
std::map<int, gdl::Keys::Key>&  Option::getKeyJ2() { return (_j2); }
