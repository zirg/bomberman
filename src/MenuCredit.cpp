//
// NewGame.cpp for NewGame in /home/lecoq_j//Tech2/ProjetC++/Bomberman/src/menu
//
// Made by jean-charles lecoq
// Login   <lecoq_j@epitech.net>
// 
// Started on  Wed May  8 22:51:46 2013 jean-charles lecoq
// Last update Sun Jun  9 17:56:47 2013 jean grizet
//

#include	"MenuCredit.hh"
#include	<iostream>

MenuCredit::MenuCredit(Texture& texture)
  :_texture(texture)
{ }

MenuCredit::~MenuCredit()
{ }

void	MenuCredit::initialize()
{
  /* au fond */
  x1Title_ = -2900;
  x2Title_ = -1970;
  y1Title_ = 240.5;
  y2Title_ = 570.5;
  z1Title_ = -1970;
  z2Title_ = -2900;

  /* cordonne background*/
  x1_ = -3185;
  x2_ = -1785;
  y1_ = -600;
  y2_ = 600;
  z1_ = -1785;
  z2_ = -3185;
}


void	MenuCredit::update(gdl::GameClock const & gameClock, gdl::Input &input)
{
  (void)input;
  (void)gameClock;
}


void	MenuCredit::draw()
{
  glPushMatrix();
  glLoadIdentity();
  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
  _texture.bindTexture(Texture::CREDIT);
  glBegin(GL_QUADS);
  glTexCoord2f(0.0, 0.0);
  glVertex3f(x1_, y2_, z1_);
  glTexCoord2f(0.0, 1.0f);
  glVertex3f(x1_, y1_, z1_);
  glTexCoord2f(1.0f, 1.0f);
  glVertex3f(x2_, y1_, z2_);
  glTexCoord2f(1.0f, 0.0);
  glVertex3f(x2_, y2_, z2_);
  glEnd();
}

void    MenuCredit::drawTitle()
{

  glPushMatrix();
  glLoadIdentity();
  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
  _texture.bindTexture(Texture::TITLECRE);
  glBegin(GL_QUADS);
  glTexCoord2f(0.0, 0.0);
  glVertex3f(x1Title_, y2Title_, z1Title_);
  glTexCoord2f(0.0, 1.0f);
  glVertex3f(x1Title_, y1Title_, z1Title_);
  glTexCoord2f(1.0f, 1.0f);
  glVertex3f(x2Title_, y1Title_, z2Title_);
  glTexCoord2f(1.0f, 0.0);
  glVertex3f(x2Title_, y2Title_, z2Title_);
  glEnd();

  glDisable(GL_BLEND);
  glDisable(GL_TEXTURE_2D);
}

void    MenuCredit::zoomTitle(float x1, float x2, float y1, float y2, float z1, float z2)
{
  x1Title_ += x1;
  x2Title_ += x2;
  y1Title_ += y1;
  y2Title_ += y2;
  z1Title_ += z1;
  z2Title_ += z2;
}
