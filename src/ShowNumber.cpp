 //
// ShowNumber.cpp for ShowNumber in /home/lecoq_j//Dropbox/ProjetTech2/Bomberman/src
// 
// Made by jean-charles lecoq
// Login   <lecoq_j@epitech.net>
// 
// Started on  Sat May 25 14:32:31 2013 jean-charles lecoq
// Last update Sat Jun  8 21:42:32 2013 jean grizet
//

#include	"ShowNumber.hh"

ShowNumber::ShowNumber(Texture& texture)
  :_texture(texture)
{
}

ShowNumber::~ShowNumber()
{}

void	ShowNumber::initialize(int nbMax, float xFirst, float yFirst, float zFirst,
			       float lenx, float leny, float lenz, int dir)
{
  i_ = 0;

  number_ = 0;
  nbMax_ = nbMax;
  while (i_ < nbMax_)
    {
      plan_.push_back(new Plan(_texture));
      i_++;
    }

  std::vector<Plan*>::iterator	it;
  for (it = plan_.begin(); it != plan_.end(); ++it)
    {
      (*it)->initialize(xFirst, xFirst + lenx, yFirst, yFirst + leny, zFirst, zFirst + lenz);
      if (dir == 1)
        xFirst += lenx;
      if (dir == 2)
        yFirst += leny;
      if (dir == 3)
        zFirst += lenz;
    }
  for (it = plan_.begin(); it != plan_.end(); ++it)
    (*it)->update(_linkTexture.getNumber(0));
}


void	ShowNumber::update(int number)
{
  int	 i = 1;
  int	puissance;
  std::ostringstream  puis;
  std::string         p;
  puis << 1;
  while (i < nbMax_)
    {
      puis << 0;
      i++;
    }
  p.assign(puis.str());
  puissance = atoi(p.c_str());

  int nb;
  std::vector<Plan*>::iterator	it;
  for (it = plan_.begin(); it != plan_.end(); ++it)
    {
      nb = number / puissance;
      (*it)->update(_linkTexture.getNumber(nb));
      number = number % puissance;
      puissance /= 10;
    }
}

void	ShowNumber::draw()
{
  std::vector<Plan*>::iterator	it;

  for (it = plan_.begin(); it != plan_.end(); ++it)
      (*it)->draw();
}
