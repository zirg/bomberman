//
// Model.cpp for bomberman in /home/grilla_b//Documents/epi/projet_tek2/boomberman/repo/bomberman/src
// 
// Made by brian grillard
// Login   <grilla_b@epitech.net>
// 
// Started on  Mon May 27 11:20:30 2013 brian grillard
// Last update Sun Jun  9 16:53:19 2013 jean grizet
//

#include "Model.hh"
#include <iostream>

namespace Model
{
  Bomberman::Bomberman(int player, int posX, int posY,			\
		       gdl::Keys::Key up, gdl::Keys::Key down,		\
		       gdl::Keys::Key right, gdl::Keys::Key left,	\
		       gdl::Keys::Key shoot, Map *map, GameSound& gs, bool load) :
    _sound(gs), _load(load)
  {
    this->_player = player;
    this->_posX = posX;
    this->_posY = posY;

    this->_shoot = shoot;
    this->_key.insert( std::pair<gdl::Keys::Key, eDirection>(up, UP) );
    this->_key.insert( std::pair<gdl::Keys::Key, eDirection>(down, DOWN) );
    this->_key.insert( std::pair<gdl::Keys::Key, eDirection>(right, RIGHT) );
    this->_key.insert( std::pair<gdl::Keys::Key, eDirection>(left, LEFT) );

    this->_map = map;
    this->_push[LEFT] = &Bomberman::keyLeft;
    this->_push[RIGHT] = &Bomberman::keyRight;
    this->_push[UP] = &Bomberman::keyUp;
    this->_push[DOWN] = &Bomberman::keyDown;
    this->_bagBomb = new BagOfBomb(player, _sound);
    this->_bagBomb->addBomb(6);
  }

  Bomberman::Bomberman(int player, int posX, int posY,	\
		       Map *map, GameSound& gs, bool load) :
    _sound(gs), _load(load)
  {
    _updateEvent = NULL;
    this->_player = player;
    this->_posX = posX;
    this->_posY = posY;
    this->_map = map;
    this->_push[LEFT] = &Bomberman::keyLeft;
    this->_push[RIGHT] = &Bomberman::keyRight;
    this->_push[UP] = &Bomberman::keyUp;
    this->_push[DOWN] = &Bomberman::keyDown;
    this->_bagBomb = new BagOfBomb(player, _sound);
    this->_bagBomb->addBomb(6);
  }

  Bomberman::~Bomberman(void) 
  { 
    delete (_bagBomb);
    delete (model_);
  }

  void			Bomberman::initialize()
  {
    int  r = rand() % 255;
    int  g = rand() % 255;
    int  b = rand() % 255;
    gdl::Color	c(r,g,b);

    _end = 0;
    _speed = 0.0330;
    _animSpeed = 1.22;
    _updateEvent = NULL;
    position_.x = _posX * this->_sizeCase + (this->_sizeCase / 2);
    position_.y = 0;
    position_.z = _posY * this->_sizeCase + (this->_sizeCase / 2);
    rotation_.x = 0.0f;
    rotation_.y = 0.1f;
    rotation_.z = 0.0f;
    this->_timeInput = 0.01;
    this->_move = 0;
    this->_map->getMap()[_posY][_posX]->setBomberman(this);
    this->_map->getMap()[_posY][_posX]->setObject(NULL);
    if (_load == 0)
      {
	this->_map->getMap()[_posY][_posX + 1]->setObject(NULL);
	this->_map->getMap()[_posY][_posX + 1]->setWalk(0);
	this->_map->getMap()[_posY + 1][_posX]->setObject(NULL);
	this->_map->getMap()[_posY + 1][_posX]->setWalk(0);
      }
    this->_xD = 0;
    this->_yD = 0;
    this->_aD = 0;
    _death = 0;
    _maxBomb = 1;
    _pourcent = 0.0;
    this->_map->getMap()[this->_posY][this->_posX]->setWalk(this->_player);
    this->model_ = new gdl::Model(gdl::Model::load("data/assets/marvin.fbx"));
    gdl::Model::cut_animation(*(this->model_), "Take 001", 35, 54, "run");
    gdl::Model::cut_animation(*(this->model_), "Take 001", 75, 120, "stop"); 
    this->model_->set_default_model_color(c);
  }

  bool			Bomberman::checkSizeMoveHori(Case *cs)
  {
    if (this->_direction == UP || this->_direction == DOWN)
      {
	if (cs->getX() > 0)
	  {
	    Case *temp = this->_map->getMap()[cs->getY()][cs->getX() - 1];
	    if ((((position_.x - this->_posMiddle) < cs->getWestLim()) && \
		 (temp->canWalk() != 0)) || (cs->getX() > this->_map->getsizeX()))
	      return (false);
	  }
	if (cs->getX() < this->_map->getsizeX())
	  {
	    Case *temp = this->_map->getMap()[cs->getY()][cs->getX() + 1];
	    if (((position_.x + this->_posMiddle) > cs->getEstLim()) && (temp->canWalk() != 0))
	      return (false);
	  }
      }
    return (true);
  }

  bool			Bomberman::checkSizeMoveVerti(Case *cs)
  {
    if (this->_direction == LEFT || this->_direction == RIGHT)
      {
	if (cs->getY() > 0)
	  {
	    Case *temp = this->_map->getMap()[cs->getY() - 1][cs->getX()];
	    if (((position_.z - this->_posMiddle) < cs->getNorthLim() &&
		(temp->canWalk() != 0)) || (cs->getY() > this->_map->getsizeY()))
		return (false);
	  }
	if (cs->getY() < this->_map->getsizeY())
	  {
	    Case *temp = this->_map->getMap()[cs->getY() + 1][cs->getX()];
	    if ((position_.z + this->_posMiddle) > cs->getSouthLim() && (temp->canWalk() != 0))
	      return (false);
	  }
      }
    return (true);
  }

  void			Bomberman::checkBonus(Case *cs)
  {
    int			type = cs->getIdBonus();

    if (type == 3)
      {
	if (this->_speed > 0.0248)
	  {
	    this->_speed -= 0.0006;
	    this->_pourcent += 1.0;
	    this->_animSpeed += 0.018;
	  }
	cs->setObject(NULL);
	cs->setIdBonus(0);
	_sound.playEffect(GameSound::BONUS);
      }
    else if (type == 4)
      {
	if (this->_maxBomb < 6)
	  this->_maxBomb += 1;
	cs->setObject(NULL);
	cs->setIdBonus(0);
	_sound.playEffect(GameSound::BONUS);
      }
    else if (type == 5)
      {
	this->_bagBomb->addPower(1);
	cs->setObject(NULL);
	cs->setIdBonus(0);
	_sound.playEffect(GameSound::BONUS);
      }
  }

  bool			Bomberman::changePosInMap(int incrX, int incrY)
  {
    Case		*temp = this->_map->getMap()[this->_posY + incrY][this->_posX + incrX];
    Case		*now = this->_map->getMap()[this->_posY][this->_posX];

    if (temp->canWalk() != 0)
      return (false);
    if ((this->checkSizeMoveHori(temp) == false) || 
	(this->checkSizeMoveVerti(temp) == false))
	return (false);
    if ((now->canWalk()) != 6)
      now->setWalk(0);
    now->setBomberman(NULL);
    this->_posX += incrX;
    this->_posY += incrY;
    temp->setBomberman(this);
    temp->setWalk(this->_player);
    this->checkBonus(temp);
    return (true);  
  }

  bool			Bomberman::checkLimEst(int incr, Case *cs)
  {
    float		lim = cs->getEstLim();

    if ((this->_posX + incr > this->_map->getsizeX()) &&
	(position_.x + this->_posMiddle) >= lim)
      return (false);
    if ((position_.x + this->_posMiddle) >= lim)
      return (changePosInMap(incr, 0));
    return (true);
  }

  bool			Bomberman::checkLimWest(int incr, Case *cs)
  {
    float		lim = cs->getWestLim();

    if ((this->_posX + incr < 0) && 
	(position_.x - this->_posMiddle) <= lim)
	return (false);
    if ((position_.x - this->_posMiddle) <= lim)
      return (changePosInMap(incr, 0));
    return (true);
  }

  bool			Bomberman::checkLimNorth(int incr, Case *cs)
  {
    float		lim = cs->getNorthLim();

    if ((this->_posY + incr < 0) && 
	(position_.z - this->_posMiddle) <= lim)
      return (false);
    if ((position_.z - this->_posMiddle) <= lim)
      return (changePosInMap(0, incr));
    return (true);
  }

  bool			Bomberman::checkLimSouth(int incr, Case *cs)
  {
    float		lim = cs->getSouthLim();

    if ((this->_posY + incr > this->_map->getsizeY()) && 
	(position_.z + this->_posMiddle) >= lim)
      return (false);
    if ((position_.z + this->_posMiddle) >= lim)
      return (changePosInMap(0, incr));
    return (true);
  }

  //Key functions change direction, position and angles 

  void			Bomberman::keyRight(Case *cs)
  {
    this->_direction = RIGHT;
    this->_angle = 90;
    if (this->checkLimEst(1, cs) == false)
      return;
    position_.x += this->_posMiddle;
  }

  void			Bomberman::keyLeft(Case *cs)
  {
    this->_direction = LEFT;
    this->_angle = 270;
    if (this->checkLimWest(-1, cs) == false)
      return;
    position_.x -= this->_posMiddle;
  }

  void			Bomberman::keyUp(Case *cs)
  {
    this->_direction = UP;
    this->_angle = 180;
    if (this->checkLimNorth(-1, cs) == false)
      return;
    position_.z -= this->_posMiddle;
  }

  void			Bomberman::keyDown(Case *cs)
  {
    this->_direction = DOWN;
    this->_angle = 0;
    if (this->checkLimSouth(1, cs) == false)
      return;
    position_.z += this->_posMiddle;
  }
  //play functions play animation

  void			Bomberman::playRun()
  {
    _move = 1;
    this->model_->play("run", gdl::Anim::RUN);
  }

  void			Bomberman::playStop()
  {
    _move = 0;
    this->model_->play("run", gdl::Anim::STOP);
    this->model_->play("stop");
  }

  void			Bomberman::goMove(eDirection touch,
					  gdl::GameClock const & gameClock)
  {
    keyPush		ptr;

    if (touch >= 4 || touch < 0)
      return;
    ptr = NULL;
    if (this->checkTimeInput(gameClock, _speed) == true)
      {
	this->model_->play("stop", gdl::Anim::STOP);
	ptr = this->_push[touch];
	if (ptr != NULL)
	  {
	    this->playRun();
	    (this->*ptr)(this->_map->getMap()[this->_posY][this->_posX]);
	  }
      }
  }

  bool			Bomberman::checkTimeInput(gdl::GameClock const & gameClock,
						  double timeMax)
  {
    float		time;

    time = gameClock.getTotalGameTime();
    if ((time - this->_timeInput) >= timeMax)
      {
	this->_timeInput = time;
	return (true);
      }
    return (false);
  }

  void			Bomberman::moveBomberman(gdl::GameClock const & gameClock,
						 gdl::Input & input)
  {
    eDirection		touch = NIL;

    for (std::map<gdl::Keys::Key, eDirection>::iterator 
	   it = this->_key.begin(); it != this->_key.end(); ++it)
      if (input.isKeyDown(it->first) == true)
	touch = it->second;
    if (touch != NIL)
      this->goMove(touch, gameClock);
    if (_move == 1 && touch == NIL)
      this->playStop();
  }

  bool			Bomberman::Death()
  {
    if (this->_death == 2)
      return (false);
    if ((this->_map->getMap()[_posY][_posX]->getDeath() == 1) &&	\
	this->_end != 2)
      this->_death = 1;
    if (this->_death == 1)
      this->makeDeath();
    return (true);
  }

  void			Bomberman::inputBomb(gdl::GameClock const & gameClock,
					     gdl::Input & input)
  {
    if ((input.isKeyDown(this->_shoot) == true) && 
	(checkTimeInput(gameClock, 0.07) == true) && (this->_bagBomb->getNbon() < this->_maxBomb))
      {
	this->putBomb(gameClock.getTotalGameTime(), true);
      }
  }

  void			Bomberman::update(gdl::GameClock const & gameClock, 
					  gdl::Input & input)
  {
    this->model_->set_anim_speed("run", _animSpeed);
    this->model_->set_anim_speed("stop", 1.4);
    if (this->Death() == false)
      return;
    if (this->_death != 1)
      {
	this->model_->update(gameClock);
	this->inputBomb(gameClock, input);
	this->moveBomberman(gameClock, input);
      }
  }

  void			Bomberman::makeDeath()
  {
    this->_yD += 100;
    this->_aD += 10;
    if (position_.y + this->_yD >= 5000)
      {
	this->_map->getMap()[_posY][_posX]->setWalk(0);
	this->_map->getMap()[_posY][_posX]->setBomberman(NULL);
	this->_death = 2;
      }
  }

  bool			Bomberman::putBomb(double time, bool pl)
  {
    int			type;
    int			pos;
    Case		*cs = this->_map->getMap()[this->_posY][this->_posX];

    pos = 0;
    (void)time;
    if (this->_bagBomb->getNbon() >= this->_maxBomb)
      return (false);
    if (this->_bagBomb->getNbon() == this->_maxBomb && pl == false)
      return (false);
    if ((pos = this->_bagBomb->getPosFree()) == -1)
      return (false);
    type = this->_map->getMap()[this->_posY][this->_posX]->canWalk();
    if ((type != 0) && (type != this->_player))
      return (false);
    this->_bagBomb->getBag()[pos]->initBomb(_map, _posY, _posX, time, this->_bagBomb, pos);
    this->_bagBomb->increaseNbon(1);
    this->_bagBomb->changeStatePosBomb(pos);
    cs->setObject(this->_bagBomb->getBag()[pos]);
    cs->setWalk(6);
    return (true);
  }

  void			Bomberman::draw(void)
  {
    if (this->_end == 2)
      return ;
    glPushMatrix();
    glLoadIdentity();
    glTranslatef(position_.x, position_.y + this->_yD, position_.z);
    glRotatef(this->_angle + this->_aD, rotation_.x, rotation_.y + 0.1f, rotation_.z);
    this->model_->draw();
    glPopMatrix();
  }

  AObject		*Bomberman::getReplace() const
  { return (NULL); }

  IEvent		*Bomberman::createEvent(gdl::GameClock const & gameClock, \
                                        gdl::Input & input)
  {
    if (_updateEvent == NULL)
      _updateEvent = new Event(this, gameClock, input);
    return (_updateEvent);
  }

  int			Bomberman::getEnd() const {return (this->_end); };
  void			Bomberman::setEnd(int end) { this->_end = end; };
  gdl::Model*		Bomberman::getModel() const { return (this->model_); }
  BagOfBomb*		Bomberman::getBagOfBomb() const { return (this->_bagBomb); }
  int			Bomberman::getPoints() const { return (this->_bagBomb->getPoints()); }
  float			Bomberman::getRealSpeed() const { return (this->_speed); }
  int			Bomberman::getSpeed() const 
  {
    int	val = ((_pourcent / 13) * 100);
    if (val > 100)
      val = 100;
    return (val);
  }
  
  bool			Bomberman::isDead() const
  { return (_death == 2 ? true : false); }

  int			Bomberman::getMaxBomb() const { return (this->_maxBomb); }
  int			Bomberman::getPower() const { return this->_bagBomb->getPower(); }

  void			Bomberman::setSpeed(float speed) { this->_speed = speed; }
  void			Bomberman::setAnimSpeed(float speed) { this->_animSpeed = speed; }
  void			Bomberman::setMaxBomb(int nbr) { this->_maxBomb = nbr; }
  void			Bomberman::setPower(int nbr) { this->_bagBomb->setPower(nbr); }
  float			Bomberman::getAnimSpeed() const { return (this->_animSpeed); }
  float			Bomberman::getPourcent() const { return (this->_pourcent); }
  void			Bomberman::setPourcent(float pourcent) { this->_pourcent = pourcent; }
  void			Bomberman::setPoints(int points) {this->_bagBomb->setPoints(points); }
}
