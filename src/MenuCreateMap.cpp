//
// NewGame.cpp for NewGame in /home/lecoq_j//Tech2/ProjetC++/Bomberman/src/menu
//
// Made by jean-charles lecoq
// Login   <lecoq_j@epitech.net>
// 
// Started on  Wed May  8 22:51:46 2013 jean-charles lecoq
// Last update Sun Jun  9 16:56:45 2013 jean-charles lecoq
//

#include	"MenuCreateMap.hh"
#include	<iostream>

MenuCreateMap::MenuCreateMap(Option& opt, Texture& texture)
  :_texture(texture), option_(opt)
{ }

MenuCreateMap::~MenuCreateMap()
{ }

void	MenuCreateMap::initialize()
{
  

  time_ = 0.0;
  isHeight_ = true;
  
  height_ = 15;
  width_ = 15;

  option_.setSizeY(height_);
  option_.setSizeX(width_);
  showHeight_ = new ShowNumber(_texture);
  showWidth_ = new ShowNumber(_texture);

  showHeight_->initialize(3, -3480, -65, 470, 0, 60, -60, 3);
  showWidth_->initialize(3, -3480, -65,  -290, 0, 60, -60, 3);

  showHeight_->update(height_);
  showWidth_->update(width_);

  x1Title_ = -3400;
  x2Title_ = -3400;
  y1Title_ = 240.5;
  y2Title_ = 570.5;
  z1Title_ = 700;
  z2Title_ = -700;

  x1_ = -3480;
  x2_ = -3480;
  y1_ = -600;
  y2_ = 600;
  z1_ = 990;
  z2_ = -990; 
}

void	MenuCreateMap::upDown(int dir)
{
  if (isHeight_ == true)
    {
      height_ += 1 * dir;
      if (height_ == 121)
	height_ = 15;
      if (height_ == 14)
	height_ = 120;
      option_.setSizeY(height_);
      showHeight_->update(height_);
    }
  else
    {
      width_ += 1 * dir;
      if (width_ == 121)
	width_ = 15;
      if (width_ == 14)
	width_ = 120;
      option_.setSizeX(width_);
      showWidth_->update(width_);
    }
}

void	MenuCreateMap::update(gdl::GameClock const & gameClock, gdl::Input &input)
{
  if (time_ > gameClock.getTotalGameTime())
    return;
  if ((input.isKeyDown(gdl::Keys::Left) == true) || (input.isKeyDown(gdl::Keys::Right) == true))
    {
      if (isHeight_ == true)
	isHeight_ = false;
      else
	isHeight_ = true;
      time_ = gameClock.getTotalGameTime() + 0.15;
    }
  if (input.isKeyDown(gdl::Keys::Up) == true)
    {
      upDown(1);
      time_ = gameClock.getTotalGameTime() + 0.15;
    }
  if (input.isKeyDown(gdl::Keys::Down) == true)
    {
      upDown(-1);
      time_ = gameClock.getTotalGameTime() + 0.15;
    }
}

void	MenuCreateMap::draw()
{
  glPushMatrix();
  glLoadIdentity();
  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
  if (isHeight_ == true)
    _texture.bindTexture(Texture::CPHEIGHT);
  else
    _texture.bindTexture(Texture::CPWIDTH);
  glBegin(GL_QUADS);
  glTexCoord2f(0.0, 0.0);
  glVertex3f(x1_, y2_, z1_);
  glTexCoord2f(0.0, 1.0f);
  glVertex3f(x1_, y1_, z1_);
  glTexCoord2f(1.0f, 1.0f);
  glVertex3f(x2_, y1_, z2_);
  glTexCoord2f(1.0f, 0.0);
  glVertex3f(x2_, y2_, z2_);
  glEnd();
  glDisable(GL_BLEND);
  glDisable(GL_TEXTURE_2D);
  showHeight_->draw();
  showWidth_->draw();
}


void    MenuCreateMap::zoomTitle(float x1, float x2, float y1, float y2, float z1, float z2)
{
  x1Title_ += x1;
  x2Title_ += x2;
  y1Title_ += y1;
  y2Title_ += y2;
  z1Title_ += z1;
  z2Title_ += z2;
}

void    MenuCreateMap::drawTitle()
{

  glPushMatrix();
  glLoadIdentity();
  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
  _texture.bindTexture(Texture::TITLECM);
  glBegin(GL_QUADS);
  glTexCoord2f(0.0, 0.0);
  glVertex3f(x1Title_, y2Title_, z1Title_);
  glTexCoord2f(0.0, 1.0f);
  glVertex3f(x1Title_, y1Title_, z1Title_);
  glTexCoord2f(1.0f, 1.0f);
  glVertex3f(x2Title_, y1Title_, z2Title_);
  glTexCoord2f(1.0f, 0.0);
  glVertex3f(x2Title_, y2Title_, z2Title_);
  glEnd();
  glDisable(GL_BLEND);
  glDisable(GL_TEXTURE_2D);
}
