//
// SaveGame.cpp for bomberman in /home/grilla_b//Documents/epi/projet_tek2/boomberman/repo/bomberman
// 
// Made by brian grillard
// Login   <grilla_b@epitech.net>
// 
// Started on  Thu Jun  6 04:19:16 2013 brian grillard
// Last update Sun Jun  9 14:36:16 2013 jean grizet
//

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include "SaveGame.hh"
#include "SaveMap.hh"

SaveGame::SaveGame()
{
}

SaveGame::~SaveGame()
{
}

bool		SaveGame::open_file(std::ofstream & file, std::string const & path)
{
  file.open(path.c_str(), std::ios::out);
  if (file.bad())
    {
      std::cerr << "fail openning" << std::endl;
      return false;
    }
  return (true);
}

bool		SaveGame::save(std::vector<Model::Bomberman *> player, std::string const & path, Map* map)
{
  int		fd;
  t_save	save;
  int		ret = 0;
  SaveMap	sm;
  std::string ext(path);

  ext.append(".map");
  sm.recordMap(ext, map);
  std::vector<Model::Bomberman *>::iterator it = player.begin();
  if ((fd = open(path.c_str(), O_CREAT | O_RDWR | O_TRUNC, S_IRWXU, S_IRWXO)) == -1)
    if ((fd = open(path.c_str(), O_RDWR | O_TRUNC, S_IRWXU, S_IRWXO)) == -1)
      return (false);
  for (; it != player.end(); ++it)
    {
      if ((*it)->isDead())
	continue;
      save.player = (*it)->getPlayer();
      save.posX = (*it)->getPosX();
      save.posY = (*it)->getPosY();
      save.animSpeed = (*it)->getAnimSpeed();
      save.speed = (*it)->getRealSpeed();
      save.pourcent = (*it)->getPourcent();
      save.maxBomb = (*it)->getMaxBomb();
      save.power = (*it)->getPower();
      save.points = (*it)->getPoints();
      if ((ret = write(fd, &save, sizeof(t_save))) == -1)
	return (false);
    }
  close(fd);
  return (true);
}
