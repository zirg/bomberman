//
// Thread.cpp for  in /home/grizet_j//C++/plazza/src
// 
// Made by jean grizet
// Login   <grizet_j@epitech.net>
// 
// Started on  Mon Apr 15 13:32:44 2013 jean grizet
// Last update Fri Jun  7 17:11:04 2013 jean grizet
//

#include "Consumer.hh"
#include "Thread.hh"
#include <signal.h>

static void		*exec_it(void *arg)
{
  IThread	*e = (IThread *)arg;

  return ((*e)(arg));
}

template <typename T>
Thread<T>::Thread(T* consumer, void *(T::*meth)(void *)) :
  _obj(consumer), _meth(meth) , _status(IThread::INIT)
{}

template <typename T>
Thread<T>::~Thread(void)
{}

template <typename T>
IThread::eStatus       Thread<T>::getStatus() const
{
  return (this->_status);
}

template <typename T>
void          Thread<T>::run()
{
  _status = IThread::RUN;
  if (pthread_create(&_thread, NULL, &exec_it, (void*)this) != 0)
    {
      _status = IThread::INIT;
      std::cerr << PTHREAD_CREATE_ERR << std::endl;
    }
}

template <typename T>
void          Thread<T>::wait(void)
{
  if (pthread_join(_thread, NULL) != 0)
    {
      std::cerr << PTHREAD_JOIN_ERR << std::endl;
      exit(EXIT_FAILURE);
    }
  else
    _status = IThread::DEAD;
}

template <typename T>
int          Thread<T>::cancel(void)
{
  if (pthread_cancel(_thread) != 0)
    {
      std::cerr << PTHREAD_CANCEL_ERR << std::endl;
      exit(EXIT_FAILURE);
    }
  return (0);
}

template <typename T>
void          Thread<T>::kill(void)
{
  if (pthread_kill(_thread, SIGINT) != 0)
    {
      std::cerr << PTHREAD_CANCEL_ERR << std::endl;
      exit(EXIT_FAILURE);
    }
}

template <typename T>
void		*Thread<T>::operator()(void * arg)
{
  return ((*_obj.*_meth)(arg));
}

template Thread<Consumer>::Thread(Consumer*, void *(Consumer::*)(void *));
