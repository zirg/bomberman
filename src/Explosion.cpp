//
// Bomberman.cpp for  in /home/grizet_j//C++/bomberman/src
// 
// Made by jean grizet
// Login   <grizet_j@epitech.net>
// 
// Started on  Mon Apr 29 12:27:58 2013 jean grizet
// Last update Fri Jun  7 18:04:41 2013 jean grizet
//

#include "Explosion.hh"

Explosion::Explosion(void) :
  _smoke(new gdl::Image(gdl::Image::load("data/assets/fire.png"))),
  _explode(false), _end(0)
{
  _max = 0;
}

Explosion::Explosion(gdl::Image *s) :
  _smoke(s),
  _explode(false), _end(0)
{  
  _max = 0;
}

Explosion::~Explosion(void)
{ }

void Explosion::initialize(void)
{
  position_.x = 200.0;
  position_.y = 0.0;
  position_.z = 0.0;
  _max = 0;
  _end = 0;
}

void Explosion::update(gdl::GameClock const & gameClock, gdl::Input & input)
{
  (void)gameClock;
  (void)input;
  _explode = true;
  _max += 0.02;
  if (_max >= 1.2)
    _end = 1;
}

void	Explosion::setEnd(int end) { this->_end = end; }
int	Explosion::getEnd() const { return (this->_end); }

void	Explosion::drawSmoke()
{
  double	min;
  double	max;

  min = 0.4 + _max;
  max = 0.6 + _max;
  if (max > 1)
    max -= 0.6;
  if (max > 0.7)
    max -= 0.4;
  if (min > 1)
    min -= 0.6;
  if (min > 0.7)
    min -= 0.4;

  //Back face
  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);  
  _smoke->bind();
  glBegin(GL_QUADS);
  glTexCoord2f(min, min);
  glVertex3f(0.0, 200.0f, 350.0f);
  glTexCoord2f(min, max);
  glVertex3f(0.0, 0.0, 350.0f);
  glTexCoord2f(max, max);
  glVertex3f(350.0f, 0.0, 350.0f);
  glTexCoord2f(max, min);
  glVertex3f(350.0f, 200.0f, 350.0f);

  glTexCoord2f(min, min);
  glVertex3f(0.0, 200.f, 350.f);
  glTexCoord2f(min, max);
  glVertex3f(0.0, 200.f, 0.0);
  glTexCoord2f(max, max);
  glVertex3f(350.0f, 200.0f, 0.0);
  glTexCoord2f(max, min);
  glVertex3f(350.0f, 200.0f, 350.f);

  glTexCoord2f(min, min);
  glVertex3f(0.0, 0.0, 350.f);
  glTexCoord2f(max, min);
  glVertex3f(0.0, 0.0, 0.0);
  glTexCoord2f(max, max);
  glVertex3f(0.0, 200.0f, 0.0);
  glTexCoord2f(min, max);
  glVertex3f(0.0, 200.0f, 350.f);

  glTexCoord2f(min, min);
  glVertex3f(350.0f, 0.0, 350.f);
  glTexCoord2f(min, max);
  glVertex3f(350.0f, 0.0, 0.0);
  glTexCoord2f(max, max);
  glVertex3f(350.0f, 200.0f, 0.0);
  glTexCoord2f(max, min);
  glVertex3f(350.0f, 200.0f, 350.f);
  glEnd();
}

void Explosion::draw(void)
{
  if (_explode && _end == 0)
    {
      glPushMatrix();
      glLoadIdentity();
      glTranslatef(position_.x, position_.y, position_.z);
      drawSmoke();
      glPopMatrix();
    }
}


AObject		*Explosion::getReplace() const
{ return (NULL); }

IEvent        *Explosion::createEvent(gdl::GameClock const & gameClock,	\
				gdl::Input & input)
{
  return (new Event(this, gameClock, input));
}
