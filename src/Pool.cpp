//
// Pool.cpp for  in /home/grizet_j//C++/plazza/src
// 
// Made by jean grizet
// Login   <grizet_j@epitech.net>
// 
// Started on  Tue Apr 16 11:44:19 2013 jean grizet
// Last update Fri Jun  7 17:12:08 2013 jean grizet
//

#include "IEvent.hh"
#include "Pool.hh"

template <typename T, typename U>
Pool<T, U>::Pool(IEventQueue<T>& t, IEventQueue<U>& c,	\
		 std::vector<IThread *>& th) :
  _task(t), _complete(c), _thread(th)
{}

template <typename T, typename U>
Pool<T, U>::~Pool()
{
  stop();
}

template <typename T, typename U>
void  Pool<T, U>::pushTask(T* elem)
{
  _task.push(elem);
}

template <typename T, typename U>
bool	Pool<T, U>::popCompleted(U** elem)
{
  return (_complete.tryPop(elem));
}

template <typename T, typename U>
void	Pool<T, U>::run()
{
  std::vector<IThread *>::iterator it;

  for (it = _thread.begin(); it != _thread.end(); ++it)
    (*it)->run();
}

template <typename T, typename U>
void	Pool<T, U>::stop()
{
  std::vector<IThread *>::iterator it;

  for (it = _thread.begin(); it != _thread.end(); ++it)
    {
      (*it)->cancel();
    }
}

template Pool<IEvent,IEvent>::Pool(IEventQueue<IEvent>&,IEventQueue<IEvent>&, \
				   std::vector<IThread *>&);
