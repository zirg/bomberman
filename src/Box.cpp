//
// Bomberman.cpp for  in /home/grizet_j//C++/bomberman/src
// 
// Made by jean grizet
// Login   <grizet_j@epitech.net>
// 
// Started on  Mon Apr 29 12:27:58 2013 jean grizet
// Last update Sat Jun  8 17:09:43 2013 jean grizet
//

#include "Box.hh"

Box::Box(void) :
  _texture(new gdl::Image(gdl::Image::load("data/assets/secu.jpg"))),
  _smoke(new gdl::Image(gdl::Image::load("data/assets/fire.png"))),
  _explode(false), _end(0),
  _replace(NULL)
{
  _max = 0;
}

Box::Box(gdl::Image* t, gdl::Image *s, AObject *b) :
  _texture(t),
  _smoke(s),
  _explode(false), 
  _end(0),
  _replace(b)
{  
  _max = 0;
}

Box::Box(gdl::Image* t) :
  _texture(t),
  _smoke(NULL),
  _explode(false),
  _end(0),
  _replace(NULL)
{  
  _max = 0;
}

Box::~Box(void)
{
  if (_replace != NULL)
    {
      delete (_replace);
      _replace = NULL;
    }
}

void Box::initialize(void)
{
  position_.x = 200.0;
  position_.y = 0.0;
  position_.z = 0.0;
}

void	Box::initBox(float posX, float posY, float posZ)
{
  position_.x = posX;
  position_.y = posY;
  position_.z = posZ;
}

void Box::update(gdl::GameClock const & gameClock, gdl::Input & input)
{
  (void)gameClock;
  (void)input;
  _explode = true;
  position_.y += 170;
  if (position_.y >= 6000)
    {
      _explode = false;
      _end = 1;
    }
}

void	Box::setEnd(int end) { this->_end = end; }
int	Box::getEnd() const { return (this->_end); }

void	Box::drawVertex()
{
 //Back face
  _texture->bind();
  glBegin(GL_QUADS);
  glTexCoord2f(0.0, 0.0);
  glVertex3f(0.0, 350.0f, 350.0f);
  glTexCoord2f(0.0, 1.0f);
  glVertex3f(0.0, 0.0, 350.0f);
  glTexCoord2f(1.0f, 1.0f);
  glVertex3f(350.0f, 0.0, 350.0f);
  glTexCoord2f(1.0f, 0.0);
  glVertex3f(350.0f, 350.0f, 350.0f);

  //  Top face
  glTexCoord2f(0.0, 0.0);
  glVertex3f(0.0, 350.f, 350.f);
  glTexCoord2f(0.0, 1.0f);
  glVertex3f(0.0, 350.f, 0.0);
  glTexCoord2f(1.0f, 1.0f);
  glVertex3f(350.0f, 350.f, 0.0);
  glTexCoord2f(1.0f, 0.0);
  glVertex3f(350.0f, 350.f, 350.f);

  //Left face
  glTexCoord2f(0.0, 0.0);
  glVertex3f(0.0, 0.0, 350.f);
  glTexCoord2f(0.0, 1.0f);
  glVertex3f(0.0, 0.0, 0.0);
  glTexCoord2f(1.0f, 1.0f);
  glVertex3f(0.0, 350.0f, 0.0);
  glTexCoord2f(1.0f, 0.0);
  glVertex3f(0.0, 350.0f, 350.f);


  //Right face
  glTexCoord2f(0.0, 0.0);
  glVertex3f(350.0f, 0.0, 350.f);
  glTexCoord2f(0.0, 1.0f);
  glVertex3f(350.0f, 0.0, 0.0);
  glTexCoord2f(1.0f, 1.0f);
  glVertex3f(350.0f, 350.0f, 0.0);
  glTexCoord2f(1.0f, 0.0);
  glVertex3f(350.0f, 350.0f, 350.f);
  glEnd();
}

void		Box::drawSmoke()
{
  double	bottom;
  double	min;
  double	max;

  bottom = position_.y - 1000.0;
  if (bottom < 0)
    bottom = 0;
  min = 0.3 + 1080.0 / position_.y;
  max = 0.7 + 1080.0 / position_.y;
  if (max > 1)
    max -= 0.6;
  if (max > 0.7)
    max -= 0.4;
  if (min > 1)
    min -= 0.6;
  if (min > 0.7)
    min -= 0.4;
  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);
  _smoke->bind();
  glBegin(GL_QUADS);
  glTexCoord2f(0.0, 0.0);
  glVertex3f(0.0, position_.y, 175.0);
  glTexCoord2f(0.0, 1080.0f / position_.y);
  glVertex3f(0.0, bottom, 175.0);
  glTexCoord2f(1.0f, 1080.0f / position_.y);
  glVertex3f(350.0f, bottom, 175.0);
  glTexCoord2f(1.0f, 0.0);
  glVertex3f(350.0f, position_.y, 175.0);

  //Fire
  glTexCoord2f(min, min);
  glVertex3f(0.0, 200.0f, 350.0f);
  glTexCoord2f(min, max);
  glVertex3f(0.0, 0.0, 350.0f);
  glTexCoord2f(max, max);
  glVertex3f(350.0f, 0.0, 350.0f);
  glTexCoord2f(max, min);
  glVertex3f(350.0f, 200.0f, 350.0f);

  glTexCoord2f(min, min);
  glVertex3f(0.0, 200.0f, 350.f);
  glTexCoord2f(min, max);
  glVertex3f(0.0, 200.0f, 0.0);
  glTexCoord2f(max, max);
  glVertex3f(350.0f, 200.0f, 0.0);
  glTexCoord2f(max, min);
  glVertex3f(350.0f, 200.0f, 350.f);

  glTexCoord2f(min, min);
  glVertex3f(0.0, 0.0, 350.f);
  glTexCoord2f(max, min);
  glVertex3f(0.0, 0.0, 0.0);
  glTexCoord2f(max, max);
  glVertex3f(0.0, 200.0f, 0.0);
  glTexCoord2f(min, max);
  glVertex3f(0.0, 200.0f, 350.f);

  glTexCoord2f(min, min);
  glVertex3f(350.0f, 0.0, 350.f);
  glTexCoord2f(min, max);
  glVertex3f(350.0f, 0.0, 0.0);
  glTexCoord2f(max, max);
  glVertex3f(350.0f, 200.0f, 0.0);
  glTexCoord2f(max, min);
  glVertex3f(350.0f, 200.0f, 350.f);
  glEnd();
}

void Box::draw(void)
{
  glPushMatrix();
  glLoadIdentity();

  glTranslatef(position_.x, position_.y, position_.z);
  glRotatef(this->rotation_.x, 0.1f, 0.0f, 0.0f);
  glRotatef(this->rotation_.y, 0.0f, 1.0f, 0.0f);
  glRotatef(this->rotation_.z, 0.0f, 0.0f, 1.0f);
  drawVertex();
  if (_explode == 1)
    {
      glTranslatef(0.0, -position_.y, 0.0);
      drawSmoke();
    }
  glPopMatrix();
}


AObject		*Box::getReplace() const
{
  return (_replace);
}

IEvent        *Box::createEvent(gdl::GameClock const & gameClock,	\
				gdl::Input & input)
{
  return (new Event(this, gameClock, input));
}

float		Box::getPosX() const
{
  return (this->position_.x);
}
