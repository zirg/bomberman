//
// Viseur.cpp for bomberman in /home/grilla_b//Documents/epi/projet_tek2/boomberman/real5
// 
// Made by brian grillard
// Login   <grilla_b@epitech.net>
// 
// Started on  Fri May 17 11:19:31 2013 brian grillard
// Last update Sun Jun  9 17:34:27 2013 jean-charles lecoq
//

#include <fstream>
#include <iostream>
#include "Viseur.hh"

Viseur::Viseur(int x, int y, MapEditor *map) :_posX(x), _posY(y), _map(map)
{ 
  _timeInput = 0;
  _end  = 0;
  _texture = new gdl::Image(gdl::Image::load("data/assets/tesseract.jpg"));
  _textblock = new gdl::Image(gdl::Image::load("data/assets/secu.jpg"));
  _textbox = new gdl::Image(gdl::Image::load("data/assets/nosecu.jpg"));
}

Viseur::~Viseur() {}

void	Viseur::initialize()
{
  position_.x = 200;
  position_.y = 0;
  position_.z = 0;
}

bool  Viseur::checkTimeInput(gdl::GameClock const & gameClock)
{
  float                       time;

  time = gameClock.getTotalGameTime();
  if ((time - this->_timeInput) >= 0.15)
    {
      this->_timeInput = time;
      return (true);
    }
  return (false);
}

void	Viseur::changePosition(gdl::GameClock const & gameClock, gdl::Input & input, int y, int x)
{
  (void)input; 
  if (this->checkTimeInput(gameClock) == true)
    {
      this->_map->getMap()[_posY + y][_posX + x]->setViseur(this);
      this->_map->getMap()[_posY][_posX]->setViseur(NULL);
      _posY += y;
      _posX += x;
    }
}

void	Viseur::setObject(AObject *object, int type)
{
  this->_map->getMap()[_posY][_posX]->setObject(object);
  this->_map->getMap()[_posY][_posX]->setType(type);
}

void	Viseur::deleteObject(gdl::GameClock const & gameClock)
{
  if (checkTimeInput(gameClock) == true)
    {
      if (this->_map->getMap()[_posY][_posX]->getObject() != NULL)
	{
	  this->_map->getMap()[_posY][_posX]->setType(0);
	  delete this->_map->getMap()[_posY][_posX]->getObject();
	}
      this->_map->getMap()[_posY][_posX]->setObject(NULL);
    }
}

void	Viseur::update(gdl::GameClock const & gameClock, gdl::Input & input)
{
  if ((input.isKeyDown(gdl::Keys::Down) == true) && (_posY < this->_map->getsizeY()))
    this->changePosition(gameClock, input, 1, 0);
  if ((input.isKeyDown(gdl::Keys::Up)) == true && (_posY > 0))
    this->changePosition(gameClock, input, -1, 0);
  if ((input.isKeyDown(gdl::Keys::Right) == true) && (_posX < this->_map->getsizeX()))
    this->changePosition(gameClock, input, 0, 1);
  if ((input.isKeyDown(gdl::Keys::Left) == true) && (_posX > 0))
    this->changePosition(gameClock, input, 0, -1);
  inputSet(gameClock, input); 
}

void	Viseur::draw()
{
  glPushMatrix();
  glLoadIdentity();

  _texture->bind();
  glBegin(GL_QUADS);
  glTexCoord2f(0.0, 0.0);
  glVertex3f(position_.x, position_.y, position_.z + 350.f);
  glTexCoord2f(0.0, 1.0f);
  glVertex3f(position_.x, position_.y, position_.z);
  glTexCoord2f(1.0f, 1.0f);
  glVertex3f(position_.x + 350.0f, position_.y, position_.z);
  glTexCoord2f(1.0f, 0.0);
  glVertex3f(position_.x + 350.0f, position_.y, position_.z + 350.f);

  glEnd();
  glPopMatrix();
}

AObject         *Viseur::getReplace() const
{
  return (_replace);
}

IEvent        *Viseur::createEvent(gdl::GameClock const & gameClock,       \
                                gdl::Input & input)
{
  return (new Event(this, gameClock, input));
}

void	Viseur::inputSet(gdl::GameClock const & gameClock, gdl::Input & input)
{
  if ((input.isKeyDown(gdl::Keys::A) == true) && (checkTimeInput(gameClock) == true))
    this->setObject(new Box(_textbox), 1);
  if ((input.isKeyDown(gdl::Keys::B) == true) && (checkTimeInput(gameClock) == true))
    this->setObject(new Box, 2);
  if (input.isKeyDown(gdl::Keys::Z) == true)
    this->deleteObject(gameClock);
}

void    Viseur::setEnd(int end) { this->_end = end; }
int     Viseur::getEnd() const { return (this->_end); }

int	Viseur::getPosY() const { return (this->_posY); }
int	Viseur::getPosX() const { return (this->_posX); }
