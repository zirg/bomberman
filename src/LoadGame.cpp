//
// LoadGame.cpp for bomberman in /home/grilla_b//Documents/epi/projet_tek2/boomberman/repo/bomberman
// 
// Made by brian grillard
// Login   <grilla_b@epitech.net>
// 
// Started on  Thu Jun  6 05:22:53 2013 brian grillard
// Last update Sun Jun  9 14:34:41 2013 jean grizet
//

#include "LoadGame.hh"

LoadGame::LoadGame()
{
}

LoadGame::~LoadGame()
{
}


bool			LoadGame::setInfo(Option& opt, Map *map, Global_IA* gl, GameSound& gameSound,
					  std::vector<Model::Bomberman*>* players, gdl::GameClock const & gameClock,
					  int size, std::list<AObject *>* obj)
{
  int			nbj = 0;
  int			nbi = 0;

  for (int n = 0; n != size; n++)
    {
      t_sav		sa = tabsave[n];
      Model::Bomberman*	player;

      if ((sa.posX > map->getsizeX()) || (sa.posY > map->getsizeY()) || (sa.posX < 0) || (sa.posY < 0))
	return (false);
      if (sa.player == 8)
	{
	  player = new Model::Bomberman(sa.player, sa.posX, sa.posY, opt.getKeyJ1()[0],
					 opt.getKeyJ1()[1], opt.getKeyJ1()[2],
					opt.getKeyJ1()[3], opt.getKeyJ1()[4], map, gameSound, 1);
	  nbj++;
	}
      else if (sa.player == 9)
	{
	  player = new Model::Bomberman(sa.player, sa.posX, sa.posY, opt.getKeyJ2()[0],
					 opt.getKeyJ2()[1], opt.getKeyJ2()[2],
					opt.getKeyJ2()[3], opt.getKeyJ2()[4], map, gameSound, 1);
	  nbj++;
	}
      else
	{
	  player = new IA(sa.player, sa.posX, sa.posY, map, gl, players, gameSound, &gameClock);
	  nbi++;
	}
      players->push_back(player);
      obj->push_front(player);
      obj->push_front(player->getBagOfBomb());
    }  
  opt.setPlayer(nbj);
  opt.setIA(nbi);
  return (true);
}

void			LoadGame::setState(std::vector<Model::Bomberman*>* players)
{
  int			n = 0;
  std::vector<Model::Bomberman*>::iterator it = players->begin();

  for (; it != players->end(); ++it)
    {
      (*it)->setAnimSpeed(tabsave[n].animSpeed);
      (*it)->setSpeed(tabsave[n].speed);
      (*it)->setPourcent(tabsave[n].pourcent);
      (*it)->setMaxBomb(tabsave[n].maxBomb);
      (*it)->setPower(tabsave[n].power);
      (*it)->setPoints(tabsave[n].points);
      n++;
    }
}

bool			LoadGame::load(Map *map, Option& opt, Global_IA** gl, std::string const & path,
				       std::vector<Model::Bomberman*>* players, gdl::GameClock const & gameClock,
				       GameSound& gameSound, std::list<AObject *>* obj)
{
  int			size = 0;
  int			ret = 0;
  int			fd;
  int			i = 0;
  int			n = 0;

  if ((map->initializeMap(path)) == false)
    return (false);
  (*gl) = new Global_IA(map);
  if ((fd = open(path.c_str(), O_RDWR)) == -1)
    return (false);
  size = lseek(fd, size, SEEK_END);
  lseek(fd, 0, SEEK_SET);
  while (i < size)
    {
      if ((ret = read(fd, &tabsave[n], sizeof(t_sav))) == -1)
	return (-1);
      i += ret;
      n++;
      lseek(fd, i, SEEK_SET);
    }
  close(fd);
  if ((setInfo(opt, map, *gl, gameSound, players, gameClock, n, obj)) == false)
    return (false);
  return (true);
}
