//
// Case.cpp for bomberman in /home/grilla_b//Documents/epi/projet_tek2/boomberman/real/src
// 
// Made by brian grillard
// Login   <grilla_b@epitech.net>
// 
// Started on  Thu May  9 17:18:40 2013 brian grillard
// Last update Sat Jun  8 15:18:01 2013 jean grizet
//

#include "Case.hh"
#include <iostream>

Case::Case(int x, int y, int d, AObject *obj)
  : _x(x), _y(y), _walk(d)
{

  _death = 0;
  _pos.x = (x * 350.0f);
  _pos.y = 1.0f;
  _pos.z = (y * 350.0f);
  _lim[0] = _pos.z;
  _lim[1] = _pos.z + 350.0f;
  _lim[2] = _pos.x;
  _lim[3] = _pos.x + 350.0f;
  _object = obj;
  if (_object != NULL)
    {
      _object->position_.x = _pos.x;
      _object->position_.y = _pos.y;
      _object->position_.z = _pos.z;
    }
  _bomberman = NULL;
  _idBonus = 0;
}

Case::~Case()
{}

int			Case::getX() const { return (this->_x); }
int			Case::getY() const { return (this->_y); }
AObject*		Case::getObject() const { return (this->_object); }

AObject*		Case::getBomberman() const { return (_bomberman); }

void			Case::setBomberman(AObject* bomberman) 
{
  _mu.lock();
  this->_bomberman = bomberman;
  _mu.unlock();
}
void			Case::setX(int x) 
{ 
  _mu.lock();
  this->_x = x; 
  _mu.unlock();
}
void			Case::setY(int y) 
{ 
  _mu.lock();
  this->_y = y; 
  _mu.unlock();
}
void			Case::setObject(AObject* object) 
{ 
  _mu.lock();
  this->_object = object;
  if (object != NULL)
    {
      object->position_.x = _pos.x;
      object->position_.y = _pos.y;
      object->position_.z = _pos.z;
    }
  _mu.unlock();
}

int			Case::canWalk() const { return (this->_walk); }

void			Case::setWalk(int b) 
{ 
  _mu.lock();
  _walk = b; 
  _mu.unlock();
}

float			Case::getNorthLim() const { return (_lim[0]); }
float			Case::getSouthLim() const { return (_lim[1]); }
float			Case::getEstLim() const { return (_lim[3]); }
float			Case::getWestLim() const { return (_lim[2]); }
bool			Case::getDeath() const { return (this->_death); }
int			Case::getIdBonus() const { return this->_idBonus; }
void			Case::setIdBonus(int bonus) { this->_idBonus = bonus; };

void			Case::setDeath(bool death)
{ 
  _mu.lock();
  this->_death = death;
  _mu.unlock();
}
