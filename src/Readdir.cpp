//
// Readdir.cpp for Readdir in /home/lecoq_j//BombermanDepot/bomberman/src
// 
// Made by jean-charles lecoq
// Login   <lecoq_j@epitech.net>
// 
// Started on  Fri Jun  7 18:34:51 2013 jean-charles lecoq
// Last update Sun Jun  9 12:25:34 2013 brian grillard
//

#include <iostream>
#include <unistd.h>
#include <stdlib.h>

#include	"Readdir.hh"

Readdir::Readdir(std::string path, std::vector<std::string>& vec)
  :_path(path), _data(vec)
{
}

Readdir::~Readdir()
{}


bool    Readdir::checkDouble(std::string& name)
{
  std::vector<std::string>::iterator    it;

  for (it = _data.begin(); it != _data.end(); ++it)
    if ((*it) == name)
      return false;
  return true;
}

void	Readdir::recupData()
{
  DIR   *rep;
  struct dirent *file;
  std::string   name;
  int		pos;

  rep = opendir(_path.c_str());
  if (rep == NULL)
    {
      std::cerr << "YOU SUCK : missing " << _path << std::endl;
      exit(EXIT_FAILURE);
    }
  while ((file = readdir(rep)) != NULL)
    {
      if (file->d_name[0] != '.')
        if (file->d_name[1] != '.')
          {
            name = file->d_name;
            if (name.size() >= 5)
              {
		if ((pos = name.find(".")) < name.size())
		    name.erase(pos, name.size());
		if (checkDouble(name) == true)
		  _data.push_back(name);
              }
          }
    }
  closedir(rep);
}
