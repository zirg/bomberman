//
// Game.cpp for  in /home/grizet_j//C++/bomberman/src
// 
// Made by jean grizet
// Login   <grizet_j@epitech.net>
// 
// Started on  Wed Jun  5 21:36:47 2013 jean grizet
// Last update Sat Jun  8 19:55:37 2013 jean grizet
//

#include "Game.hh"

Game::Game() :
  _menu(gameClock_, input_, _sound, _opt),
  _game(_opt, _sound, gameClock_, input_),
  _creator(_opt, "map.map", gameClock_, input_),
  _intro(_opt, _sound, gameClock_, input_, _menu.getTexture())
{
  _opt.setScreenX(1200);
  _opt.setScreenY(1000);
  _ingame = false;
}

Game::~Game() 
{}

void		Game::initialize()
{
  window_.setWidth(_opt.getScreenX());
  window_.setHeight(_opt.getScreenY());
  window_.create();
  _sound.init();
  _sound.setVolumeMusic(5);
  _sound.setVolumeEffect(30);
  _intro.initialize();
  _menu.initialize();  
}

void		Game::update()
{
  if (_intro.isEnded() == false)
    {
      _intro.update();
      return;
    }
  if (_menu.isQuit())
    exit(EXIT_SUCCESS);
  _ingame = (_menu.isPlaying() || _menu.isLoadGame() ? true : false);
  if (_ingame == true)
    manageGame();
  else if (_menu.isCreateMap() == true)
    manageCreator();
  else
    _menu.update();
}

void		Game::manageCreator()
{
  if (_creator.isLoaded() == false)
    {
      std::stringstream ss;
      ss << _menu.getNbrMap() + 1;
      _creator.initialize();
      _creator.setName(ss.str());
      ss.str("");
    }
  _creator.update();
  if (_creator.isEnded())
    {
      _creator.unload();
      _menu.stopPlaying();
      _ingame = false;
    }
}

void		Game::manageGame()
{
  if (_game.isLoaded() == false)
    {
      if (_menu.isLoadGame())
	_game.initialize(_menu.getSaveGame());
      else
	_game.initialize();
    }
  if (_game.isLoaded())
    _game.update();
  if (_game.isEnded())
    {
      _ingame = false;
      _menu.stopPlaying();
      _game.unload();
    }
}

void		Game::draw()
{
  if (_intro.isEnded() == false)
    {
      _intro.draw();
      return;
    }
  if (_ingame == true)
    _game.draw();
  else if (_menu.isCreateMap() == true && _creator.isLoaded())
    {
      _creator.draw();
      window_.display();
    }
  else
    {
      _menu.draw();
      window_.display();
    }
}

void		Game::unload()
{}
