//
// Map.cpp for map in /home/grilla_b//Documents/epi/projet_tek2/boomberman/real
// 
// Made by brian grillard
// Login   <grilla_b@epitech.net>
// 
// Started on  Thu May  9 15:41:46 2013 brian grillard
// Last update Sun Jun  9 15:52:22 2013 jean grizet
// Last update Sun May 26 15:44:00 2013 jean grizet
//

#include <sstream>
#include "Map.hh"

Map::Map()
{
  _textblock = new gdl::Image(gdl::Image::load("data/assets/nosecu.jpg"));
  _textbox = new gdl::Image(gdl::Image::load("data/assets/secu.jpg"));
  _ground = new gdl::Image(gdl::Image::load("data/assets/ground.jpg"));
  _smoke = new gdl::Image(gdl::Image::load("data/assets/fire.png"));
  _ruby = new gdl::Image(gdl::Image::load("data/assets/ruby.jpg"));
  _saph = new gdl::Image(gdl::Image::load("data/assets/saphire.jpg"));
  _emerald = new gdl::Image(gdl::Image::load("data/assets/emerald.jpg"));
  _wall = new Box(_textblock, _smoke, NULL);
}

Map::~Map()
{
  for (std::list<Box *>::iterator it = _boxes.begin();	\
       it != _boxes.end(); ++it)
    {
      Box	*tmp;

      tmp = (*it);
      if (tmp != NULL)
	delete (tmp);
    }
  for (std::list<Case *>::iterator it = _cases.begin();	\
       it != _cases.end(); ++it)
    {
      Case	*tmp;

      tmp = (*it);
      if (tmp != NULL)
	delete (tmp);
    }
  delete (_textblock);
  delete (_textbox);
  delete (_ground);
  delete (_ruby);  
  delete (_saph);
  delete (_emerald);
  delete (_wall);
}


void			Map::initializeMap(int x, int y)
{
  Box		*b = NULL;
  Case		*c = NULL;
  this->_sizeX = x;
  this->_sizeY = y;
  this->_rsizeX = x * 350.0f;
  this->_rsizeZ = y * 350.0f;

  srand(time(NULL));
  for (int ty = 0; ty <= y; ty++)
    {
      this->_map.push_back(*(new std::vector<Case*>));
      for (int tx = 0; tx <= x; tx++)
	{
	  if (tx % 4 == 1 && ty % 4 == 1)
	    {
	      b = new Box(_textblock, _smoke, NULL);
	      _boxes.push_front(b);
	      c = new Case(tx, ty, 1, b);
	      _cases.push_front(c);
	      this->_map[ty].push_back(c);
	    }
	  else
	    {
	      b = new Box(_textbox, _smoke, generateBonus());
	      _boxes.push_front(b);
	      c = new Case(tx, ty, 2, b);
	      _cases.push_front(c);
	      this->_map[ty].push_back(c);
	    }
	}
    }
}

bool			Map::is_empty(std::ifstream& pFile)
{
  return pFile.peek() == std::ifstream::traits_type::eof();
}

bool			Map::getSizeMapLoad(std::ifstream& file, std::string & value)
{
  if (file.good())
    {
      std::getline(file, value, '\n');
      std::stringstream ss;
      if (!(ss << value))
	return (false);
      if (!(ss >> _sizeX))
	return (false);
      std::getline(file, value, '\n');
      std::stringstream ssy;
      if (!(ssy << value))
	return (false);
      if (!(ssy >> _sizeY))
	return (false);
    }
  else
    return false;
  return (true);
}

bool			Map::checkSizeMapLoad() const
{
  if (((this->_sizeX < 0) || (this->_sizeX > 999)) ||
      ((this->_sizeY < 0) || (this->_sizeY > 999)))
    return (false);
  return (true);
}


bool			Map::initializeMap(std::string const & path)
{
  std::string           filename(path);
  filename.append(".map");
  std::ifstream		file (filename.c_str());
  std::vector<int>	val;
  std::string		value;
  std::string		value2;
  int			nbr = 0;;

  if (file.good())
    {
      if (is_empty(file) == true)
	return (false);
    }
  if (getSizeMapLoad(file, value) == false)
    return (false);
  if (checkSizeMapLoad() == false)
    return (false);
  this->_rsizeX = this->_sizeX * 350.0f;
  this->_rsizeZ = this->_sizeY * 350.0f;  
  while (file.good())
    {
      getline(file, value, '\n');
      std::stringstream iss;
      iss << value;
      while (getline(iss, value2, '.'))
	{
	  nbr = 0;
	  std::stringstream isso;
	  isso << value2;
	  if (!(isso >> nbr))
	    return (false);
	  val.push_back(nbr);
	  isso.str("");
	}
      iss.str("");
    }
  file.close();
  if (val.empty() || (_sizeX * _sizeY) != (val.size()))
    return (false);
  srand(time(NULL));
  for (int ty = 0; (ty <= _sizeY); ty++)
    {
      this->_map.push_back(*(new std::vector<Case*>));
      for (int tx = 0; (tx <= _sizeX); tx++)
	{
	  int v = val[ty * (_sizeX - 1) + tx];
	  if (v == 1 || v == 2)
	    addBlock(v, tx, ty);
	  else if (v > 1 && v < 6)
	    addBlockAndBonus(v, tx, ty);
	  else
	    this->_map[ty].push_back(new Case(tx, ty, 0, NULL));
	}
    }
  return (true);
}

void			Map::addBlock(int ref, int & tx, int & ty)
{
  Box *b = NULL;
  if (ref == 1)
    {
      b = new Box(_textblock, _smoke, NULL);
      this->_map[ty].push_back(new Case(tx, ty, 1, b));
      return;
    }
  b = new Box(_textbox, _smoke, generateBonus());
  this->_map[ty].push_back(new Case(tx, ty, 2, b));
}

void			Map::addBlockAndBonus(int ref, int & tx, int & ty)
{
  if (ref == 3)
    {
      this->_map[ty].push_back(new Case(tx, ty, 0, new Bonus(_emerald, 3)));
      this->_map[ty][tx]->setIdBonus(3);
    }
  else if (ref == 4)
    {
      this->_map[ty].push_back(new Case(tx, ty, 0, new Bonus(_saph, 4)));
      this->_map[ty][tx]->setIdBonus(4);
    }
  else
    {
      this->_map[ty].push_back(new Case(tx, ty, 0, new Bonus(_ruby, 5)));
      this->_map[ty][tx]->setIdBonus(5);
    }
}


Bonus			*Map::generateBonus()
{
  int	t = rand() % 100;
  Bonus	*bonus = NULL;
  if (t >= 0 && t < 5)
    bonus = new Bonus(_emerald, 3);
  else if (t >= 5 && t < 10)
    bonus = new Bonus(_ruby, 5);
  else if (t >= 10 && t < 15)
    bonus = new Bonus(_saph, 4);
  return (bonus);
}

void	Map::update(gdl::GameClock const & gameClock, gdl::Input & input)
{
  for (int tx = 0; tx <= _sizeX; tx++)
    {
      for (int ty = 0; ty <= _sizeY; ty++)
        {
          if (_map[ty][tx]->getObject() != NULL)
            _map[ty][tx]->getObject()->update(gameClock, input);
	}
    }
}

void			Map::draw(int x, int y, int size)
{
  for (int tx = (x - size); tx <= (x + size); tx++)
    {
      for (int ty = (y - size); ty <= (y + size); ty++)
        {
	  if (tx >= 0 && ty >= 0 && tx <= _sizeX && ty <= _sizeY)
	    {
	      drawGround(_map[ty][tx]);
	      AObject *obj = _map[ty][tx]->getObject();
	      if (obj != NULL)
		obj->draw();
	      obj = _map[ty][tx]->getBomberman();
	      if (obj != NULL)
		obj->draw();
	    }
	  else 
	    drawWall(tx, ty);	    
	}
    }
}

void			Map::drawWall(int tx, int ty)
{
  if (((tx == -1 || tx == _sizeX + 1) && ty >= -1 && ty <= _sizeY +1) || \
      ((ty == -1 || ty == _sizeY + 1) && tx >= -1 && tx <= (_sizeX + 1)))
    {
      _wall->position_.x = tx * 350.0f;
      _wall->position_.z = ty * 350.0f;
      _wall->position_.y = 0.0;
      _wall->draw();
    }
}

void			Map::drawGround(Case *c)
{
  if (c->canWalk() == 1 || c->canWalk() == 2)
    return;
  _ground->bind();
  glBegin(GL_QUADS);
  glTexCoord2f(0.0, 0.0);
  glVertex3f(c->getWestLim(), 0.0, c->getNorthLim() - 1.0);
  glTexCoord2f(0.0, 1.0);
  glVertex3f(c->getWestLim(), 0.0, c->getSouthLim());
  glTexCoord2f(1.0, 1.0);
  glVertex3f(c->getEstLim(), 0.0, c->getSouthLim());
  glTexCoord2f(1.0, 0.0);
  glVertex3f(c->getEstLim(), 0.0, c->getNorthLim() - 1.0);
  glEnd();
}

int			Map::getsizeX() const { return (this->_sizeX); }
int			Map::getsizeY() const { return (this->_sizeY); }
float			Map::getrsizeX() const { return (this->_rsizeX); }
float			Map::getrsizeZ() const { return (this->_rsizeZ); }
std::vector< std::vector<Case*> >	Map::getMap() const 
{ return (this->_map); }
