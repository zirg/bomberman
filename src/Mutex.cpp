//
// Mutex.cpp for  in /home/grizet_j//C++/plazza/src
// 
// Made by jean grizet
// Login   <grizet_j@epitech.net>
// 
// Started on  Mon Apr 15 12:54:20 2013 jean grizet
// Last update Mon Apr 15 17:46:21 2013 jean grizet
//

#include "Mutex.hh"

Mutex::Mutex()
{
  if (pthread_mutex_init(&_mutex, NULL))
    {
      std::cerr << MUTEX_INIT_ERR << std::endl;
      exit(EXIT_FAILURE);
    }
}

Mutex::~Mutex()
{
  if (pthread_mutex_destroy(&_mutex))
    {
      std::cerr << MUTEX_DESTROY_ERR << std::endl;
      exit(EXIT_FAILURE);
    }
}

void          Mutex::lock(void)
{
  pthread_mutex_lock(&(this->_mutex));
}

void          Mutex::unlock(void)
{
  pthread_mutex_unlock(&(this->_mutex));
}

bool          Mutex::trylock(void)
{
  if (pthread_mutex_trylock(&(this->_mutex)) == 0)
    return (false);
  else
    return (true);
}
