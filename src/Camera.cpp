//
// Camera.cpp for  in /home/grizet_j//C++/bomberman/src
// 
// Made by jean grizet
// Login   <grizet_j@epitech.net>
// 
// Started on  Mon Apr 29 12:24:29 2013 jean grizet
// Last update Thu Jun  6 01:39:56 2013 jean grizet
//

#include <iostream>

#include "Camera.hh"

Camera::Camera(void) : 
  _x(0), _y(0), _xd(0.0), _yd(0.0), _player(0),
  position_(0.0f, 0.0f, 1000.0f), 
  rotation_(0.0f, 0.0f, 0.0f),
  look_(0.0f, 0.0f, 0.0f)
{ }

void Camera::initialize(Option& opt)
{
  _x = opt.getScreenX();
  _y = opt.getScreenY();
  _xd = opt.getScreenX();
  _yd = opt.getScreenY();
  _player = opt.getPlayer();
}

void Camera::update(gdl::GameClock const & gameClock, gdl::Input & input, \
		    int player)
{
  (void)input;
  (void)gameClock;
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  if (player == 0)
    {
      glViewport(0, 0, _x, _y);
      gluPerspective(70.0f, _xd/_yd, 1.0f, 8000.0f);
    }
  else if (player == 1)
    {
      glViewport(0, 0, _x / 2, _y);
      gluPerspective(70.0f, (_xd / 2.0) / _yd, 1.0f, 10000.0f);
    }
  else if (player == 2)
    {
      glViewport(_x / 2, 0, _x / 2, _y);
      gluPerspective(70.0f, (_xd / 2.0) / _yd, 1.0f, 10000.0f);
    }
  else
    {
      glViewport(0, 0, _x, _y);
      gluPerspective(60.0f, 1980.0f/1200.0f, 1.0f, 8000.0f);
    }
  gluLookAt(position_.x, position_.y, position_.z,
	    look_.x, look_.y, look_.z, 
	    0.0f, 1.0f, 0.0f);
}

void	Camera::setMode()
{
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();
  glEnable(GL_DEPTH_TEST);
  glDepthFunc(GL_LEQUAL);
}

void	Camera::setPosition(int x, int y, int z)
{
  position_.x = x;
  position_.y = y;
  position_.z = z;
}

void	Camera::setLook(int x, int y, int z)
{
  look_.x = x;
  look_.y = y;
  look_.z = z;
}
