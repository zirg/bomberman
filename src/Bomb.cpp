//
// Bomb.cpp for bomberman in /home/grilla_b//Documents/epi/projet_tek2/boomberman/real4/src
// 
// Made by brian grillard
// Login   <grilla_b@epitech.net>
// 
// Started on  Tue May 21 10:41:11 2013 brian grillard
// Last update Sun Jun  9 17:31:56 2013 jean grizet
//

#include <iostream>
#include "Bomb.hh"
#include "BagOfBomb.hh"
#include "Explosion.hh"
#include "APlayer.hpp"

Bomb::Bomb(GameSound& gs) :
  _sound(gs)
{
  this->_texture = (new gdl::Image(gdl::Image::load("data/assets/fire.png")));
  this->_model = new gdl::Model(gdl::Model::load("data/assets/bomb.fbx"));
  _power = 1;
  this->_explode = 0;
  _end = 0;
  this->addExplosion(5);
}

Bomb::~Bomb() 
{
  for (std::vector<Explosion *>::iterator it = _exp.begin();	\
       it != _exp.end(); ++it)
    {
      Explosion *tmp = NULL;
      tmp = *it;
      if (tmp != NULL)
	delete (tmp);
    }
  delete (_texture);
  delete (_model);
}

void			Bomb::initBomb(Map *map, int y, int x, double time,
				       BagOfBomb *bagBomb, int pos)
{
  _map = map;
  _y = y;
  _x = x;
  _time = time;
  _bagBomb = bagBomb;
  _pos = pos;
  rotation_.y = 0.0;
}

void			Bomb::initialize() {}

void			Bomb::addExplosion(int nbr)
{
  int	i = 0;

  while (i != nbr)
    {
      this->_exp.push_back(new Explosion(_texture));
      i++;
    }
}

bool			Bomb::checkTimeInput(gdl::GameClock const & gameClock, double t)
{
  float                 time;

  time = gameClock.getTotalGameTime();
  if ((time - this->_time) >= t)
    return (true);
  return (false);
}

void			Bomb::addPoints(Case* cs, int type)
{
  AObject* bon = cs->getBomberman();
  APlayer *bomberman = dynamic_cast<APlayer *>(bon);
  
  if (type == 2)
    this->_bagBomb->addPoints(100);
  else if (cs->getIdBonus() != 0)
    this->_bagBomb->addPoints(150);
  else if (bomberman != NULL)
    {
      if (this->_bagBomb->getIdBomber() == bomberman->getPlayer())
	this->_bagBomb->addPoints(-1000);
      else
	this->_bagBomb->addPoints(1500);
    }
}

void			Bomb::setPosition(int y, int x, bool *lim, bool point)
{
  Case*			cs = this->_map->getMap()[y][x];
  int			type = cs->canWalk();

  cs->setIdBonus(0);
  if (type != 1)
    this->_unsafe.push_back(std::pair<int, int>(y, x));
  if (type == 2 || type == 1)
    (*lim) = 1;
  if (point == true)
    this->addPoints(cs, type);
}

void			Bomb::unsafeY(int y, int x, bool point)
{
  int			yu = y - 1;
  int			yd = y + 1;
  int			i = 0;
  bool			u = 0;
  bool			d = 0;

  while (i != this->_power)
    {
      if ((yu >= 0) && (u == 0))
	{
	  this->setPosition(yu, x, &u, point);
	  yu--;
	}
      if ((yd <= this->_map->getsizeY()) && (d == 0))
	{
	  this->setPosition(yd, x, &d, point);
	  yd++;
	}
      i++;
    }
}

void				Bomb::unsafeX(int y, int x, bool point)
{
  int				xl = x - 1;
  int				xr = x + 1;
  int				i = 0;
  bool				l = 0;
  bool				r = 0;

  while (i != this->_power)
    {
      if ((xl >= 0) && (l == 0))
	{
	  this->setPosition(y, xl, &l, point);
	  xl--;
	}
      if ((xr <= this->_map->getsizeX()) && (r == 0))
	{
	  this->setPosition(y, xr, &r, point);
	  xr++;
	}
      i++;
    }
}


void				Bomb::setUnsafe(int y, int x, bool point)
{
  if (!_unsafe.empty())
    _unsafe.clear();
  this->unsafeY(y, x, point);
  this->unsafeX(y, x, point);
}

std::vector< std::pair<int, int> >	Bomb::getUnsafe()
{
  _mu.lock();
  std::vector< std::pair<int, int> > ret = _unsafe;
  _mu.unlock();
  return (ret);
}

void				Bomb::setObject()
{
  std::vector< std::pair<int, int> >::iterator it = this->_unsafe.begin();
  std::vector<Explosion *>::iterator ex = this->_exp.begin();
  ++ex;
  for (; it != this->_unsafe.end(); ++it)
    {
      Case*	cs = this->_map->getMap()[it->first][it->second];      
      if (cs->canWalk() != 2 && cs->canWalk() != 6)
	{
	  cs->setWalk(0);
	  cs->setIdBonus(0);
	  this->setExplosion(ex, cs);
	  ++ex;
	}
    }
}

void				Bomb::setExplosion(std::vector<Explosion *>::iterator ex, Case *cs)
{
  if (*ex != NULL)
    (*ex)->initialize();
  cs->setObject((*ex));
  cs->setIdBonus(0);
  cs->setDeath(true);
}

void				Bomb::Explose()
{
  Case*					cs = this->_map->getMap()[_y][_x];
  std::vector<Explosion*>::iterator	ex = this->_exp.begin();

  this->_explode = 1;
  this->setExplosion(ex, cs);
  this->setUnsafe(this->_y, this->_x, true);
  this->setObject();
  _sound.playEffect(GameSound::EXPLOSION);
}

void				Bomb::update(gdl::GameClock const & gameClock, gdl::Input & input)
{
  this->setUnsafe(_y, _x, false);
  rotation_.y += 5;
  if (checkTimeInput(gameClock, 3.0) == true && (this->_explode == 0))
    this->Explose();
  if ((this->_explode == 1))
    this->updateExplosion(gameClock, input);
  else if (this->_explode == 2)
    this->endExplosion();
}

void				Bomb::updateObj(Case *cs, AObject *obj, gdl::GameClock const &
						gameClock, gdl::Input & input)
{
  double			temt = (static_cast<Bomb*>(obj))->getTime(); 
  
  if ((cs->canWalk() == 6) && (obj != NULL))
    {
      if (temt >= this->_time)
	(static_cast<Bomb*>(obj))->setTime(this->_time);
    }
  else
    {
      obj->update(gameClock, input);
      if ((obj->getEnd()) == 1)
	this->_explode = 2;
    }
}

void				Bomb::loopUpdate(gdl::GameClock const & gameClock, gdl::Input & input,
						 std::vector<std::pair<int, int> >::iterator it)
{
  Case*				cs = this->_map->getMap()[it->first][it->second];
  AObject*			obj = cs->getObject();

  if (obj != NULL)
    this->updateObj(cs, obj, gameClock, input);
}

void				Bomb::updateExplosion(gdl::GameClock const & gameClock, gdl::Input & input)
{
  std::vector<std::pair<int, int> >::iterator	it = this->_unsafe.begin();
  this->_map->getMap()[_y][_x]->getObject()->update(gameClock, input);
  for (; it != this->_unsafe.end(); ++it)
    {
      if (it->first >= 0 && it->second >= 0 && it->first <= _map->getsizeY() \
	  && it->second <= _map->getsizeX())
	this->loopUpdate(gameClock, input, it);
    }
}

bool				Bomb::setDefaultCaseBomb()
{
  this->_explode = 0;
  Case *temp = this->_map->getMap()[this->_y][this->_x];
  if ((temp == NULL) || (temp->getObject() == NULL))
    return (false);
  temp->setObject(NULL);
  temp->setWalk(0);
  temp->setDeath(0);
  return (true);
}

void				Bomb::setDefaultCaseExplosion(int y, int x)
{
  Case *cs = this->_map->getMap()[y][x];
  if (cs != NULL)
    {
      AObject *obj = cs->getObject();
      if (obj != NULL && (cs->canWalk() != 6))
	{
	  AObject	*bonus;
	  bonus = obj->getReplace();
	  cs->setObject(bonus);
	  if (bonus != NULL)
	    {
	      Bonus *b = static_cast<Bonus *>(bonus);
	      cs->setIdBonus(b->getType());
	    }
	  else
	    cs->setIdBonus(0);
	  cs->setWalk(0);
	}
      cs->setDeath(0);
    }
}

void				Bomb::endExplosion()
{
  rotation_.y = 0.0;
  if (this->setDefaultCaseBomb() == false)
    return;
  std::vector< std::pair<int, int> >::iterator it = this->_unsafe.begin();
  for (; it != this->_unsafe.end(); ++it)
    {
      if (it->first >= 0 && it->second >= 0 && it->first <= _map->getsizeY() \
	  && it->second <= _map->getsizeX())
	this->setDefaultCaseExplosion(it->first, it->second);
    }
  this->_unsafe.erase(this->_unsafe.begin(), this->_unsafe.end());
  this->_bagBomb->changeStatePosBomb(this->_pos);
  this->_bagBomb->decreaseNbon(1);
}

void				Bomb::draw()
{
  glPushMatrix();
  glLoadIdentity();
  glTranslatef(position_.x + 175.0f, position_.y + 175.0f,	\
	       position_.z + 175.0f);
  glRotatef(rotation_.y, 0.0, 1.0, 0.0);
  glRotatef(8.0, 0.0, 0.0, 1.0);
  this->_model->draw();
  glPopMatrix();
}

AObject			*Bomb::getReplace() const
{ return (NULL); }

IEvent			*Bomb::createEvent(gdl::GameClock const & gameClock, \
					   gdl::Input & input)
{
  return (new Event(this, gameClock, input));
}

int				Bomb::getEnd() const { return this->_end; }
void				Bomb::setEnd(int end) { this->_end = end; }
void				Bomb::setTime(double time) { this->_time = time; }
void				Bomb::addPower(int nbr) { this->_power += nbr; }
int				Bomb::getX() const { return (_x); }
int				Bomb::getY() const { return (_y); }
int				Bomb::getPower() const { return (this->_power); }
void                            Bomb::setPower(int power) { this->_power = power; }

double				Bomb::getTime()
{
  double			t;

  t = _time;
  return (t);
}

