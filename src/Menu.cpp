//
// Bomber.cpp for  in /home/grizet_j//C++/bomberman/src
// 
// Made by jean grizet
// Login   <grizet_j@epitech.net>
// 
// Started on  Mon Apr 29 12:15:19 2013 jean grizet
// Last update Sun Jun  9 17:27:27 2013 jean-charles lecoq
//

#include <iostream>
#include "Menu.hh"

Menu::Menu(gdl::GameClock& gc, gdl::Input& i, GameSound& gs, Option& opt) :
  gameClock_(gc), input_(i), _gameSound(gs), _option(opt)
{}

void  Menu::initialize(void)
{
   _option.setScreenX(1200);
   _option.setScreenY(1000);
   _option.setSizeX(15);
   _option.setSizeY(15);
   _option.setPlayer(1);
   _option.setIA(0);
   _option.setMode(0);
   camera_ = new Camera();
   camera_->initialize(_option);

   this->typeMenu_ = 0;
   this->inMove_ = false;

   _listMap.push_back("default");
   _mapSave = new Readdir("./data/map/", _listMap);
   _gameSave = new Readdir("./data/save/data", _listGame);

   _mapSave->recupData();
   _gameSave->recupData();
   _posListMap = _listMap.begin();
   _posListGame = _listGame.begin();

   _option.setTypeMap((*_posListMap));

   _game = false;
   _isQuit = false;
   _isCreateMap = false;
   _isLoadGame = false;

   newGame_ = new MenuNewGame(_option, _texture, _listMap, _posListMap);
   loadGame_ = new MenuLoadGame(_texture, _listGame, _posListGame);
   mapGenerator_ = new MenuCreateMap(_option, _texture);
   score_ = new MenuScores(_texture);
   option_ = new MenuSettings(_option, _gameSound, _texture);
   help_ = new MenuHelp(_texture);
   credit_ = new MenuCredit(_texture);
   quit_ = new MenuQuit(_texture);

   newGame_->initialize();
   option_->initialize();
   loadGame_->initialize();
   score_->initialize();
   help_->initialize();
   credit_->initialize();
   mapGenerator_->initialize();
   quit_->initialize();

   speedCamera_ = 50;
   typeMenu_ = 0;

   typeZoom_ = true;
   inMenu_ = false;
   inZoom_ = false;
   xCamera_ = 0;
   yCamera_ = 0;
   zCamera_ = 0;
   xCenter_ = 0;
   yCenter_ = 0;
   zCenter_ = -3000;
   xDestCamera_ = 0;
   yDestCamera_ = 0;
   zDestCamera_ = 0;
   xDestCenter_ = 0;
   yDestCenter_ = 0;
   zDestCenter_ = -3000;

   mapZoom_[0] = &Menu::zoomNewGame;
   mapZoom_[1] = &Menu::zoomCredit;
   mapZoom_[2] = &Menu::zoomMapGenerator;
   mapZoom_[3] = &Menu::zoomScore;
   mapZoom_[4] = &Menu::zoomOption;
   mapZoom_[5] = &Menu::zoomQuit;
   mapZoom_[6] = &Menu::zoomLoadGame;
   mapZoom_[7] = &Menu::zoomHelp;

   mapPos_[0] = &Menu::posNewGame;
   mapPos_[1] = &Menu::posLoadGame;
   mapPos_[2] = &Menu::posMapGenerator;
   mapPos_[3] = &Menu::posScore;
   mapPos_[4] = &Menu::posOption;
   mapPos_[5] = &Menu::posHelp;
   mapPos_[6] = &Menu::posCredit;
   mapPos_[7] = &Menu::posQuit;

   mapGo_[0] = &Menu::goNewGame;
   mapGo_[1] = &Menu::goLoadGame;
   mapGo_[2] = &Menu::goMapGenerator;
   mapGo_[3] = &Menu::goScore;
   mapGo_[4] = &Menu::goOption;
   mapGo_[5] = &Menu::goHelp;
   mapGo_[6] = &Menu::goCredit;
   mapGo_[7] = &Menu::goQuit;

   mapUpdateMenu_[0] = &Menu::updateNewGame;
   mapUpdateMenu_[1] = &Menu::updateCredit;
   mapUpdateMenu_[2] = &Menu::updateMapGenerator;
   mapUpdateMenu_[3] = &Menu::updateScore;
   mapUpdateMenu_[4] = &Menu::updateOption;
   mapUpdateMenu_[5] = &Menu::updateQuit;
   mapUpdateMenu_[6] = &Menu::updateLoadGame;
   mapUpdateMenu_[7] = &Menu::updateHelp;
   _gameSound.playMusic(GameSound::MENU);
}



bool	Menu::isPlaying() const
{
  return (_game);
}

bool	Menu::isCreateMap() const
{
  return (_isCreateMap);
}

bool	Menu::isLoadGame() const
{
  return (_isLoadGame);
}


bool	Menu::isQuit() const
{
  return (_isQuit);
}

void	Menu::stopPlaying()
{
   _mapSave->recupData();
   _gameSave->recupData();
   loadGame_->getPosListGame() = loadGame_->getListGame().begin();
   _posListMap = _listMap.begin();
  _game = false;
  _isCreateMap = false;
  _isLoadGame = false;
  xDestCamera_ = 0;
  yDestCamera_ = 0;
  zDestCamera_ = 0;
  inZoom_ = true;
  typeZoom_ = false;
  inMenu_ = false;
  _gameSound.playMusic(GameSound::MENU);
}

int	Menu::getNbrMap() const {return _listMap.size(); }


void	Menu::moveCamera()
{
  inMove_ = false;
  /* pos camera */
  if (xCamera_ != xDestCamera_)
    {
      if (xCamera_ < xDestCamera_)
	xCamera_ += speedCamera_;
      else
	xCamera_ -= speedCamera_;
      inMove_ = true;
    }
  if (yCamera_ != yDestCamera_)
    {
      if (yCamera_ < yDestCamera_)
	yCamera_ += speedCamera_;
      else
	yCamera_ -= speedCamera_;
      inMove_ = true;
    }
  if (zCamera_ != zDestCamera_)
    {
       if (zCamera_ < zDestCamera_)
	 zCamera_ += speedCamera_;
       else
	 zCamera_ -= speedCamera_;
       inMove_ = true;
    }
  /* Center Camera */
  if (xCenter_ != xDestCenter_)
    {
      if (xCenter_ < xDestCenter_)
	xCenter_ += speedCamera_;
      else
	xCenter_ -= speedCamera_;
       inMove_ = true;
    }
  if (yCenter_ != yDestCenter_)
    {
      if (yCenter_ < yDestCenter_)
	yCenter_ += speedCamera_;
      else
	yCenter_ -= speedCamera_;
      inMove_ = true;
    }
  if (zCenter_ != zDestCenter_)
    {
      if (zCenter_ < zDestCenter_)
	zCenter_ += speedCamera_;
      else
	zCenter_ -= speedCamera_;
      inMove_ = true;
    }
}


void  Menu::update()
{
  moveCamera();
  if (inMove_ == true)
    {
      if (inZoom_ == true)
	{
	  if (typeZoom_ == true)
	    (this->*mapZoom_[typeMenu_])(-1);
	  else
	    (this->*mapZoom_[typeMenu_])(1);
	}
      else
	{
	  (this->*mapZoom_[typeMenu_])(1);
	  if (dir_ == 1)
	    goRight();
	  if (dir_ == 0)
	    goLeft();
	}
      return;
    }
  inZoom_ = false;
  if (input_.isKeyDown(gdl::Keys::Back) == true)
    {
      if (typeMenu_ == 4)
	 if (option_->getAskKey() == true)
	   return;
      xDestCamera_ = 0;
      yDestCamera_ = 0;
      zDestCamera_ = 0;
      inZoom_ = true;
      typeZoom_ = false;
       inMenu_ = false;
    }
  if (inMenu_ == true)
    {
      if (input_.isKeyDown(gdl::Keys::Return) && typeMenu_ == 0)
	 {
	   _game = true;
	   _gameSound.stopMusic(GameSound::MENU);
	 }
      if (input_.isKeyDown(gdl::Keys::Return) && typeMenu_ == 2)
	{
	   _isCreateMap = true;
	   _gameSound.stopMusic(GameSound::MENU);
	}
      if (input_.isKeyDown(gdl::Keys::Return) && typeMenu_ == 6)
	{
	  if (!_listGame.empty())
	    {
	      _isLoadGame = true;
	      _gameSound.stopMusic(GameSound::MENU);
	    }
	}
      if (input_.isKeyDown(gdl::Keys::Return) && typeMenu_ == 5)
	{
	  _isQuit = quit_->getIsQuit();
	  if (_isQuit == false)
	    {
	      xDestCamera_ = 0;
	      yDestCamera_ = 0;
	      zDestCamera_ = 0;
	       inZoom_ = true;
	       typeZoom_ = false;
	       inMenu_ = false;
	    }
	}
      (this->*mapUpdateMenu_[typeMenu_])();
      return;
    }
  if (input_.isKeyDown(gdl::Keys::Return) == true)
    {
      (this->*mapGo_[typeMenu_])();
      typeZoom_ = true;
      inMenu_ = true;
       inZoom_ = true;
    }
  if (input_.isKeyDown(gdl::Keys::Left) == true && !inZoom_)
    {
      typeMenu_++;
      if (typeMenu_ > 7)
	typeMenu_ = 0;
       (this->*mapPos_[typeMenu_])();
       dir_ = 0;
    }
  if (input_.isKeyDown(gdl::Keys::Right) == true && !inZoom_)
    {
      typeMenu_--;
       if (typeMenu_ < 0)
	 typeMenu_ = 7;
       (this->*mapPos_[typeMenu_])();
       dir_ = 1;
    }
}

std::string		Menu::getSaveGame() const
{
  if (_listGame.empty())
    return "";
  return (*_posListGame);
}


void  Menu::draw(void)
{
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  glClearColor(165.0f, 216.0f, 255.0f, 1.0f);
  glClearDepth(1.0f);
  camera_->setPosition(xCamera_, yCamera_, zCamera_);
  camera_->setLook(xCenter_, yCenter_, zCenter_);
  camera_->update(gameClock_, input_, 3);
  glPushMatrix();
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  glDisable(GL_DEPTH_TEST);
  glOrtho(0.0f, 1200, 1000, 0.0, 0.0, 1.0);
  _texture.bindTexture(Texture::BGPRINCIPAL);
  glBegin(GL_QUADS);
  glTexCoord2f(0.0, 0.0);
  glVertex3f(0.0, 0.0, 0.0);
  glTexCoord2f(1.0, 0.0);
  glVertex3f(1200.0, 0.0, 0.0);
  glTexCoord2f(1.0, 1.0);
  glVertex3f(1200.0, 1000.0, 0.0);
  glTexCoord2f(0.0, 1.0);
  glVertex3f(0.0, 1000.0, 0.0);
  glEnd();
  glPopMatrix();
  glEnable(GL_DEPTH_TEST);
  camera_->setMode();
  
  /*Draw background & element Menu*/
  newGame_->draw();
  loadGame_->draw();
  mapGenerator_->draw();
  score_->draw();
  option_->draw();
  help_->draw();
  credit_->draw();
  quit_->draw();

  newGame_->drawTitle();
  loadGame_->drawTitle();
  mapGenerator_->drawTitle();
  score_->drawTitle();
  option_->drawTitle();
  help_->drawTitle();
  credit_->drawTitle();
  quit_->drawTitle();
}


void  Menu::unload(void)
{
}

 /* function update */
void	Menu::updateNewGame()
{
  newGame_->update(gameClock_, input_);
 }

void	Menu::updateLoadGame()
{
  loadGame_->update(gameClock_, input_);
}

void	Menu::updateMapGenerator()
{
  mapGenerator_->update(gameClock_, input_);
}

void	Menu::updateScore()
{
  score_->update(gameClock_, input_);
}

void	Menu::updateOption()
{
  option_->update(gameClock_, input_);
}

void	Menu::updateHelp()
{
  help_->update(gameClock_, input_);
}

void	Menu::updateCredit()
{
  credit_->update(gameClock_, input_);
}

void	Menu::updateQuit()
{
  quit_->update(gameClock_, input_);
}
/* ! END function update*/

/* Function GO */
void	Menu::goNewGame()
{
  xDestCamera_ = 0;
  yDestCamera_ = 0;
  zDestCamera_ = -2450;
  speedCamera_ = 122.5;
}

void	Menu::goLoadGame()
{
  xDestCamera_ = -1750;
  yDestCamera_ = 0;
  zDestCamera_ = -1750;
  speedCamera_ = 87.5;
}

void	Menu::goMapGenerator()
{
  xDestCamera_ = -2450;
  yDestCamera_ = 0;
  zDestCamera_ = 0;
  speedCamera_ = 122.5;
}

void	Menu::goScore()
{
  xDestCamera_ = -1750;
  yDestCamera_ = 0;
  zDestCamera_ = 1750;
  speedCamera_ = 87.5;
}

void	Menu::goOption()
{
  xDestCamera_ = 0;
  yDestCamera_ = 0;
  zDestCamera_ = 2450;
  speedCamera_ = 122.5;
}

void	Menu::goHelp()
{
  xDestCamera_ = 1750;
  yDestCamera_ = 0;
  zDestCamera_ = 1750;
  speedCamera_ = 87.5;
}

void	Menu::goCredit()
{
  xDestCamera_ = 2450;
  yDestCamera_ = 0;
  zDestCamera_ = 0;
  speedCamera_ = 122.5;
}

void	Menu::goQuit()
{
  xDestCamera_ = 1750;
  yDestCamera_ = 0;
  zDestCamera_ = -1750;
  speedCamera_ = 87.5;
}
/* ! END function pos*/

/* Function Pos */
void	Menu::posNewGame()
{
  xDestCenter_ = 0;
  yDestCenter_ = 0;
  zDestCenter_ = -3000;
  speedCamera_ = 150.0;
}

void	Menu::posLoadGame()
{
  xDestCenter_ = -3000;
  yDestCenter_ = 0;
  zDestCenter_ = -3000;
  speedCamera_ = 150.0;
}

void	Menu::posMapGenerator()
{
  xDestCenter_ = -3000;
  yDestCenter_ = 0;
  zDestCenter_ = 0;
  speedCamera_ = 150.0;
}

void	Menu::posScore()
{
  xDestCenter_ = -3000;
  yDestCenter_ = 0;
  zDestCenter_ = 3000;
  speedCamera_ = 150.0;
}

void	Menu::posOption()
{
  xDestCenter_ = 0;
  yDestCenter_ = 0;
  zDestCenter_ = 3000;
  speedCamera_ = 150.0;
}

void	Menu::posHelp()
{
  xDestCenter_ = 3000;
  yDestCenter_ = 0;
  zDestCenter_ = 3000;
  speedCamera_ = 150.0;
}

void	Menu::posCredit()
{
  xDestCenter_ = 3000;
  yDestCenter_ = 0;
  zDestCenter_ = 0;
  speedCamera_ = 150.0;
}

void	Menu::posQuit()
{
  xDestCenter_ = 3000;
  yDestCenter_ = 0;
  zDestCenter_ = -3000;
  speedCamera_ = 150.0;
}
/* ! END function pos*/

/* function zoom title*/
void	Menu::zoomNewGame(int mul)
{
  newGame_->zoomTitle(-5 * mul, 5 * mul,
		      -7 * mul, -7 * mul,
		      106 * mul, 106 * mul);
}

void	Menu::zoomLoadGame(int mul)
{
loadGame_->zoomTitle(-106 * mul, -106 * mul,
		     -7 * mul, -7 * mul,
		     -5 * mul, 5 * mul);
}

void	Menu::zoomMapGenerator(int mul)
{
  mapGenerator_->zoomTitle(106 * mul, 106 * mul,
			   -7 * mul, -7 * mul,
			    -5 * mul, 5 * mul);
}

void	Menu::zoomScore(int mul)
{
  score_->zoomTitle(78.5 * mul, 78.5 * mul,
		    -7 * mul, -7 * mul,
		    -78.5 * mul, -78.5 * mul);
}

void	Menu::zoomOption(int mul)
{
  option_->zoomTitle(-5 * mul, 5 * mul,
		     -7 * mul, -7 * mul,
		     -106 * mul, -106 * mul);
}

void	Menu::zoomHelp(int mul)
{
help_->zoomTitle(-78.5 * mul, -78.5 * mul,
		   -7 * mul, -7 * mul,
		   78.5 * mul, 78.5 * mul);
  
}

void	Menu::zoomCredit(int mul)
{
  credit_->zoomTitle(78.5 * mul, 78.5 * mul,
		       -7 * mul, -7 * mul,
		       78.5 * mul, 78.5 * mul);
}

void	Menu::zoomQuit(int mul)
{
  quit_->zoomTitle(-78.5 * mul, -78.5 * mul,
		   -7 * mul, -7 * mul,
		   -78.5 * mul, -78.5 * mul);
}
/* ! END function zoom*/



void	Menu::goRight()
{
  if (typeMenu_ == 7)
    (this->*mapZoom_[0])(-1);
  if (typeMenu_ == 1)
    (this->*mapZoom_[2])(-1);
  if (typeMenu_ == 3)
    (this->*mapZoom_[4])(-1);
  if (typeMenu_ == 5)
    (this->*mapZoom_[6])(-1);
  if (typeMenu_ == 0)
    (this->*mapZoom_[1])(-1);
  if (typeMenu_ == 2)
    (this->*mapZoom_[3])(-1);
  if (typeMenu_ == 4)
    (this->*mapZoom_[5])(-1);
  if (typeMenu_ == 6)
    (this->*mapZoom_[7])(-1);
}

void	Menu::goLeft()
{
  if (typeMenu_ == 1)
    (this->*mapZoom_[0])(-1);
  if (typeMenu_ == 3)
    (this->*mapZoom_[2])(-1);
  if (typeMenu_ == 5)
    (this->*mapZoom_[4])(-1);
  if (typeMenu_ == 7)
    (this->*mapZoom_[6])(-1);
  if (typeMenu_ == 2)
    (this->*mapZoom_[1])(-1);
  if (typeMenu_ == 4)
    (this->*mapZoom_[3])(-1);
  if (typeMenu_ == 6)
    (this->*mapZoom_[5])(-1);
  if (typeMenu_ == 0)
    (this->*mapZoom_[7])(-1);
}

Texture*	Menu::getTexture()
{ return (&_texture); }
