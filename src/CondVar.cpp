//
// CondVar.cpp for  in /home/grizet_j//C++/plazza/src
// 
// Made by jean grizet
// Login   <grizet_j@epitech.net>
// 
// Started on  Mon Apr 15 13:13:27 2013 jean grizet
// Last update Sat Apr 20 22:15:57 2013 jean grizet
//

#include "CondVar.hh"

CondVar::CondVar()
{
  if (pthread_mutex_init(&_mutex, NULL))
    {
      std::cerr << CONDVAR_INIT_ERR << std::endl;
      exit(EXIT_FAILURE);
    }
  if (pthread_cond_init(&_cond, NULL))
    {
      std::cerr << CONDVAR_INIT_ERR << std::endl;
      exit(EXIT_FAILURE);
    }
  pthread_mutex_lock(&_mutex);
}

CondVar::~CondVar(void) 
{
  if (pthread_mutex_destroy(&_mutex))
    {
      std::cerr << CONDVAR_INIT_ERR << std::endl;
      exit(EXIT_FAILURE);
    }
}

void  CondVar::wait(void)
{
  pthread_cond_wait(&(this->_cond), &(this->_mutex));
}

void  CondVar::signal(void)
{
  pthread_cond_signal(&(this->_cond));
}

void  CondVar::broadcast(void)
{
  pthread_cond_broadcast(&(this->_cond));
}
