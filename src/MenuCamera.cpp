//
// MenuCamera.cpp for  in /home/grizet_j//C++/bomberman/src
// 
// Made by jean grizet
// Login   <grizet_j@epitech.net>
// 
// Started on  Mon Apr 29 12:24:29 2013 jean grizet
// Last update Wed Jun  5 18:18:52 2013 jean-charles lecoq
//

#include	<iostream>
#include "MenuCamera.hh"

MenuCamera::MenuCamera(void)
  : position_(0.0f, 0.0f, 0.0f), rotation_(0.0f, 0.0f, 0.0f), center_(0.0f, 0.0f, -3000.0f)
{
  std::cout << "New MenuCamera " << std::endl;
}
void MenuCamera::initialize(void)
{
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  gluPerspective(60.0f, 1980.0f/1200.0f, 1.0f, 1000000.0f);
  gluLookAt(position_.x, position_.y, position_.z,
	    center_.x, center_.y, center_.z,
	    0.0f, 1.0f, 0.0f);
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();
  glEnable(GL_DEPTH_TEST);
  glDepthFunc(GL_LEQUAL);
}

void	MenuCamera::setCenter(float x, float y, float z)
{

  center_.x = x;
  center_.y = y;
  center_.z = z;
}

void	MenuCamera::setPosition(float x, float y, float z)
{
  position_.x = x;
  position_.z = z;
  position_.y = y;
}

void MenuCamera::update()
{
  this->initialize();
}
