//
// Event.cpp for  in /home/grizet_j//C++/bomberman/src
// 
// Made by jean grizet
// Login   <grizet_j@epitech.net>
// 
// Started on  Tue May 14 10:57:24 2013 jean grizet
// Last update Fri Jun  7 16:30:24 2013 jean grizet
//

#include "Event.hh"

Event::Event(AObject *aobj, gdl::GameClock const & gclock,	\
	     gdl::Input& inp, bool ex) :
  _obj(aobj),
  _clock(gclock),
  _input(inp),
  _exit(ex)
{ (void)aobj; (void)gclock; (void)inp; }

Event::~Event() {}

void		Event::run()
{
  if (_obj != NULL)
    _obj->update(_clock, _input);
}

bool		Event::exit() const
{ return (this->_exit); }
