//
// NewGame.cpp for NewGame in /home/lecoq_j//Tech2/ProjetC++/Bomberman/src/menu
//
// Made by jean-charles lecoq
// Login   <lecoq_j@epitech.net>
// 
// Started on  Wed May  8 22:51:46 2013 jean-charles lecoq
// Last update Sat Jun  8 12:33:06 2013 jean-charles lecoq
//

#include	"MenuSettings.hh"
#include	<iostream>

MenuSettings::MenuSettings(Option& opt, GameSound& gc, Texture& texture)
  :gameSound_(gc),
   option_(opt),
   _texture(texture),
   mappingJ1_(option_.getKeyJ1()),
   mappingJ2_(option_.getKeyJ2())   
{
}

MenuSettings::~MenuSettings()
{ }

void	MenuSettings::initialize()
{
  /*init map for bind*/
  setBindP1_[0] = &MenuSettings::bindKeyP1;
  setBindP1_[1] = &MenuSettings::bindP1;
  setBindP1_[2] = &MenuSettings::bindP1U;
  setBindP1_[3] = &MenuSettings::bindP1D;
  setBindP1_[4] = &MenuSettings::bindP1R;
  setBindP1_[5] = &MenuSettings::bindP1L;
  setBindP1_[6] = &MenuSettings::bindP1B;
  setBindP2_[0] = &MenuSettings::bindKeyP2;
  setBindP2_[1] = &MenuSettings::bindP2;
  setBindP2_[2] = &MenuSettings::bindP2U;
  setBindP2_[3] = &MenuSettings::bindP2D;
  setBindP2_[4] = &MenuSettings::bindP2R;
  setBindP2_[5] = &MenuSettings::bindP2L;
  setBindP2_[6] = &MenuSettings::bindP2B;
  setBindSound_[0] = &MenuSettings::bindSound;
  setBindSound_[1] = &MenuSettings::bindMusicVol;
  setBindSound_[2] = &MenuSettings::bindMusicMute;
  setBindSound_[3] = &MenuSettings::bindEffectsVol;
  setBindSound_[4] = &MenuSettings::bindEffectsMute;
  /*End init map*/
  
  mappingJ1_ = option_.getKeyJ1();
  mappingJ2_ = option_.getKeyJ2();

  pSound_ = false;
  pOne_ = true;
  pTwo_ = false;
  time_ = 0.0;
  menuP1_ = 0;
  menuP2_ = 0;
  menuSound_ = 0;
  askKey_ = false;

  volM_ = 100;
  volE_ = 100;
  muteM_ = 0;
  muteE_ = 0;
  up_ = new Plan(_texture);
  down_ = new Plan(_texture);
  left_ = new Plan(_texture);
  right_ = new Plan(_texture);
  bombe_ = new Plan(_texture);

  up_->initialize(-150, -250, -100, -50, 3470, 3470);
  down_->initialize(-150, -250, -200, -150, 3470, 3470);
  right_->initialize(-150, -250, -300, -250, 3470, 3470);
  left_->initialize(-150, -250, -400, -350, 3470, 3470);
  bombe_->initialize(-150, -250, -480, -430, 3470, 3470);

  showMusicVolume_ = new ShowNumber(_texture);
  showEffectsVolume_ = new ShowNumber(_texture);
  showMuteMusic_ = new Plan(_texture);
  showMuteEffects_ = new Plan(_texture);

  showMuteMusic_->update(Texture::OFF);
  showMuteEffects_->update(Texture::OFF);

  showMusicVolume_->initialize(3, 55, -100, 3470, -50, 50, 0, 1);
  showEffectsVolume_->initialize(3, 60, -420, 3470, -50, 50, 0, 1);
  showMuteMusic_->initialize(-90, -200, -250, -135, 3470, 3470);
  showMuteEffects_->initialize(-110, -220, -565, -450, 3470, 3470);
  showMusicVolume_->update(volM_);
  showEffectsVolume_->update(volE_);

  mappingJ1_[0] = gdl::Keys::W;
  mappingJ1_[1] = gdl::Keys::S;
  mappingJ1_[2] = gdl::Keys::D;
  mappingJ1_[3] = gdl::Keys::A;
  mappingJ1_[4] = gdl::Keys::Space;

  mappingJ2_[0] = gdl::Keys::Up;
  mappingJ2_[1] = gdl::Keys::Down;
  mappingJ2_[2] = gdl::Keys::Right;
  mappingJ2_[3] = gdl::Keys::Left;
  mappingJ2_[4] = gdl::Keys::Numpad0;

  drawKeys();
  x1Title_ = 700;
  x2Title_ = -700;
  y1Title_ = 240.5;
  y2Title_ = 570.5;
  z1Title_ = 3400;
  z2Title_ = 3400;

  x1_ = 990;
  x2_ = -990;
  y1_ = -600;
  y2_ = 600;
  z1_ = 3480;
  z2_ = 3480;
}

bool	MenuSettings::checkInput(gdl::Keys::Key k)
{
  int	i;

  for (i = 0; i <= 4; i++)
    {
      if ((menuP1_ != (i + 2)) && (mappingJ1_[i] == k))
	return false;
      if ((menuP2_ != (i + 2)) && (mappingJ2_[i] == k))
	return false;
    }
  return true;
}

void	MenuSettings::checkMapping(gdl::Keys::Key k)
{
  if (pOne_ == true)
    {
      if (menuP1_ == 2)
	mappingJ1_[0] = k;
      if (menuP1_ == 3)
	mappingJ1_[1] = k;
      if (menuP1_ == 4)
	mappingJ1_[2] = k;
      if (menuP1_ == 5)
	mappingJ1_[3] = k;
      if (menuP1_ == 6)
	mappingJ1_[4] = k;
    }
  if (pTwo_ == true)
    {
      if (menuP2_ == 2)
	mappingJ2_[0] = k;
      if (menuP2_ == 3)
	mappingJ2_[1] = k;
      if (menuP2_ == 4)
	mappingJ2_[2] = k;
      if (menuP2_ == 5)
	mappingJ2_[3] = k;
      if (menuP2_ == 6)
	mappingJ2_[4] = k;
    }
}

void	MenuSettings::askKey(gdl::Input& input)
{
  int	i;

  i = 0;
  if (input.isKeyDown(gdl::Keys::Space) == true)
    {
      if (checkInput(gdl::Keys::Space) == true)
	{	      
	  checkMapping(gdl::Keys::Space);
	  askKey_ = false;
	  return;
	}
    }
  for (i = 0; i <= 25; i++)
    {
      if (input.isKeyDown((gdl::Keys::Key)i) == true)
	{
	  if (checkInput((gdl::Keys::Key)i) == true)
	    {
	      checkMapping(((gdl::Keys::Key)i));
	      askKey_ = false;
	      return;
	    }
	}
    }
  for (i = 71; i <= 84; i++)
    {
      if (input.isKeyDown((gdl::Keys::Key)i) == true)
	{
	  if (checkInput((gdl::Keys::Key)i) == true)
	    {
	      checkMapping(((gdl::Keys::Key)i));
	      askKey_ = false;
	      return;
	    }
	}
    } 
}

void	MenuSettings::update(gdl::GameClock const & gameClock, gdl::Input &input)
{

  if (time_ > gameClock.getTotalGameTime())
    return;
  if (askKey_ == true)
    {
      askKey(input);
      return;
    }
  if ((input.isKeyDown(gdl::Keys::Left) == true) || (input.isKeyDown(gdl::Keys::Right) == true))
    {
      if ((pOne_ == true) && ((menuP1_ >= 2) && (menuP1_ <= 6)))
	return;
      if ((pTwo_ == true) && ((menuP2_ >= 2) && (menuP2_ <= 6)))
	return;
      if ((pOne_ == true) && (menuP1_ == 0))
	{
	  menuSound_ = 0;
	  pSound_ = true;
	  pOne_ = false;
	  pTwo_ = false;
	  time_ = gameClock.getTotalGameTime() + 0.2;
	  return;
	}
      if ((pTwo_ == true) && (menuP2_ == 0))
	{
	  menuSound_ = 0;
	  pSound_ = true;
	  pOne_ = false;
	  pTwo_ = false;
	  time_ = gameClock.getTotalGameTime() + 0.2;
	  return;
	}
      if ((pOne_ == true) && (menuP1_ == 1))
	{
	  pTwo_ = true;
	  pOne_ = false;
	  pSound_ = false;
	  menuP2_ = 1;
	  menuP1_ = 1;
	  time_ = gameClock.getTotalGameTime() + 0.2;
	  return;
	}
      if ((pTwo_ == true) && (menuP2_ == 1))
	{
	  pSound_ = false;
	  pOne_ = true;
	  pTwo_ = false;
	  menuP1_ = 1;
	  menuP2_ = 1;
	  time_ = gameClock.getTotalGameTime() + 0.2;
	  return;
	}
      if (pSound_ == true)
	{
	  if (menuSound_ == 0)
	    {
	      pOne_ = true;
	      pTwo_ = false;
	      pSound_ = false;
	      menuP1_ = 0;
	      time_ = gameClock.getTotalGameTime() + 0.2;
	      return;
	    }
	  if (menuSound_ == 1)
	    {
	      if (input.isKeyDown(gdl::Keys::Right) == true)
		volM_ += 5;
	      if (input.isKeyDown(gdl::Keys::Left) == true)
		volM_ -= 5;
	      if (volM_ > 100)
		volM_ = 100;
	      if (volM_ < 0)
		volM_ = 0;
	      gameSound_.setVolumeMusic(volM_);
	      gameSound_.playMusic(GameSound::MENU);
	      showMusicVolume_->update(volM_);
	      time_ = gameClock.getTotalGameTime() + 0.2;
	      return;
	    }
	  if (menuSound_ == 2)
	    {
	      if (muteM_ == 0)
		{
		  muteM_ = 1;
		  showMuteMusic_->update(Texture::ON);
		  gameSound_.setVolumeMusic(0);
		  gameSound_.playMusic(GameSound::MENU);
		}
	      else
		{
		  muteM_ = 0;
		  showMuteMusic_->update(Texture::OFF);
		  gameSound_.setVolumeMusic(volM_);
		  gameSound_.playMusic(GameSound::MENU);
		}
	      time_ = gameClock.getTotalGameTime() + 0.2;
	      return;
	    }
	  if (menuSound_ == 3)
	    {
	      if (input.isKeyDown(gdl::Keys::Right) == true)
		volE_ += 5;
	      if (input.isKeyDown(gdl::Keys::Left) == true)
		volE_ -= 5;
	      if (volE_ > 100)
		volE_ = 100;
	      if (volE_ < 0)
		volE_ = 0;
	      gameSound_.setVolumeEffect(volE_);
	      showEffectsVolume_->update(volE_);
	      time_ = gameClock.getTotalGameTime() + 0.2;
	      return;
	    }
	  if (menuSound_ == 4)
	    {
	      if (muteE_ == 0)
		{
		  muteE_ = 1;
		  showMuteEffects_->update(Texture::ON);
		  gameSound_.setVolumeEffect(0);
		}
	      else
		{
		  muteE_ = 0;
		  showMuteEffects_->update(Texture::OFF);
		  gameSound_.setVolumeEffect(volE_);
		}
	      time_ = gameClock.getTotalGameTime() + 0.2;
	    }
	  return;
	}
      if (pSound_ == false)
	{
	  pOne_ = false;
	  pTwo_ = false;
	  pSound_ = true;
	  menuSound_ = 0;
	}
      time_ = gameClock.getTotalGameTime() + 0.2;
      return;
    }
  if (input.isKeyDown(gdl::Keys::Down) == true)
    {
      if (pOne_ == true)
	upDownJ1(1);
      if (pTwo_ == true)
	upDownJ2(1);
      if (pSound_ == true)
	upDownSound(1);
      time_ = gameClock.getTotalGameTime() + 0.2;
      return;
    }
  if (input.isKeyDown(gdl::Keys::Up) == true)
    {
      if (pOne_ == true)
	upDownJ1(-1);
      if (pTwo_ == true)
	upDownJ2(-1);
      if (pSound_ == true)
	upDownSound(-1);
      time_ = gameClock.getTotalGameTime() + 0.2;
      return;
    }
  if (input.isKeyDown(gdl::Keys::Return) == true)
    {
      if (pOne_ == true)
	if ((menuP1_ >= 2) && (menuP1_ <= 6))
	  askKey_ = true;
      if (pTwo_ == true)
	if ((menuP2_ >= 2) && (menuP2_ <= 6))
	  askKey_ = true;
    }
}

void	MenuSettings::upDownJ2(int dir)
{
  menuP2_ += 1 * dir;
  if (menuP2_ > 6)
    menuP2_ = 0;
  else
    if (menuP2_ == -1)
      menuP2_ = 6;

}

void	MenuSettings::upDownJ1(int dir)
{
  menuP1_ += 1 * dir;
  if (menuP1_ > 6)
    menuP1_ = 0;
  else
    if (menuP1_ == -1)
      menuP1_ = 6;
}

void	MenuSettings::upDownSound(int dir)
{
  menuSound_ += 1 * dir;
  if (menuSound_ > 4)
    menuSound_ = 0;
  else
    if (menuSound_ == -1)
      menuSound_ = 4;
}

void	MenuSettings::draw()
{
  /* draw Background */
  glPushMatrix();
  glLoadIdentity();
  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
  /*bind */
  if (pOne_ == true)
    (this->*setBindP1_[menuP1_])();
  else
    if (pTwo_ == true)
      (this->*setBindP2_[menuP2_])();
    else
      (this->*setBindSound_[menuSound_])();
  /* ! end bind */
  glBegin(GL_QUADS);
  glTexCoord2f(0.0, 0.0);
  glVertex3f(x1_, y2_, z1_);
  glTexCoord2f(0.0, 1.0f);
  glVertex3f(x1_, y1_, z1_);
  glTexCoord2f(1.0f, 1.0f);
  glVertex3f(x2_, y1_, z2_);
  glTexCoord2f(1.0f, 0.0);
  glVertex3f(x2_, y2_, z2_);
  glEnd();
  glDisable(GL_BLEND);
  glDisable(GL_TEXTURE_2D);
  /* ! end draw background*/
  if ((pOne_ == true) || (pTwo_ == true))
    {
      drawKeys();
      up_->draw();
      down_->draw();
      right_->draw();
      left_->draw();
      bombe_->draw();
     
    }
  if (pSound_ == true)
    {

      showMuteMusic_->draw();
      showMuteEffects_->draw();
      showEffectsVolume_->draw();
      showMusicVolume_->draw();
    }
  if (askKey_ == true)
    {
      glPushMatrix();
      glLoadIdentity();
      glEnable(GL_BLEND);
      glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
      _texture.bindTexture(Texture::SETTINGSPRESSED);
      glBegin(GL_QUADS);
      glTexCoord2f(0.0, 0.0);
      glVertex3f(990, 600, 3300);
      glTexCoord2f(0.0, 1.0f);
      glVertex3f(990, -600, 3300);
      glTexCoord2f(1.0f, 1.0f);
      glVertex3f(-990, -600, 3300);
      glTexCoord2f(1.0f, 0.0);
      glVertex3f(-990, 600, 3300);
      glEnd();
      glDisable(GL_BLEND);
      glDisable(GL_TEXTURE_2D);
    }
}

void	MenuSettings::drawKeys()
{
  if (pOne_ == true)
    {
      up_->update(_linkTexture.getKeys(mappingJ1_[0]));
      down_->update(_linkTexture.getKeys(mappingJ1_[1]));
      right_->update(_linkTexture.getKeys(mappingJ1_[2]));
      left_->update(_linkTexture.getKeys(mappingJ1_[3]));
      bombe_->update(_linkTexture.getKeys(mappingJ1_[4]));
    }
  else
    {
      up_->update(_linkTexture.getKeys(mappingJ2_[0]));
      down_->update(_linkTexture.getKeys(mappingJ2_[1]));
      right_->update(_linkTexture.getKeys(mappingJ2_[2]));
      left_->update(_linkTexture.getKeys(mappingJ2_[3]));
      bombe_->update(_linkTexture.getKeys(mappingJ2_[4]));
    }
}

void  MenuSettings::bindKeyP1()
{
  _texture.bindTexture(Texture::SETTINGSKEYP1);
}
void  MenuSettings::bindKeyP2()
{
  _texture.bindTexture(Texture::SETTINGSKEYP2);
}
void  MenuSettings::bindP1()
{
  _texture.bindTexture(Texture::SETTINGSP1);
}
void  MenuSettings::bindP2()
{
  _texture.bindTexture(Texture::SETTINGSP2);
}
void  MenuSettings::bindP1U()
{
  _texture.bindTexture(Texture::SETTINGSP1U);
}
void  MenuSettings::bindP1D()
{
  _texture.bindTexture(Texture::SETTINGSP1D);
}
void  MenuSettings::bindP1R()
{
  _texture.bindTexture(Texture::SETTINGSP1R);
}
void  MenuSettings::bindP1L()
{
  _texture.bindTexture(Texture::SETTINGSP1L);
}
void  MenuSettings::bindP1B()
{
  _texture.bindTexture(Texture::SETTINGSP1B);
}
void  MenuSettings::bindP2U()
{  
  _texture.bindTexture(Texture::SETTINGSP2U);
}
void  MenuSettings::bindP2D()
{
  _texture.bindTexture(Texture::SETTINGSP2D);
}
void  MenuSettings::bindP2R()
{
  _texture.bindTexture(Texture::SETTINGSP2R);
}
void  MenuSettings::bindP2L()
{
  _texture.bindTexture(Texture::SETTINGSP2L);
}
void  MenuSettings::bindP2B()
{
  _texture.bindTexture(Texture::SETTINGSP2B);
}
void  MenuSettings::bindSound()
{
  _texture.bindTexture(Texture::SETTINGSSOUND);
}
void  MenuSettings::bindMusicVol()
{
  _texture.bindTexture(Texture::SETTINGSMUSICVOL);
}
void  MenuSettings::bindMusicMute()
{
  _texture.bindTexture(Texture::SETTINGSMUSICMUTE);
}
void  MenuSettings::bindEffectsVol()
{
  _texture.bindTexture(Texture::SETTINGSEFFECTVOL);
}
void  MenuSettings::bindEffectsMute()
{  
  _texture.bindTexture(Texture::SETTINGSEFFECTMUTE);
}

void    MenuSettings::zoomTitle(float x1, float x2, float y1, float y2, float z1, float z2)
{
  x1Title_ += x1;
  x2Title_ += x2;
  y1Title_ += y1;
  y2Title_ += y2;
  z1Title_ += z1;
  z2Title_ += z2;
}

void    MenuSettings::drawTitle()
{
  glPushMatrix();
  glLoadIdentity();
  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
  _texture.bindTexture(Texture::TITLESET);
  glBegin(GL_QUADS);
  glTexCoord2f(0.0, 0.0);
  glVertex3f(x1Title_, y2Title_, z1Title_);
  glTexCoord2f(0.0, 1.0f);
  glVertex3f(x1Title_, y1Title_, z1Title_);
  glTexCoord2f(1.0f, 1.0f);
  glVertex3f(x2Title_, y1Title_, z2Title_);
  glTexCoord2f(1.0f, 0.0);
  glVertex3f(x2Title_, y2Title_, z2Title_);
  glEnd();
  glDisable(GL_BLEND);
  glDisable(GL_TEXTURE_2D);
}

bool	MenuSettings::getAskKey() const {return askKey_;}
