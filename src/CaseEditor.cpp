//
// CaseEditor.cpp for bomberman in /home/grilla_b//Documents/epi/projet_tek2/boomberman/repo/bomberman/map_editor
// 
// Made by brian grillard
// Login   <grilla_b@epitech.net>
// 
// Started on  Sun Jun  2 14:36:14 2013 brian grillard
// Last update Sun Jun  2 16:18:58 2013 brian grillard
//

#include "CaseEditor.hh"

CaseEditor::CaseEditor(int x, int y, int type, AObject *obj)
{
  _pos.x = (x * 350.0f);
  _pos.y = 1.0f;
  _pos.z = (y * 350.0f);
  _object = obj;
  _type = type;
  if (_object != NULL)
    {
      _object->position_.x = _pos.x;
      _object->position_.y = _pos.y;
      _object->position_.z = _pos.z;
    }
}

CaseEditor::~CaseEditor() {}

void                    CaseEditor::setObject(AObject* object)
{
  this->_object = object;
  if (object != NULL)
    {
      object->position_.x = _pos.x;
      object->position_.y = _pos.y;
      object->position_.z = _pos.z;
    }
}

void                    CaseEditor::setViseur(AObject *viseur)
{
  this->_viseur = viseur;
  if (_viseur != NULL)
    {
      viseur->position_.x = _pos.x;
      viseur->position_.y = _pos.y;
      viseur->position_.z = _pos.z;
    }
}

void                    CaseEditor::setType(int type) { _type = type; }
int                     CaseEditor::getType() const { return (_type); }
int                     CaseEditor::getX() const { return (this->_x); }
int                     CaseEditor::getY() const { return (this->_y); }
AObject*                CaseEditor::getObject() const { return (this->_object); }
AObject*                CaseEditor::getViseur() const { return (this->_viseur); }
void                    CaseEditor::setX(int x) { this->_x = x; }
void                    CaseEditor::setY(int y) { this->_y = y; }
