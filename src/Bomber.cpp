//
// Bomber.cpp for  in /home/grizet_j//C++/bomberman/src
// 
// Made by jean grizet
// Login   <grizet_j@epitech.net>
// 
// Started on  Mon Apr 29 12:15:19 2013 jean grizet
// Last update Sun Jun  9 17:53:20 2013 jean grizet
//

#include "Bomber.hh"
#include "SaveMap.hh"

Bomber::Bomber(Option& opt, GameSound& gs, gdl::GameClock& gc, gdl::Input& i) :
  _option(opt), _sound(gs),
  _in(*(new Mutex), *(new CondVar)),
  _out(*(new Mutex), *(new CondVar)),
  _pool(new Pool<IEvent,IEvent>(_in, _out, _thread)),
  gameClock_(gc), input_(i),
  _hud(opt)
{
  _map = NULL;
  _updateTime = 0.0;
  _back = new gdl::Image(gdl::Image::load("data/assets/mars.jpg"));
  _score = new gdl::Image(gdl::Image::load("data/assets/score.png"));
  _player1 = NULL;
  _player2 = NULL;
  _end = 0;
  _glob = NULL;
  _loaded = false;
}

Bomber::~Bomber()
{
  _pool->stop();
  usleep(25);
  if (_map != NULL)
    delete (_map);
  if (_glob != NULL)
    delete (_glob);
  delete (_score);
  delete (_back);
}

void  Bomber::initialize(void)
{
  _loaded = true;
  _end = 0;
  _map = new Map;
  if (_option.getTypeMap() != "default")
    {
      std::string file("./data/map/");

      file.append(_option.getTypeMap());
      if (!_map->initializeMap(file))
	{
	  _loaded = false;
	  _end = 3;
	  return;
	}
      _option.setSizeX(_map->getsizeX());
      _option.setSizeY(_map->getsizeY());
    }
  else
    _map->initializeMap(_option.getSizeX(), _option.getSizeY());
  _cam.initialize(_option);
  _glob = new Global_IA(_map);
  initPool();
  initPlayer();
  initIA();
  initEvent();
  _pool->run();  
  _menu.initialize();
  _sound.playMusic(GameSound::GAME);
  _timer = time(NULL);
}

void	Bomber::initPool()
{
  for (int i = 0; i < (_option.getPlayer() + _option.getIA() + 1) * 2; i++)
    _thread.push_back(new Thread<Consumer>(new Consumer(_in, _out),	\
					   &Consumer::run));
}

void  Bomber::initialize(const std::string& file)
{
  std::string	save("./data/save/data/");

  LoadGame	loader;
  _loaded = true;
  _end = 0;
  save.append(file);
  _cam.initialize(_option);
  _map = new Map;
  if (!loader.load(_map, _option, &_glob, save,			\
		   &_players, gameClock_, _sound, &_obj))
    {
      _loaded = false;
      _end = 3;
      return;
    }
  _player1 = _players[0];
  if (_option.getPlayer() == 2)
    _player2 = _players[1];
  loader.setState(&_players);
  initPool();
  initEvent();
  _menu.initialize();
  _sound.playMusic(GameSound::GAME);
  _timer = time(NULL);
  _pool->run();
}


void	Bomber::initEvent()
{
  for (std::list<AObject *>::iterator it = _obj.begin();	\
       it != _obj.end(); ++it)
    {
      (*it)->initialize();
      _event.push_front((*it)->createEvent(gameClock_, input_));
    }
}
void	Bomber::initIA()
{
  Model::Bomberman *tmp = NULL;

  for (int i = 0; i < _option.getIA(); i++)
    {
      int c = (i + 1) * 2;
      if (c >= _option.getSizeX() || c >= _option.getSizeY())
	break;
      tmp = new IA(10 + i, c, c, _map, _glob,	\
		   &_players, _sound, &gameClock_);
      _players.push_back(tmp);
      _obj.push_front(tmp);
      _obj.push_front(tmp->getBagOfBomb());
    }
}

void	Bomber::initPlayer()
{
  _player1 = new Model::Bomberman(8, 0, 0, _option.getKeyJ1()[0],	\
				  _option.getKeyJ1()[1],		\
				  _option.getKeyJ1()[2],		\
				  _option.getKeyJ1()[3],		\
				  _option.getKeyJ1()[4],		\
				  this->_map, _sound);
  _players.push_back(_player1);
  _obj.push_front(_player1);
  _obj.push_front(_player1->getBagOfBomb());
  if (_option.getPlayer() == 2)
    {
      _player2 = new Model::Bomberman(9, _option.getSizeX() - 1,	\
				      _option.getSizeY() - 1,		\
				      _option.getKeyJ2()[0],		\
				      _option.getKeyJ2()[1],		\
				      _option.getKeyJ2()[2],		\
				      _option.getKeyJ2()[3],		\
				      _option.getKeyJ2()[4],		\
				      this->_map, _sound);
      _players.push_back(_player2);
      _obj.push_back(_player2);
      _obj.push_front(_player2->getBagOfBomb());
    }
}

void  Bomber::update(void)
{
  manageEndGame();
  if (_end == 4)
    {
      manageMenu();
      return;
    }
  if (_end == 0)
    {
      if (input_.isKeyDown(gdl::Keys::Escape) == true)
	{
	  gameClock_.pause();
	  _end = 4;
	}
      else
	{
	  _glob->update();
	  for (std::list<IEvent *>::iterator it = _event.begin();	\
	       it != _event.end(); ++it)
	    _pool->pushTask(*it);
	  usleep(575);
	}
    }
}

void	Bomber::manageEndGame()
{
  if (((_option.getMode() == 1 &&					\
	time(NULL) >= _timer + _option.getTime()) ||			\
       ((_player1 == NULL || _player1->isDead()) &&			\
	(_player2 == NULL || _player2->isDead())) ||			\
       _glob->getNbBox() == 0) && _end == 0)
    {
      _timer = time(NULL);
      _updateTime = gameClock_.getTotalGameTime();
      _end = 1;
    }
  if (_end == 1 && gameClock_.getTotalGameTime() - _updateTime >= 3.5)
    {
      _end = 2;
    }
}

void	Bomber::manageMenu()
{
  _menu.update(gameClock_, input_);
  if (_menu.getAction() == 1)
    _end = 0;
  else if (_menu.getAction() == 2)
    {
      time_t rawtime;

      time (&rawtime);
      std::string filename;

      filename.append("./data/save/data/");
      filename.append(ctime(&rawtime));
      filename.erase(filename.end() - 1);
      _save.save(_players, filename.c_str(), _map);
      _end = 0;
    }
  else if (_menu.getAction() == 3)
    {
      _end = 1;
      _updateTime = gameClock_.getTotalGameTime();
    }
  if (_menu.getAction() != 0)
    {
      _menu.initialize();
      gameClock_.play();
    }
}

void	Bomber::drawPlayer()
{
  int	absx = 0.0;
  int	absy = 0.0;
  bool	dead = false;

  dead = _player1->isDead();
  if (dead != true && _player2 != NULL)
    dead = _player2->isDead();
  if (_option.getPlayer() == 2)
    {
      absx = (_player1->getPosX() - _player2->getPosX());
      absx = (absx >= 0 ? absx : -absx);
      absy = (_player1->getPosY() - _player2->getPosY());
      absy = (absy >= 0 ? absy : -absy);
    }
  if ((absx <= 10 && absy <= 10) || dead)
    drawMiddle();
  else
    {
      _cam.setPosition(_player1->position_.x,
		       _player1->position_.y + 3200.0f,
		       _player1->position_.z + 2000.0f);
      _cam.setLook(_player1->position_.x,
		   _player1->position_.y,
		   _player1->position_.z);
      _cam.update(gameClock_, input_, 1);
      drawBack();
      _cam.setMode();
      _map->draw(_player1->getPosX(), _player1->getPosY(), 15);
      _cam.setPosition(_player2->position_.x,
		       _player2->position_.y + 3200.0f,
		       _player2->position_.z + 2000.0f);
      _cam.setLook(_player2->position_.x,
		   _player2->position_.y,
		   _player2->position_.z);
      _cam.update(gameClock_, input_, 2);
      drawBack();
      _cam.setMode();
      _map->draw(_player2->getPosX(), _player2->getPosY(), 15);
    }
}

void	Bomber::drawMiddle()
{
  double	x, y, z;
  
  if (_option.getPlayer() == 2 && (_player1 == NULL || _player1->isDead()))
    {
      Model::Bomberman *tmp = _player2;
      _player2 = _player1;
      _player1 = tmp;
    }
  if (_player2 == NULL || _player2->isDead())
    {
      x = _player1->position_.x;
      y = _player1->position_.y;
      z = _player1->position_.z;
    }
  else
    {
      x = (_player1->position_.x + _player2->position_.x) / 2 ;
      y = _player1->position_.y;
      z =  (_player1->position_.z + _player2->position_.z) / 2;
    }
  _cam.setPosition(x, y + 3200.0f, z + 2000.0f);
  _cam.setLook(x, y, z);
  _cam.update(gameClock_, input_, 0);
  drawBack();
  _cam.setMode();
  _map->draw(_player1->getPosX(), _player1->getPosY(), 20);
}

void	Bomber::drawBack()
{
  glPushMatrix();
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  glDisable(GL_DEPTH_TEST);
  glOrtho(0.0f, _option.getScreenX(), _option.getScreenY(), 0.0, 0.0, 1.0);
  _back->bind();
  glBegin(GL_QUADS);
  glTexCoord2f(0.0, 0.0);
  glVertex3f(0.0, 0.0, 0.0);
  glTexCoord2f(1.0, 0.0);
  glVertex3f(1200.0, 0.0, 0.0);
  glTexCoord2f(1.0, 1.0);
  glVertex3f(1200.0, 1000.0, 0.0);
  glTexCoord2f(0.0, 1.0);
  glVertex3f(0.0, 1000.0, 0.0);
  glEnd();
  glPopMatrix();
}

void	Bomber::drawScore()
{
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  glViewport(0, 0, _option.getScreenX(), _option.getScreenY());
  gluPerspective(70.0f, _option.getScreenX() / _option.getScreenY(),	\
		 1.0f, 10000.0f);
  gluLookAt(_option.getScreenX() / 2.0, 1000.0f, _option.getScreenY() / 2.0,
            _option.getScreenX() / 2.0, 0.0, _option.getScreenY() / 2.0,
            0.0f, 1.0f, 0.0f);
  glPushMatrix();
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  glDisable(GL_DEPTH_TEST);
  glOrtho(0.0f, _option.getScreenX(), _option.getScreenY(), 0.0, 0.0, 1.0);
  _score->bind();
  glBegin(GL_QUADS);
  glTexCoord2f(0.0, 0.0);
  glVertex3f(0.0, 0.0, 0.0);
  glTexCoord2f(1.0, 0.0);
  glVertex3f(1200.0, 0.0, 0.0);
  glTexCoord2f(1.0, 1.0);
  glVertex3f(1200.0, 1000.0, 0.0);
  glTexCoord2f(0.0, 1.0);
  glVertex3f(0.0, 1000.0, 0.0);
  glEnd();
  glPopMatrix(); 
  gdl::Text	t;
  std::stringstream ss;
  t.setSize(60);

  int i = 0;
  for (std::vector<Model::Bomberman *>::iterator it = _players.begin(); \
       it != _players.end() && i < 8; ++it, i++)
    {
      ss.str("");
      ss << (*it)->getPoints();
      t.setText(ss.str());
      t.setPosition(300, 175 + (i * 100));
      t.draw();
    }
}

void  Bomber::draw(void)
{
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
  glClearDepth(1.0f);

  if (_end == 1)
    drawScore();
  else
    {
      drawPlayer();
      _hud.draw();
      if (_end == 4)
	{
	  _menu.draw();
	  _hud.drawMenu();
	}
      else
	_hud.drawText(_player1, _player2);
    }
}

void  Bomber::unload(void)
{
  if (_loaded == false)
    return;
  _loaded = false;
  _pool->stop();
  _sound.stopMusic(GameSound::GAME);
  _sound.stopEffect(GameSound::EXPLOSION);
  _sound.stopEffect(GameSound::BONUS);
  usleep(150);
  if (_end == 3)
    return;
  for (std::list<IEvent *>::iterator it = _event.begin();	\
       it != _event.end(); ++it)
    {
      IEvent *tmp = *it;
      delete (tmp);
    }
  for (std::vector<Model::Bomberman *>::iterator it = _players.begin(); \
       it != _players.end(); ++it)
    {
      Model::Bomberman *tmp;

      tmp = *it;
      if (tmp != NULL)
        delete(tmp);
    }
  _event.clear();
  _obj.clear();
  _players.clear();
  _player1 = NULL;
  _player2 = NULL;
  if (_glob != NULL)
    {
      delete (_glob);
      _glob = NULL;
    }
  if (_map != NULL)
    {
      delete (_map);
      _map = NULL;
    }
}

bool	Bomber::isLoaded() const
{ return (_loaded); }

bool	Bomber::isEnded() const 
{ return ((_end == 2 || _end == 3) ? true : false); }

bool	compare(Model::Bomberman *b1, Model::Bomberman *b2)
{
  if (b1->getPoints() >= b2->getPoints())
    return (true);
  return (false);
}
