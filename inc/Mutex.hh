//
// Mutex.hh for  in /home/grizet_j//C++/plazza/inc
// 
// Made by jean grizet
// Login   <grizet_j@epitech.net>
// 
// Started on  Mon Apr 15 12:50:41 2013 jean grizet
// Last update Mon Apr 15 17:45:22 2013 jean grizet
//

#ifndef __MUTEX_HH__
#define __MUTEX_HH__

#include <cstdlib>
#include <iostream>
#include <pthread.h>

#include "IMutex.hh"

#define MUTEX_INIT_ERR		"Cannot create mutex"
#define MUTEX_DESTROY_ERR	"Cannot destroy mutex"

class	Mutex : public IMutex
{
private:
  pthread_mutex_t	_mutex;

  Mutex(const Mutex &);
  Mutex& operator=(const Mutex &);
public:
  Mutex();
  ~Mutex();
  void		lock(void);
  void		unlock(void);
  bool		trylock(void);
};

#endif
