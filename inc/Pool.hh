//
// Pool.hh for  in /home/grizet_j//C++/plazza/inc
// 
// Made by jean grizet
// Login   <grizet_j@epitech.net>
// 
// Started on  Mon Apr 15 19:11:37 2013 jean grizet
// Last update Thu May 23 20:26:32 2013 jean grizet
//

#ifndef __POOL_HH__
#define __POOL_HH__

#include <iostream>
#include <vector>

#include "IPool.hh"
#include "IThread.hh"
#include "IEventQueue.hh"

template <typename T, typename U>
class			Pool : public IPool<T, U>
{
private:
  IEventQueue<T>&		_task;
  IEventQueue<U>&		_complete;
  std::vector<IThread *>&	_thread;

  Pool(const Pool&);
  Pool& operator=(const Pool&);
public:
  Pool(IEventQueue<T>&, IEventQueue<U>&, std::vector<IThread *>&);
  ~Pool();
  void	pushTask(T*);
  bool	popCompleted(U**);
  void	run();
  void	stop();
};

#endif
