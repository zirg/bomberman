//
// EventQueue.hh for  in /home/grizet_j//C++/plazza/inc
// 
// Made by jean grizet
// Login   <grizet_j@epitech.net>
// 
// Started on  Mon Apr 15 15:13:32 2013 jean grizet
// Last update Mon May 13 20:00:37 2013 jean grizet
//

#ifndef __EVENT_QUEUE_HH__
#define __EVENT_QUEUE_HH__

#include <iostream>
#include <queue>

#include "IEventQueue.hh"
#include "IMutex.hh"
#include "ICondVar.hh"
#include "IEvent.hh"

template <typename T>
class		EventQueue : public IEventQueue<T>
{
  bool			_finished;
  IMutex&		_mutex;
  ICondVar&		_cond;
  std::queue<T*>	_queue;

  EventQueue(const EventQueue&);
  EventQueue& operator=(const EventQueue&);
public:
  EventQueue(IMutex&, ICondVar&);
  ~EventQueue(void);
  void	push(T*);
  bool	tryPop(T**);
  T*	pop(void);
  bool	isFinished(void) const;
  void	setFinished(void);
  unsigned int	getSize() const;
};

#endif
