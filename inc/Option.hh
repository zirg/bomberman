//
// Option.hh for  in /home/grizet_j//C++/bomberman/inc
// 
// Made by jean grizet
// Login   <grizet_j@epitech.net>
// 
// Started on  Fri May 31 15:48:44 2013 jean grizet
// Last update Thu Jun  6 07:02:31 2013 jean grizet
//

#ifndef __OPTION_HH__
#define __OPTION_HH__

#include	"Input.hpp"

#include	<iostream>
#include	<map>
#include	<time.h>

class	Option
{
  int		_screenX;
  int		_screenY;
  int		_sizeX;
  int		_sizeY;
  int		_player;
  int		_ia;
  int		_mode;
  time_t	_time;
  std::string	_typeMap;
  std::map<int, gdl::Keys::Key>&  _j1;
  std::map<int, gdl::Keys::Key>&  _j2;

public:
  Option();
  ~Option();
  void	setScreenX(int);
  void	setScreenY(int);
  void	setSizeX(int);
  void	setSizeY(int);
  void	setIA(int);
  void	setPlayer(int);
  void	setMode(int);
  void	setTime(int);
  void	setTypeMap(std::string&);

  int		getScreenX() const;
  int		getScreenY() const;
  int		getSizeX() const;
  int		getSizeY() const;
  int		getIA() const;
  int		getPlayer() const;
  int		getMode() const;
  time_t	getTime() const;
  std::map<int, gdl::Keys::Key>&  getKeyJ1();
  std::map<int, gdl::Keys::Key>&  getKeyJ2();
  std::string	getTypeMap() const;
};

#endif
