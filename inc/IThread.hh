//
// IThread.hh for  in /home/grizet_j//C++/plazza/inc
// 
// Made by jean grizet
// Login   <grizet_j@epitech.net>
// 
// Started on  Mon Apr 15 13:06:04 2013 jean grizet
// Last update Fri Jun  7 16:36:55 2013 jean grizet
//

#ifndef __I_THREAD_HH__
#define __I_THREAD_HH__

#include <iostream>
#include <pthread.h>

class	IThread
{
public:
  enum eStatus
    {
      INIT = 0,
      RUN,
      DEAD
    };
  virtual ~IThread(void) {}
  virtual eStatus	getStatus() const = 0;
  virtual void		run() = 0;
  virtual void		wait(void) = 0;
  virtual int		cancel(void) = 0;
  virtual void		kill(void) = 0;
  virtual void		*operator()(void *) = 0;
};

#endif
