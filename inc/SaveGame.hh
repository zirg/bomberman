//
// SaveGame.hh for bomberman in /home/grilla_b//Documents/epi/projet_tek2/boomberman/repo/bomberman
// 
// Made by brian grillard
// Login   <grilla_b@epitech.net>
// 
// Started on  Thu Jun  6 04:15:23 2013 brian grillard
// Last update Fri Jun  7 04:55:13 2013 brian grillard
//

#ifndef SAVEGAME_HH
#define SAVEGAME_HH

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include <fstream>
#include <iostream>
#include <vector>
#include "Model.hh"

typedef struct	s_save
{
  int		player;
  int		posX;
  int		posY;
  float		animSpeed;
  float		speed;
  float		pourcent;
  int		maxBomb;
  int		power;
  int		points;
}		t_save;

class		SaveGame
{

public:
  SaveGame();
  ~SaveGame();

  bool		open_file(std::ofstream & file, std::string const & path);
  bool		save(std::vector<Model::Bomberman *> player, std::string const & path, Map *map);
};


#endif
