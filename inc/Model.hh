//
// Model.hh for bomberman in /home/grilla_b//Documents/epi/projet_tek2/boomberman/repo/bomberman
// 
// Made by brian grillard
// Login   <grilla_b@epitech.net>
// 
// Started on  Mon May 27 11:22:59 2013 brian grillard
// Last update Sat Jun  8 17:52:51 2013 alexandre payen
//

#ifndef __MODEL_HH__
#define __MODEL_HH__

#include <map>
#include <vector>

#include "AObject.hh"
#include "Model.hpp"
#include "Input.hpp"
#include "Color.hpp"
#include "APlayer.hpp"
#include "BagOfBomb.hh"
#include "Bomb.hh"
#include "Event.hh"
#include "GameSound.hh"

namespace Model
{
  class Bomberman : public AObject, public APlayer
  { 
  private:
    GameSound&				_sound;
    float				_animSpeed;
    float				_speed;
    int					_death;
    bool				_end;
    double			        _timeInput;
    bool				_move;
    typedef void			(Bomberman::*keyPush)(Case *);
    std::map<eDirection, keyPush>       _push;
  protected:
    gdl::Model				*model_;
  private:
    BagOfBomb*				_bagBomb;
    int					_maxBomb;
    Event				*_updateEvent;
    float				_xD;
    float				_yD;
    int					_aD;    
    double				_pourcent;
    gdl::Color                          _color;
    bool				_load;

  public:
    Bomberman(int player, int posX, int posY,				\
	      gdl::Keys::Key up, gdl::Keys::Key down,			\
	      gdl::Keys::Key right, gdl::Keys::Key left,		\
	      gdl::Keys::Key shoot, Map *map, GameSound& gs, bool load = 0);
    Bomberman(int player, int posX, int posY, Map *map, GameSound& gs, bool load = 0);
    ~Bomberman(void);
    void				initialize();
    void				update(gdl::GameClock const &, gdl::Input &);
    void				draw(void);

    //DIRECTION
    void				keyRight(Case *cs);
    void				keyLeft(Case *cs);
    void				keyUp(Case *cs);
    void				keyDown(Case *cs);
    void				goMove(eDirection touch, gdl::GameClock const & gameClock);
    void				moveBomberman(gdl::GameClock const & gameClock, 
						      gdl::Input & input);
    //ANIMATION
    void				playRun();
    void				playStop();
    //TIME INPUT
    bool				checkTimeInput(gdl::GameClock const & gameClock, 
						       double timeMax);
    //GETTER
    gdl::Model*				getModel() const;
    BagOfBomb*				getBagOfBomb() const;
    //CHECK LIMIT
    bool				changePosInMap(int incrX, int incrY);
    bool				checkLimWest(int incr, Case *cs);
    bool				checkLimEst(int incr, Case *cs);
    bool				checkLimSouth(int incr, Case *cs);
    bool				checkLimNorth(int incr, Case *cs);
    bool				checkSizeMoveHori(Case *cs);
    bool				checkSizeMoveVerti(Case *cs);
    //BOMB
    bool				putBomb(double time, bool);
    void				inputBomb(gdl::GameClock const & gameClock, gdl::Input & input);
    AObject				*getReplace() const;
        //EVENT
    IEvent				*createEvent(gdl::GameClock const &, \
						     gdl::Input &);
    //DEATH
    int                                 getEnd() const;
    void				setEnd(int end);
    void				makeDeath();
    bool				Death();
    bool				isDead() const;
    //Bonus
    void				checkBonus(Case*);
    //POINTS
    int					getPoints() const;
    //SPEED
    int					getSpeed() const;
    int					getMaxBomb() const;
    int					getPower() const;
    void				setSpeed(float speed);
    void				setPower(int power);				
    void				setMaxBomb(int nbr);
    void				setAnimSpeed(float speed);
    float				getAnimSpeed() const;
    float				getPourcent() const;
    void				setPourcent(float pourcent);
    void				setPoints(int points);
    float				getRealSpeed() const;
  };
}

#endif
