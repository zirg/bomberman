//
// MapEditor.hh for bomberman in /home/grilla_b//Documents/epi/projet_tek2/boomberman/repo/bomberman/map_editor
// 
// Made by brian grillard
// Login   <grilla_b@epitech.net>
// 
// Started on  Sun Jun  2 14:25:23 2013 brian grillard
// Last update Sun Jun  2 19:05:22 2013 brian grillard
//

#ifndef MAPEDITOR_HH
#define MAPEDITOR_HH

#include <iostream>
#include <vector>
#include "Game.hpp"
#include "CaseEditor.hh"

class			MapEditor
{

private:
  int                                   _sizeX;
  int                                   _sizeY;
  float                                 _rsizeX;
  float                                 _rsizeZ;
  std::vector< std::vector<CaseEditor*> >     _map;

public:
  MapEditor();
  ~MapEditor();

  void					initializeMap(int x, int y);
  void					update(gdl::GameClock const &, gdl::Input &);
  void					draw();
  int                                   getsizeX() const;
  int                                   getsizeY() const;
  float                                 getrsizeX() const;
  float                                 getrsizeZ() const;
  std::vector< std::vector<CaseEditor*> >     getMap() const;


};

#endif
