//
// IEventQueue.hh for  in /home/grizet_j//C++/plazza/inc
// 
// Made by jean grizet
// Login   <grizet_j@epitech.net>
// 
// Started on  Mon Apr 15 15:11:49 2013 jean grizet
// Last update Sat Apr 20 22:44:00 2013 jean grizet
//

#ifndef __I_EVENT_QUEUE_HH__
#define __I_EVENT_QUEUE_HH__

template <typename T>
class	IEventQueue
{
public:
  virtual ~IEventQueue(void) {}
  virtual void	push(T*) = 0;
  virtual bool	tryPop(T**) = 0;
  virtual T*	pop(void) = 0;
  virtual bool	isFinished(void) const = 0;
  virtual void	setFinished(void) = 0;
  virtual unsigned int	getSize() const = 0;
};

#endif
