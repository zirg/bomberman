//
// CondVar.hh for  in /home/grizet_j//C++/plazza/inc
// 
// Made by jean grizet
// Login   <grizet_j@epitech.net>
// 
// Started on  Mon Apr 15 13:11:18 2013 jean grizet
// Last update Mon Apr 15 17:44:48 2013 jean grizet
//

#ifndef __COND_VAR_HH__
#define __COND_VAR_HH__

#include <iostream>
#include <pthread.h>

#include "Mutex.hh"
#include "ICondVar.hh"

#define CONDVAR_INIT_ERR	"Conditional var init error"
#define CONDVAR_DESTROY_ERR	"Conditional var destroy error"

class	CondVar : public ICondVar
{
private:
  pthread_mutex_t	_mutex;
  pthread_cond_t	_cond;

  CondVar(const CondVar&);
  CondVar& operator=(const CondVar&);
public:
  CondVar(void);
  ~CondVar(void);
  void	wait(void);
  void	signal(void);
  void	broadcast(void);
};

#endif
