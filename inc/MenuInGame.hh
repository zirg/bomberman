//
// MenuInGame.hh for MenuInGame in /home/lecoq_j//BombermanDepot/bomberman/inc
// 
// Made by jean-charles lecoq
// Login   <lecoq_j@epitech.net>
// 
// Started on  Fri Jun  7 07:33:46 2013 jean-charles lecoq
// Last update Fri Jun  7 23:47:41 2013 jean-charles lecoq
//

#ifndef		MENUINGAME_HH
# define	MENUINGAME_HH

#include        <utility>
#include        <string>
#include        <sstream>
#include        <iostream>
#include        <map>
#include        "Image.hpp"
#include        "Model.hpp"
#include        "Color.hpp"
#include        "Input.hpp"
#include        "GameClock.hpp"
#include        "Plan.hh"

class	MenuInGame
{
public:
  MenuInGame();
  ~MenuInGame();

  void initialize();
  void update(gdl::GameClock const &, gdl::Input &);
  void draw(void);

  int	getAction() const;

private:
  gdl::Image		*_resume;
  gdl::Image		*_save;
  gdl::Image		*_quit;

  float			_time;
  int			pos;
  
  bool			_bResume;
  bool			_bSave;
  bool			_bQuit;
};

#endif		/* ! MENUINGAME_HH */
