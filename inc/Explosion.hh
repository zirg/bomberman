//
// Explosion.hh for  in /home/grizet_j//C++/bomberman/inc
// 
// Made by jean grizet
// Login   <grizet_j@epitech.net>
// 
// Started on  Mon Apr 29 14:17:52 2013 jean grizet
// Last update Wed May 29 14:43:22 2013 jean grizet
//

#ifndef __EXPLOSION_HH__
#define __EXPLOSION_HH__

#include <GL/gl.h>
#include <GL/glu.h>

#include "AObject.hh"
#include "Image.hpp"
#include "Event.hh"

class	Explosion : public AObject
{
private:
  gdl::Image	*_texture;
  gdl::Image	*_smoke;
  bool		_explode;
  double	_max;
  bool		_end;

public:
  Explosion();
  Explosion(gdl::Image *);
  ~Explosion();
  void	initialize(void);
  void	update(gdl::GameClock const &, gdl::Input &);
  void	draw(void);
  void	drawSmoke(void);
  void	explode();
  void	setEnd(int end);
  int	getEnd() const;
  AObject *getReplace() const;
  IEvent	*createEvent(gdl::GameClock const &, gdl::Input &);
};

#endif
