//
// Load.hh for bomberman in /home/grilla_b//Documents/epi/projet_tek2/boomberman/repo/bomberman
// 
// Made by brian grillard
// Login   <grilla_b@epitech.net>
// 
// Started on  Thu Jun  6 05:20:35 2013 brian grillard
// Last update Fri Jun  7 14:50:54 2013 jean grizet
//

#ifndef LOADGAME_HH
#define LOADGAME_HH


#include <cstdlib>
#include <iostream>
#include <fstream>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string>
#include <sstream>
#include <vector>
#include "Option.hh"
#include "GameSound.hh"
#include "Map.hh"
#include "Global_IA.hh"
#include "IA.hh"

  typedef struct  s_sav
  {
    int			player;
    int			posX;
    int			posY;
    float		animSpeed;
    float		speed;
    float		pourcent;
    int			maxBomb;
    int			power;
    int			points;
  }			t_sav;



class		LoadGame
{

private:

  std::vector< std::vector<int> >	_info;
  t_sav					 tabsave[200];
public:
  LoadGame();
  ~LoadGame();

  void		 setState(std::vector<Model::Bomberman*>* players);
  bool           setInfo(Option& opt, Map *map, Global_IA* gl,
			 GameSound& gs, std::vector<Model::Bomberman*>* players, 
			 gdl::GameClock const & gameClock, int size, std::list<AObject *>* obj);

  bool          is_empty(std::ifstream& pFile);
  bool		load(Map *map, Option& opt, Global_IA** gl,
		     std::string const & path, std::vector<Model::Bomberman*>* players,
		     gdl::GameClock const & gameClock, GameSound& gameSound,
		     std::list<AObject *>* obj);
};

#endif
