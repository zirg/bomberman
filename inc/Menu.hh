//
// Bomber.hh for  in /home/grizet_j//C++/bomberman/inc
// 
// Made by jean grizet
// Login   <grizet_j@epitech.net>
// 
// Started on  Mon Apr 29 11:57:53 2013 jean grizet
// Last update Sat Jun  8 18:24:39 2013 jean-charles lecoq
//

#ifndef MENU__HH
#define MENU__HH

#include <GL/gl.h>
#include <GL/glu.h>
#include <list>

#include	<map>
#include	<utility>
#include	"Camera.hh"
#include	"MenuNewGame.hh"
#include	"MenuSettings.hh"
#include	"MenuLoadGame.hh"
#include	"MenuScores.hh"
#include	"MenuHelp.hh"
#include	"MenuCredit.hh"
#include	"MenuCreateMap.hh"
#include	"MenuQuit.hh"
#include	"GameSound.hh"
#include	"Option.hh"
#include	"Readdir.hh"
#include	"Texture.hh"

class Menu
{


public:

  Menu(gdl::GameClock&, gdl::Input&, GameSound&, Option&);
  void	initialize(void);
  void	update();
  void	draw(void);
  void	unload(void);

  /* fonction qui gere la camera en fonction du menu */
  void		goLeft();
  void		goRight();
  void		updateNewGame();
  void		updateLoadGame();
  void		updateMapGenerator();
  void		updateScore();
  void		updateOption();
  void		updateHelp();
  void		updateCredit();
  void		updateQuit();
  void		posNewGame();
  void		posLoadGame();
  void		posMapGenerator();
  void		posScore();
  void		posOption();
  void		posHelp();
  void		posCredit();
  void		posQuit();
  void		goNewGame();
  void		goLoadGame();
  void		goMapGenerator();
  void		goScore();
  void		goOption();
  void		goHelp();
  void		goCredit();
  void		goQuit();
  void		zoomNewGame(int);
  void		zoomLoadGame(int);
  void		zoomMapGenerator(int);
  void		zoomScore(int);
  void		zoomOption(int);
  void		zoomHelp(int);
  void		zoomCredit(int);
  void		zoomQuit(int);
  void		moveCamera();

  /* getting for main loop */
  bool	isPlaying() const;
  bool	isQuit() const;
  bool	isCreateMap() const;
  bool	isLoadGame() const;
  void	stopPlaying();
  int	getNbrMap() const;
  Texture*	getTexture();

  std::string             getSaveGame() const;
private:
  gdl::GameClock&	gameClock_;
  gdl::Input&		input_;
  GameSound&		_gameSound;  
  Option&		_option;

  Texture	_texture;

  Readdir	*_mapSave;
  Readdir	*_gameSave;
  std::vector<std::string>	_listMap;
  std::vector<std::string>	_listGame;

  std::vector<std::string>::iterator	_posListMap;
  std::vector<std::string>::iterator	_posListGame;

  typedef void	(Menu::*fctZoom)(int);
  typedef void	(Menu::*fctPos)();
  typedef void	(Menu::*fctGo)();
  typedef void	(Menu::*fctUpdateMenu)();

  std::map<int, fctZoom>			mapZoom_;
  std::map<int, fctPos>				mapPos_;
  std::map<int, fctGo>				mapGo_;
  std::map<int, fctUpdateMenu>			mapUpdateMenu_;

  Camera					*camera_;

  bool						_game;
  bool						_isQuit;
  bool						_isCreateMap;
  bool						_isLoadGame;


  /* Model menu */
  MenuNewGame		*newGame_;
  MenuSettings		*option_;
  MenuLoadGame		*loadGame_;
  MenuScores		*score_;
  MenuHelp		*help_;
  MenuCredit		*credit_;
  MenuCreateMap		*mapGenerator_;
  MenuQuit		*quit_;

  int			dir_;
  float			speedCamera_;
  bool			inZoom_;
  bool			typeZoom_;
  int		typeMenu_;
  bool		inMove_;
  bool		inMenu_;


  /* camera */
  float		xCamera_;
  float		yCamera_;
  float		zCamera_;
  float		xDestCamera_;
  float		yDestCamera_;
  float		zDestCamera_;
  float		xCenter_;
  float		yCenter_;
  float		zCenter_;
  float		xDestCenter_;
  float		yDestCenter_;
  float		zDestCenter_;
};

#endif
