//
// ShowNumber.hh for ShowNumber in /home/lecoq_j//Dropbox/ProjetTech2/Bomberman/inc
// 
// Made by jean-charles lecoq
// Login   <lecoq_j@epitech.net>
// 
// Started on  Sun May 26 15:40:51 2013 jean-charles lecoq
// Last update Sat Jun  8 12:26:02 2013 jean-charles lecoq
//

#ifndef		SHOWSTRING_HH
# define	SHOWSTRING_HH

#include        <utility>
#include        <string>
#include        <sstream>
#include        <iostream>
#include	<vector>
#include	<stdlib.h>
#include        "Plan.hh"
#include	"LinkTexture.hh"

class		ShowString
{
  std::vector<Plan*>	plan_;
  std::string		str_;
  int			nbMax_;

  int			i_;

  Texture		_texture;
  LinkTexture		_linkTexture;

public:
  ShowString(Texture&);
  ~ShowString();


  /* number decimal max|x first| y first| z | first | len*/
  void		initialize(int, float, float, float, float, float, float, int);
  void		update(std::string const &);
  void		draw();
};

#endif		/* !SHOWNUMBER_HH */
