//
// Case.hh for bomberman in /home/grilla_b//Documents/epi/projet_tek2/boomberman/real
// 
// Made by brian grillard
// Login   <grilla_b@epitech.net>
// 
// Started on  Thu May  9 16:51:22 2013 brian grillard
// Last update Wed May 29 13:29:07 2013 brian grillard
//

#ifndef CASE_HH
#define CASE_HH

#include "Vector3f.hpp"
#include "AObject.hh"
#include "Model.hpp"
#include "Mutex.hh"

namespace Model
{
  class Bomberman;
};

class			Case
{
private:
  bool			_death;
  Mutex			_mu;
  int			_x;
  int			_y;
  bool			_destroy;
  Vector3f		_pos;
  AObject*		_object;
  int			_walk;
  float			_lim[4];
  AObject*		_bomberman;
  int			_idBonus;
public:
  Case(int x = 0, int y = 0, int d = true, AObject *obj = NULL);
  ~Case();
  int			getX() const;
  int			getY() const;
  AObject*		getObject() const;
  void			setX(int);
  void			setY(int);
  void			setObject(AObject *);
  int			canWalk() const;
  void			setWalk(int);
  float			getNorthLim() const;
  float			getSouthLim() const;
  float			getEstLim() const;
  float			getWestLim() const;
  void			setBomberman(AObject *);
  AObject		*getBomberman() const;
  void			setDeath(bool death);
  bool			getDeath() const;
  void			setIdBonus(int bonus);
  int			getIdBonus() const;
};

#endif
