//
// SaveMap.hh for bomberman in /home/grilla_b//Documents/epi/projet_tek2/boomberman/repo/bomberman
// 
// Made by brian grillard
// Login   <grilla_b@epitech.net>
// 
// Started on  Wed Jun  5 21:20:43 2013 brian grillard
// Last update Wed Jun  5 23:11:18 2013 brian grillard
//

#ifndef SAVEMAP_HH
#define SAVEMAP_HH

#include <iostream>
#include <fstream>
#include "MapEditor.hh"
#include "Map.hh"

class		SaveMap
{

public:
  SaveMap();
  ~SaveMap();
  void		recordMap(std::string const & path, MapEditor* map);
  void		recordMap(std::string const & path, Map* map);
};

#endif
