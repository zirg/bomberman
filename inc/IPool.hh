//
// IPool.hh for  in /home/grizet_j//C++/plazza/inc
// 
// Made by jean grizet
// Login   <grizet_j@epitech.net>
// 
// Started on  Mon Apr 15 18:08:46 2013 jean grizet
// Last update Thu May 23 20:26:42 2013 jean grizet
//

#ifndef __I_POOL_HH__
#define __I_POOL_HH__

#include "IEventQueue.hh"
#include "IThread.hh"

template <typename T, typename U>
class	IPool
{
public:
  virtual ~IPool() {}
  virtual void	pushTask(T*) = 0;
  virtual bool	popCompleted(U**) = 0;
  virtual void	run() = 0;
  virtual void	stop() = 0;
};

#endif
