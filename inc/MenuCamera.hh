//
// Camera.hh for  in /home/grizet_j//C++/bomberman/inc
// 
// Made by jean grizet
// Login   <grizet_j@epitech.net>
// 
// Started on  Mon Apr 29 12:23:27 2013 jean grizet
// Last update Wed Jun  5 17:41:40 2013 jean-charles lecoq
//

#ifndef		MENUCAMERA_HH
# define	MENUCAMERA_HH

#include <GL/gl.h>
#include <GL/glu.h>

#include "GameClock.hpp"
#include "Input.hpp"
#include "Vector3f.hpp"

class MenuCamera
{
public:
  MenuCamera(void);
  void initialize(void);
  void update();
  void setCenter(float, float, float);
  void setPosition(float, float, float);

private:
  Vector3f position_;
  Vector3f rotation_;
  Vector3f center_;
};

#endif
