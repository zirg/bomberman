//
// ICondVar.hh for  in /home/grizet_j//C++/plazza/inc
// 
// Made by jean grizet
// Login   <grizet_j@epitech.net>
// 
// Started on  Mon Apr 15 13:09:45 2013 jean grizet
// Last update Mon Apr 15 13:10:40 2013 jean grizet
//

#ifndef __I_CONDVAR_HH__
#define __I_CONDVAR_HH__

class	ICondVar
{
public:
  virtual ~ICondVar(void) {}
  virtual void wait(void) = 0;
  virtual void signal(void) = 0;
  virtual void broadcast(void) = 0;
};

#endif
