//
// APlayer.hh for bomberman in /home/grilla_b//Documents/epi/projet_tek2/boomberman/real
// 
// Made by brian grillard
// Login   <grilla_b@epitech.net>
// 
// Started on  Mon May 13 18:00:27 2013 brian grillard
// Last update Fri Jun  7 18:38:46 2013 jean grizet
//

#ifndef APLAYER_HPP
#define APLAYER_HPP

#include <map>

#include "Input.hpp"
#include "Map.hh"
#include "Camera.hh"

class					APlayer
{

protected:
  Map					*_map;
  int					_movCase;
  int					_angle;
  int					_posX;
  int					_posY;
  int					_player;
  float					_sizeCase;
  float					_posMiddle;
  enum					eDirection
    {
					LEFT,
					RIGHT,
					UP,
					DOWN,
					NIL
    };
  eDirection				_direction;
  std::map<gdl::Keys::Key, eDirection>	_key;
  gdl::Keys::Key			_shoot;
public:

  APlayer() : _movCase(0), _angle(0), _posX(0),
	      _posY(0), _player(0), _sizeCase(350.0f),
	      _posMiddle(350.0f / 4), _direction(DOWN){}
  int					getPosX() const { return (_posX); };
  int					getPosY() const { return (_posY); };
  int					getPlayer() const { return (_player); };
  float					getSizeCase() const { return (_sizeCase); };
  float					getPosMiddle() const { return (_posMiddle); };
  eDirection				getDirection() const { return (_direction); };
  Map*					getMap() const  { return (_map); };
};

#endif
