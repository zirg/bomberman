//
// NewGame.hh for NewGame in /home/lecoq_j//Tech2/ProjetC++/Bomberman/inc
// 
// Made by jean-charles lecoq
// Login   <lecoq_j@epitech.net>
// 
// Started on  Wed May  8 22:59:23 2013 jean-charles lecoq
// Last update Sat Jun  8 10:38:59 2013 jean-charles lecoq
//

#ifndef		MENUCREATEMAP_HH
# define	MENUCREATEMAP_HH

#include	<utility>
#include	<string>
#include	<sstream>
#include	<iostream>
#include	<map>
#include	"Image.hpp"
#include        "Model.hpp"
#include        "Color.hpp"
#include	"Input.hpp"
#include	"GameClock.hpp"
#include	"ShowNumber.hh"
#include	"Option.hh"
#include	"Texture.hh"

class MenuCreateMap
{
public:
  MenuCreateMap(Option&, Texture&);
  ~MenuCreateMap(void);
  void initialize();
  void update(gdl::GameClock const &, gdl::Input &);
  void draw(void);
  void	upDown(int);


  void          zoomTitle(float, float, float,
                          float, float, float);
  void          drawTitle();
private:
  Texture		_texture;

  /* box for number */
  float	time_;
  bool	isHeight_;

  int	height_;
  int	width_;


  ShowNumber		*showHeight_;
  ShowNumber		*showWidth_;

  Option&		option_;

  float x1Title_;
  float x2Title_;
  float y1Title_;
  float y2Title_;
  float z1Title_;
  float z2Title_;

  float x1_;
  float x2_;
  float y1_;
  float y2_;
  float z1_;
  float z2_;

};

#endif
