//
// Viseur.hh for bomberman in /home/grilla_b//Documents/epi/projet_tek2/boomberman/real5
// 
// Made by brian grillard
// Login   <grilla_b@epitech.net>
// 
// Started on  Fri May 17 11:06:47 2013 brian grillard
// Last update Sun Jun  9 16:42:06 2013 brian grillard
//

#ifndef VISEUR_HH
#define VISEUR_HH

#include <GL/gl.h>
#include <GL/glu.h>

#include "AObject.hh"
#include "Image.hpp"
#include "GameClock.hpp"
#include "Input.hpp"
#include "MapEditor.hh"
#include "Box.hh"

class		Viseur : public AObject
{

  int					_posX;
  int					_posY;
  AObject*				_replace;
  MapEditor				*_map;
  float					_timeInput;
  int					_end;
  gdl::Image*				_texture;
  gdl::Image*                           _textbox;
  gdl::Image*                           _textblock;


public:
  Viseur(int x, int y, MapEditor *map);
  ~Viseur();
  void		initialize();
  void		update(gdl::GameClock const &, gdl::Input &);
  void		draw();
  bool		checkTimeInput(gdl::GameClock const &);
  void		changePosition(gdl::GameClock const & gameClock, gdl::Input & input, int y, int x);
  void		setObject(AObject *object, int type);
  void		deleteObject(gdl::GameClock const & gameClock);
  void		setEnd(int end);
  int		getEnd() const;
  AObject       *getReplace() const;
  IEvent        *createEvent(gdl::GameClock const &, gdl::Input &);
  int		getPosY() const;
  int		getPosX() const;
  void		inputSet(gdl::GameClock const & gameClock, gdl::Input & input);
};

#endif
