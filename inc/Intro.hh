//
// Bomber.hh for  in /home/grizet_j//C++/bomberman/inc
// 
// Made by jean grizet
// Login   <grizet_j@epitech.net>
// 
// Started on  Mon Apr 29 11:57:53 2013 jean grizet
// Last update Sat Jun  8 17:51:41 2013 jean grizet
//

#ifndef __INTRO__HH__
#define __INTRO__HH__

#include <iostream>
#include <GL/gl.h>
#include <GL/glu.h>
#include <list>
#include <sstream>

#include "Game.hpp"
#include "Option.hh"
#include "GameSound.hh"
#include "Model.hpp"
#include "Game.hpp"
#include "Camera.hh"
#include "AObject.hh"
#include "Box.hh"
#include "Map.hh"
#include "Image.hpp"
#include "Text.hpp"
#include "MapEditor.hh"
#include "SaveMap.hh"
#include "Vector3f.hpp"
#include "LetterIntro.hh"
#include "Texture.hh"

class	Intro : public gdl::Game
{
private:
  Option&				_opt;
  GameSound&				_sound;
  gdl::GameClock&			gameClock_;
  gdl::Input&				input_;
  Texture				*_texture;
  float					_timeInput;
  bool					_run;
  bool					_stape;
  gdl::Image				*_textbox;
  gdl::Image				*_smokbox;
  gdl::Model				_bomberman;
  std::vector<Box*>			_box;
  LetterIntro*				_enter;
  std::vector<LetterIntro*>		_letter;

  Camera				_cam;
  float					_updateTime;
  float					_drawTime;
  gdl::Image				*_back;
  bool					_end;
  Vector3f				_position;
  Vector3f				_rotation;
  int					_angle;
public:
  Intro(Option&, GameSound&, gdl::GameClock&, gdl::Input&, Texture *);
  ~Intro();
  void	initialize(void);
  void  update(gdl::GameClock const & gameClock, gdl::Input & input);
  void	draw(void);
  void	update(void);
  bool	isEnded() const;
  void	setName(const std::string&);
private:
  void	drawBack(void);
  void  unload();
  void	loopUpdate();
  void	drawBomber();
  void	addLetter(int, int, int);
  bool	checkTimeInput(gdl::GameClock const & gameClock, double);
  void	playWalk();
};


#endif
