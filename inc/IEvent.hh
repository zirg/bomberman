//
// IEvent.hh for  in /home/grizet_j//C++/bomberman/inc
// 
// Made by jean grizet
// Login   <grizet_j@epitech.net>
// 
// Started on  Mon May 13 19:24:45 2013 jean grizet
// Last update Fri Jun  7 16:29:44 2013 jean grizet
//

#ifndef __I_EVENT_HH__
#define __I_EVENT_HH__

#include <iostream>

class	IEvent
{
public:
  ~IEvent() {}
  virtual void		run() = 0;
  virtual bool		exit() const = 0;
};

#endif
