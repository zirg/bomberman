//
// Letter.hh for letter in /home/grilla_b//Documents/epi/projet_tek2/boomberman/repo/bomberman
// 
// Made by brian grillard
// Login   <grilla_b@epitech.net>
// 
// Started on  Sat Jun  8 00:12:06 2013 brian grillard
// Last update Sat Jun  8 17:48:27 2013 jean grizet
//

#ifndef _LETTERINTRO_H
#define _LETTERINTRO_H

#include <GL/gl.h>
#include <GL/glu.h>

#include "Image.hpp"
#include "GameClock.hpp"
#include "Vector3f.hpp"

class		LetterIntro
{

  Vector3f	_position;
  Vector3f	_rotation;
  gdl::Image*	_let;

public:
  LetterIntro(gdl::Image*, float posX, float posY, float posZ);
  ~LetterIntro();

  void		initialize();
  void		update(gdl::GameClock const & gameClock);
  void		draw();
  float		getPosX() const;
  void		addPosX(float val);
  void		drawEnter();
};

#endif
