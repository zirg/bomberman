//
// BagOfBomb.hh for bomba in /home/grilla_b//Documents/epi/projet_tek2/boomberman/real4/inc
// 
// Made by brian grillard
// Login   <grilla_b@epitech.net>
// 
// Started on  Wed May 22 13:25:04 2013 brian grillard
// Last update Thu Jun  6 09:38:50 2013 brian grillard
//

#ifndef BAGOFBOMB_HH
#define BAGOFBOMB_HH

#include <vector>
#include <algorithm>

#include "AObject.hh"
#include "Mutex.hh"
#include "GameSound.hh"

class Bomb;

class					BagOfBomb : public AObject
{
  GameSound&				_sound;
  int					_idBomber;
  bool					_end;
  std::vector<Bomb*>			_bag;
  std::vector<bool>			_idBomb;
  Mutex					_mu;
  int					_max;
  int					_nbon;
  int					_points;

public:
  BagOfBomb(int&, GameSound&);
  ~BagOfBomb();

  std::vector<Bomb*>			getBag() const;
  std::vector<bool>			getIdBomb() const;
  int					getPosFree();
  void					addBomb(int);
  int					getMaxBomb() const;
  int					getNbrBombOnMap();
  void					changeStatePosBomb(int pos);
  std::vector<Bomb*>			getBombOnMap();
  void					initialize();
  void					update(gdl::GameClock const &,	\
					       gdl::Input & input);
  void					draw();
  AObject				*getReplace() const;
  IEvent				*createEvent(gdl::GameClock const &, \
						     gdl::Input & input);
  void					addPower(int nbr);

  int					getEnd() const;
  void					setEnd(int end);

  void					increaseNbon(int nbr);
  void					decreaseNbon(int nbr);
  int					getNbon() const;
  int					getMax() const;
  int					getPoints() const;
  void					addPoints(int nbr);
  int					getIdBomber() const;
  int					getPower() const;
  void					setPower(int power);
  void					setPoints(int points);
};

#endif
