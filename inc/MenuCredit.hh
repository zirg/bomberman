//
// NewGame.hh for NewGame in /home/lecoq_j//Tech2/ProjetC++/Bomberman/inc
// 
// Made by jean-charles lecoq
// Login   <lecoq_j@epitech.net>
// 
// Started on  Wed May  8 22:59:23 2013 jean-charles lecoq
// Last update Sat Jun  8 10:30:12 2013 jean-charles lecoq
//

#ifndef		MENUCREDIT_HH
# define	MENUCREDIT_HH

#include	<utility>
#include	<string>
#include	<sstream>
#include	<iostream>
#include	<map>
#include	"Image.hpp"
#include        "Model.hpp"
#include        "Color.hpp"
#include	"Input.hpp"
#include	"GameClock.hpp"
#include	"Texture.hh"

class MenuCredit
{
public:
  MenuCredit(Texture&);
  ~MenuCredit(void);
  void initialize();
  void update(gdl::GameClock const &, gdl::Input &);
  void draw(void);

  void      drawTitle();

  void    zoomTitle(float, float,
                    float, float,
		    float, float);

private:
  Texture		_texture;

  float x1Title_;
  float x2Title_;
  float y1Title_;
  float y2Title_;
  float z1Title_;
  float z2Title_;

  float x1_;
  float x2_;
  float y1_;
  float y2_;
  float z1_;
  float z2_;

};

#endif
