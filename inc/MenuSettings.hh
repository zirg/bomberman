//
// NewGame.hh for NewGame in /home/lecoq_j//Tech2/ProjetC++/Bomberman/inc
// 
// Made by jean-charles lecoq
// Login   <lecoq_j@epitech.net>
// 
// Started on  Wed May  8 22:59:23 2013 jean-charles lecoq
// Last update Sat Jun  8 12:44:17 2013 jean-charles lecoq
//

#ifndef		MENUSETTINGS_HH
# define	MENUSETTINGS_HH

#include	<utility>
#include	<string>
#include	<sstream>
#include	<iostream>
#include	<map>
#include	"Image.hpp"
#include	"Input.hpp"
#include	"Model.hpp"
#include	"Color.hpp"
#include	"GameClock.hpp"
#include	"Plan.hh"
#include	"ShowNumber.hh"
#include	"ShowString.hh"
#include	"GameSound.hh"
#include	"Option.hh"
#include	"Texture.hh"
#include	"LinkTexture.hh"

class MenuSettings
{
public:
  MenuSettings(Option&, GameSound&, Texture&);
  ~MenuSettings(void);
  void initialize();
  void update(gdl::GameClock const &, gdl::Input &);
  void draw(void);

  void	checkMapping(gdl::Keys::Key);

  void	upDownJ1(int);
  void	upDownJ2(int);
  void	upDownSound(int);

  void	checkInput(gdl::Input&);

  void	drawKeys();
  
  bool	getAskKey() const;

  bool    checkInput(gdl::Keys::Key);
  void	  askKey(gdl::Input&);

  void	bindKeyP1();
  void	bindP1();
  void	bindP1U();
  void	bindP1D();
  void	bindP1R();
  void	bindP1L();
  void	bindP1B();

  void	bindKeyP2();
  void	bindP2();
  void	bindP2U();
  void	bindP2D();
  void	bindP2R();
  void	bindP2L();
  void	bindP2B();

  void	bindSound();
  void	bindMusicVol();
  void	bindMusicMute();
  void	bindEffectsVol();
  void	bindEffectsMute();

  void          zoomTitle(float, float, float,
                          float, float, float);
  void          drawTitle();

private:
  GameSound&		gameSound_;
  Option&		option_;
  Texture		_texture;
  
  LinkTexture		_linkTexture;

  typedef void		(MenuSettings::*bind)();

  std::map<int, bind>	setBindP1_;
  std::map<int, bind>	setBindP2_;
  std::map<int, bind>	setBindSound_;

  /* list ShowNumber */
  Plan			*up_;
  Plan			*down_;
  Plan			*left_;
  Plan			*right_;
  Plan			*bombe_;
  
  ShowNumber		*showMusicVolume_;
  ShowNumber		*showEffectsVolume_;

  Plan			*showMuteMusic_;
  Plan			*showMuteEffects_;
  
  /* variable game */
  
  /* variable code */
  bool		pSound_;
  bool		pOne_;
  bool		pTwo_;

  int		menuP1_;
  int		menuP2_;
  int		menuSound_;
  float		time_;

  int		volM_;
  int		volE_;
  int		muteM_;
  int		muteE_;

  std::map<int, gdl::Keys::Key>& mappingJ1_; 
  std::map<int, gdl::Keys::Key>& mappingJ2_; 

  bool		askKey_;

  float x1Title_;
  float x2Title_;
  float y1Title_;
  float y2Title_;
  float z1Title_;
  float z2Title_;

  float x1_;
  float x2_;
  float y1_;
  float y2_;
  float z1_;
  float z2_;
};

#endif
