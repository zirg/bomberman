//
// Box.hh for  in /home/grizet_j//C++/bomberman/inc
// 
// Made by jean grizet
// Login   <grizet_j@epitech.net>
// 
// Started on  Mon Apr 29 14:17:52 2013 jean grizet
// Last update Sat Jun  8 17:09:16 2013 jean grizet
//

#ifndef __BOX_HH__
#define __BOX_HH__

#include <GL/gl.h>
#include <GL/glu.h>

#include "AObject.hh"
#include "Image.hpp"
#include "Event.hh"

class	Box : public AObject
{
private:
  gdl::Image	*_texture;
  gdl::Image	*_smoke;
  bool		_explode;
  int		_max;
  bool		_end;
  AObject	*_replace;

public:
  Box();
  Box(gdl::Image *, gdl::Image *, AObject *);
  Box(gdl::Image *);
  ~Box();
  void	initialize(void);
  void	initBox(float, float, float);
  void	update(gdl::GameClock const &, gdl::Input &);
  void	draw(void);
  void	drawVertex(void);
  void	drawSmoke(void);
  void	explode();
  void	setEnd(int end);
  int	getEnd() const;
  float	getPosX() const;
  AObject	*getReplace() const;
  IEvent	*createEvent(gdl::GameClock const &, gdl::Input &);
};

#endif
