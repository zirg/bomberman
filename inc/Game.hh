//
// Game.hh for  in /home/grizet_j//C++/bomberman/inc
// 
// Made by jean grizet
// Login   <grizet_j@epitech.net>
// 
// Started on  Wed Jun  5 21:38:17 2013 jean grizet
// Last update Sat Jun  8 19:56:11 2013 jean grizet
//

#ifndef __GAME_HH__
#define __GAME_HH__

#include "Game.hpp"
#include "Menu.hh"
#include "Bomber.hh"
#include "LoopEdit.hh"
#include "Intro.hh"

class	Game : public gdl::Game
{
  GameSound     _sound;
  Option        _opt;
  Menu		_menu;
  Bomber	_game;
  LoopEdit	_creator;
  Intro		_intro;
  bool		_ingame;
private:
  void	manageCreator();
  void	manageGame();
public:
  Game();
  ~Game();
  void initialize();
  void update();
  void draw();
  void unload();
};

#endif
