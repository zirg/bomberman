//
// Global_IA.hh for bomberman in /home/payen_a//Project/C++/bomberman/inc
// 
// Made by alexandre payen
// Login   <payen_a@epitech.net>
// 
// Started on  Mon May 27 16:56:39 2013 alexandre payen
// Last update Sun Jun  9 17:52:04 2013 jean grizet
//

#ifndef __GLOBAL_IA_HH__
#define __GLOBAL_IA_HH__

#include <iostream>
#include <vector>
#include "Bomb.hh"
#include "Model.hh"
#include "Map.hh"
#include "Case.hh"

class	Global_IA
{
private:
  Map			*_map;
  std::vector< std::vector<Case*> >	_case;
  int			**_riskMap;
  int			_x;
  int			_y;
  int			_count;
  int			_sizeX;
  int			_sizeY;
  int			_nbBox;

public:
  Global_IA(Map *map);
  ~Global_IA();
  void			update();
  void			getRiskMap(int, int, int, int ***);
  void			clearRiskMap();
  void			setRisk(std::vector< std::pair<int, int> > unsafe);
  int			getNbBox() const;
};

#endif
