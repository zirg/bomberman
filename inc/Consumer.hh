//
// Consumer.hh for  in /home/grizet_j//C++/bomberman/inc
// 
// Made by jean grizet
// Login   <grizet_j@epitech.net>
// 
// Started on  Mon May 13 19:52:47 2013 jean grizet
// Last update Tue May 14 11:02:06 2013 jean grizet
//

#ifndef __CONSUMER__HH__
#define __CONSUMER__HH__

#include "IEvent.hh"
#include "IEventQueue.hh"

class	Consumer
{
private:
  IEventQueue<IEvent>&	_task;
  IEventQueue<IEvent>&	_done;
public:
  Consumer(IEventQueue<IEvent>&, IEventQueue<IEvent>&);
  ~Consumer();

  void*	run(void*);
};

#endif
