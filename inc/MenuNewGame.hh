//
// NewGame.hh for NewGame in /home/lecoq_j//Tech2/ProjetC++/Bomberman/inc
// 
// Made by jean-charles lecoq
// Login   <lecoq_j@epitech.net>
// 
// Started on  Wed May  8 22:59:23 2013 jean-charles lecoq
// Last update Sun Jun  9 17:25:34 2013 jean-charles lecoq
//

#ifndef		MENUNEWGAME_HH
# define	MENUNEWGAME_HH

//#include	<utility>
//#include	<string>
//#include	<sstream>
#include	<iostream>
#include	<map>
#include        "Image.hpp"
#include        "Model.hpp"
#include        "Color.hpp"
#include        "MenuCamera.hh"
#include	"Text.hpp"
#include	"Plan.hh"
#include	"ShowNumber.hh"
#include	"ShowString.hh"
#include	"Option.hh"
#include	"Texture.hh"

class MenuNewGame
{
public:
  MenuNewGame(Option&, Texture&, std::vector<std::string>&, std::vector<std::string>::iterator&);
  ~MenuNewGame(void);

  void initialize();
  void update(gdl::GameClock const &, gdl::Input &);
  void draw(void);  

  void	rightLeft(int, gdl::GameClock const &);
  void	upDown(int, gdl::GameClock const&);  

  void	typeGame(int);
  void	timeGame(int);
  void	players(int);
  void	nbIa(int);
  void	levelIa(int);
  void	typeMap(int);
  void	height(int);
  void	width(int);

  std::vector<std::string>::iterator& getPosListMap() const;
  std::vector<std::string>&           getListMap() const;

  void          zoomTitle(float, float, float,
                          float, float, float);
  void          drawTitle();
private:
  
  typedef void		(MenuNewGame::*fct)(int);

  Texture				_texture;

  /* dynamics data */
  Plan					*showGameMode_;
  ShowNumber				*showTimeGame_;
  Plan					*showPlayer_;
  ShowNumber				*showNbIa_;
  Plan					*showLevelIa_;
  ShowString				*showTypeMap_;
  ShowNumber				*showHeight_;
  ShowNumber				*showWidth_;
  
  /* variable game */
  bool					typeGame_;
  int					timeGame_;
  bool					onePlayer_;
  int					numberOfIa_;
  int					levelIa_;
  int					heightMap_;
  int					widthMap_;

  /* variable code */  
  int					posInMenu_;
  float					time_;
  Option&				_option;
  std::map<int, fct>			_fctNg;

  std::vector<std::string>&		_listMap;
  std::vector<std::string>::iterator&		_posListMap;
  float x1Title_;
  float x2Title_;
  float y1Title_;
  float y2Title_;
  float z1Title_;
  float z2Title_;

  float x1_;
  float x2_;
  float y1_;
  float y2_;
  float z1_;
  float z2_;  
};

#endif
