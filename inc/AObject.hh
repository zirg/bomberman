//
// AObject.hh for  in /home/grizet_j//C++/bomberman/inc
// 
// Made by jean grizet
// Login   <grizet_j@epitech.net>
// 
// Started on  Mon Apr 29 12:10:03 2013 jean grizet
// Last update Wed May 29 14:41:35 2013 jean grizet
//

#ifndef __A_OBJECT_HH__
#define __A_OBJECT_HH__

#include <GL/gl.h>
#include <GL/glu.h>

#include "Game.hpp"
#include "Vector3f.hpp"
#include "Mutex.hh"
#include "IEvent.hh"

class		AObject
{
public:
  AObject(void)
    : position_(0.0f, 0.0f, 0.0f), rotation_(0.0f, 0.0f, 0.0f)
  {
  }
  virtual void initialize(void) = 0;
  virtual void update(gdl::GameClock const &, gdl::Input &) = 0;
  virtual void draw(void) = 0;
  virtual IEvent *createEvent(gdl::GameClock const & gl, gdl::Input & i) = 0;
  virtual void	setEnd(int end) = 0;
  virtual int	getEnd() const = 0;
  virtual AObject *getReplace() const = 0;
  Vector3f position_;
  Vector3f rotation_;
  Mutex		_mu;
};

#endif
