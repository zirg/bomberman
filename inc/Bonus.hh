//
// Bonus.hh for  in /home/grizet_j//C++/bomberman/inc
// 
// Made by jean grizet
// Login   <grizet_j@epitech.net>
// 
// Started on  Mon Apr 29 14:17:52 2013 jean grizet
// Last update Wed May 29 15:09:12 2013 jean grizet
//

#ifndef __BONUS_HH__
#define __BONUS_HH__

#include <GL/gl.h>
#include <GL/glu.h>

#include "AObject.hh"
#include "Image.hpp"
#include "Event.hh"

class	Bonus : public AObject
{
private:
  gdl::Image	*_texture;
  int		_type;
  bool		_end;

public:
  Bonus();
  Bonus(gdl::Image *, int);
  ~Bonus();
  void	initialize(void);
  void	update(gdl::GameClock const &, gdl::Input &);
  void	draw(void);
  void	drawVertex(void);
  void	drawSmoke(void);
  void	explode();
  void	setEnd(int end);
  int	getEnd() const;
  int	getType() const;
  AObject *getReplace() const;
  IEvent	*createEvent(gdl::GameClock const &, gdl::Input &);
};

#endif
