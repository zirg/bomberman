//
// Event.hh for  in /home/grizet_j//C++/bomberman/inc
// 
// Made by jean grizet
// Login   <grizet_j@epitech.net>
// 
// Started on  Mon May 13 19:28:36 2013 jean grizet
// Last update Fri Jun  7 16:31:28 2013 jean grizet
//


#ifndef __EVENT_HH__
#define __EVENT_HH__

#include "Game.hpp"
#include "IEvent.hh"

#include "AObject.hh"

class	Event : public IEvent
{
  AObject		*_obj;
  const gdl::GameClock&	_clock;
  gdl::Input&		_input;
  bool			_exit;
public:
  Event(AObject *, gdl::GameClock const &, gdl::Input&, bool ex = false);
  ~Event();
  void		run();
  bool		exit() const;
};

#endif
