//
// Vector3f.hpp for  in /home/grizet_j//C++/bomberman/inc
// 
// Made by jean grizet
// Login   <grizet_j@epitech.net>
// 
// Started on  Mon Apr 29 12:21:22 2013 jean grizet
// Last update Mon Apr 29 12:22:25 2013 jean grizet
//

#ifndef __VECTOR_3F_HPP__
#define __VECTOR_3F_HPP__

struct Vector3f
{
  float x;
  float y;
  float z;
  
  Vector3f(void) :
    x(0.0f), y(0.0f), z(0.0f)
  { }

  Vector3f(float x, float y, float z) :
    x(x), y(y), z(z)
  { }
};

#endif
