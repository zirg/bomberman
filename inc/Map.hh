//
// Map.hh for map in /home/grilla_b//Documents/epi/projet_tek2/boomberman/real
// 
// Made by brian grillard
// Login   <grilla_b@epitech.net>
// 
// Started on  Thu May  9 15:27:15 2013 brian grillard
// Last update Fri Jun  7 14:56:19 2013 jean grizet
//

#ifndef MAP_HH
#define MAP_HH

#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <vector>
#include "Case.hh"
#include "Box.hh"
#include "Bonus.hh"
#include "Explosion.hh"
#include "Image.hpp"
#include "IPool.hh"
#include "IEvent.hh"
#include "Event.hh"

class					Map
{
private:
  int					_sizeX;
  int					_sizeY;
  float					_rsizeX;
  float					_rsizeZ;
  std::vector< std::vector<Case*> >	_map;
  gdl::Image*				_textbox;
  gdl::Image*				_textblock;
  gdl::Image*				_smoke;
  gdl::Image*				_ruby;
  gdl::Image*				_saph;
  gdl::Image*				_emerald;
  gdl::Image*				_ground;
  std::list<Case *>			_cases;
  std::list<Box *>			_boxes;
  Box					*_wall;
public:
  Map();
  ~Map();
  void	update(gdl::GameClock const &, gdl::Input &);
  void	draw(int, int , int);
  int					getsizeX() const;
  int					getsizeY() const;
  float					getrsizeX() const;
  float					getrsizeZ() const;
  std::vector< std::vector<Case*> >	getMap() const;
  void					initializeMap(int, int);
  bool					initializeMap(std::string const & path);
  void					drawGround(Case *);
  void					drawWall(int, int);
  Bonus					*generateBonus();
  bool					is_empty(std::ifstream& pFile);
  void                                addBlock(int ref, int & tx, int & ty);
  void                                addBlockAndBonus(int ref, int & tx, int & ty);
  bool                                getSizeMapLoad(std::ifstream& file, std::string & value);
  bool                                checkSizeMapLoad() const;

};

#endif
