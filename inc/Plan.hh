//
// Box.hh for  in /home/grizet_j//C++/bomberman/inc
// 
// Made by jean grizet
// Login   <grizet_j@epitech.net>
// 
// Started on  Mon Apr 29 14:17:52 2013 jean grizet
// Last update Sat Jun  8 10:18:30 2013 jean-charles lecoq
//

#ifndef __PLAN_HH__
#define __PLAN_HH__

#include <GL/gl.h>
#include <GL/glu.h>

#include "Vector3f.hpp"
#include "Texture.hh"

class	Plan
{
private:
  
  Texture		_texture;
  Texture::idTexture	_id;

  float		x1_;
  float		x2_;
  float		y1_;
  float		y2_;
  float		z1_;
  float		z2_;

public:
  Plan(Texture&);
  ~Plan();
  void	update(Texture::idTexture);
  void initialize(float, float, float, float, float, float);
  void draw(void);
};

#endif
