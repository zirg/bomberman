//
// Texture.hh for Texture in /home/lecoq_j//BombermanDepot/bomberman/inc
// 
// Made by jean-charles lecoq
// Login   <lecoq_j@epitech.net>
// 
// Started on  Fri Jun  7 20:31:51 2013 jean-charles lecoq
// Last update Sat Jun  8 17:49:05 2013 jean grizet
//

#ifndef		TEXTURE_H
# define	TEXTURE_H

#include	<map>
#include	<iostream>

#include	"Image.hpp"

class	Texture
{
public:
  enum	idTexture
    {
      TITLENG = 0,
      TITLELG,
      TITLECM,
      TITLESC,
      TITLESET,
      TITLEHELP,
      TITLECRE,
      TITLEQUIT, /*end texture title 0-7*/
      NGGM,
      NGGT,
      NGPL,
      NGNBIA,
      NGLVLIA,
      NGTM,
      NGHE,
      NGWI, /*end texture background new game 8-15*/
      LGYES,
      LGNO,/* background yes no load game 16-17*/
      CPHEIGHT,
      CPWIDTH,/*background createmap height & width 18-19*/
      SETTINGSON,
      SETTINGSOFF,
      SETTINGSKEYP1,
      SETTINGSKEYP2,
      SETTINGSP1,
      SETTINGSP2,
      SETTINGSP1U,
      SETTINGSP1D,
      SETTINGSP1R,
      SETTINGSP1L,
      SETTINGSP1B,
      SETTINGSP2U,
      SETTINGSP2D,
      SETTINGSP2R,
      SETTINGSP2L,
      SETTINGSP2B, /* SETTINGS KEYS 19 - 35*/
      SETTINGSSOUND,
      SETTINGSMUSICVOL,
      SETTINGSMUSICMUTE,
      SETTINGSEFFECTVOL,
      SETTINGSEFFECTMUTE,
      SETTINGSPRESSED, /*SETTING SOUND 19-41*/
      ON,
      OFF, /* ON OFF commun*/
      QUITYES,
      QUITNO, /*quit yes no 44-45*/
      BGPRINCIPAL,
      CREDIT,
      SURVIVAL,
      TIMEATTACK,
      ONEPLAYER,
      TWOPLAYERS,
      IANORMAL,
    };
  
public:
  Texture();
  ~Texture();

  void		bindTexture(idTexture);

private:
  std::map<idTexture, gdl::Image*>		_bindTexture;
public:
  gdl::Image*			getTexture(int);
};

#endif
