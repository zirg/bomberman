//
// Thread.hh for  in /home/grizet_j//C++/plazza/inc
// 
// Made by jean grizet
// Login   <grizet_j@epitech.net>
// 
// Started on  Mon Apr 15 13:30:12 2013 jean grizet
// Last update Fri Jun  7 16:36:06 2013 jean grizet
//

#ifndef __THREAD_HH__
#define __THREAD_HH__

#include <iostream>
#include <cstdlib>
#include <pthread.h>
#include <functional>

#include "IThread.hh"

#define PTHREAD_CREATE_ERR	"Cannot create thread"
#define PTHREAD_JOIN_ERR	"Cannot join thread"
#define PTHREAD_CANCEL_ERR	"Cannot cancel thread"

template <typename T>
class			Thread : public IThread
{
  T*			_obj;
  void			*(T::*_meth)(void *);
  pthread_t		_thread;
  IThread::eStatus	_status;

  Thread(const Thread&);
  Thread& operator=(const Thread&);
public:
  Thread(T* consumer, void *(T::*meth)(void *));
  ~Thread(void);
  IThread::eStatus	getStatus() const;
  void		        run();
  void			wait(void);
  int			cancel(void);
  void			kill(void);
  void			*operator()(void *);
};

#endif
