//
// IMutex.hh for  in /home/grizet_j//C++/plazza/inc
// 
// Made by jean grizet
// Login   <grizet_j@epitech.net>
// 
// Started on  Mon Apr 15 12:49:19 2013 jean grizet
// Last update Mon Apr 15 12:50:27 2013 jean grizet
//

#ifndef __I_MUTEX_HH__
#define __I_MUTEX_HH__

class IMutex 
{
public:
  virtual ~IMutex(void) {}
  virtual void lock(void) = 0;
  virtual void unlock(void) = 0;
  virtual bool trylock(void) = 0;
};

#endif
