//
// CaseEditor.hh for bnomberman in /home/grilla_b//Documents/epi/projet_tek2/boomberman/repo/bomberman
// 
// Made by brian grillard
// Login   <grilla_b@epitech.net>
// 
// Started on  Sun Jun  2 14:32:18 2013 brian grillard
// Last update Sun Jun  2 19:22:58 2013 brian grillard
//


#ifndef CASEEDITOR_HH
#define CASEEDITOR_HH

#include "Vector3f.hpp"
#include "AObject.hh"

class                   CaseEditor
{
  int                   _x;
  int                   _y;
  Vector3f              _pos;
  AObject*              _object;
  AObject*              _viseur;
  int                   _type;

public:
  CaseEditor(int x = 0, int y = 0, int type = 0, AObject *obj = NULL);
  ~CaseEditor();
  int                   getX() const;
  int                   getY() const;
  AObject*              getObject() const;
  AObject*              getViseur() const;
  void                  setViseur(AObject *);
  void                  setX(int);
  void                  setY(int);
  void                  setObject(AObject *);
  void                  setType(int type);
  int                   getType() const;
};

#endif
