//
// Readdir.hh for Readdir in /home/lecoq_j//BombermanDepot/bomberman/inc
// 
// Made by jean-charles lecoq
// Login   <lecoq_j@epitech.net>
// 
// Started on  Fri Jun  7 18:28:36 2013 jean-charles lecoq
// Last update Sat Jun  8 13:03:15 2013 jean-charles lecoq
//

#ifndef		READDIR_H
# define	READDIR_H

#include	<dirent.h>
#include	<sys/types.h>
#include	<string>
#include	<vector>

class		Readdir
{

public:
  bool				checkDouble(std::string& name);
  void				recupData();

public:
  Readdir(std::string, std::vector<std::string>&);
  ~Readdir();

private:
  std::string			_path;
  std::vector<std::string>&	_data;
};

#endif
