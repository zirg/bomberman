//
// Bomb.hh for bomb in /home/grilla_b//Documents/epi/projet_tek2/boomberman/real4
// 
// Made by brian grillard
// Login   <grilla_b@epitech.net>
// 
// Started on  Tue May 21 10:35:03 2013 brian grillard
// Last update Fri Jun  7 19:35:21 2013 jean grizet
//

#ifndef BOMB_HH
#define BOMB_HH

#include <map>
#include "Map.hh"
#include "Mutex.hh"
#include "Case.hh"
#include "AObject.hh"
#include "Model.hpp"
#include "Input.hpp"
#include "Game.hpp"
#include "GameSound.hh"
#include "GameClock.hpp"

class BagOfBomb;

class				Bomb : public AObject
{

private:
  GameSound&			_sound;
  gdl::Image			*_texture;
  Map*				_map;
  int				_y;
  int				_x;
  double			_time;
  BagOfBomb*			_bagBomb;
  int				_pos;
  int				_power;
  gdl::Model			*_model;
  std::vector<std::pair<int, int> >	_unsafe;
  std::vector<Explosion*> _exp;
  int				_explode;
  int				_end;
  Mutex				_mu;

public:

  Bomb(GameSound&);
  ~Bomb();

  void				initialize(void);
  void				initBomb(Map *map, int y, int x, double time, BagOfBomb* bagBomb, int posBomb);
  void				update(gdl::GameClock const &, gdl::Input &);
  void				draw(void);
  bool				checkTimeInput(gdl::GameClock const & gameClock, double t);
  std::vector< std::pair<int, int> >		getUnsafe();
  void				setUnsafe(int y, int x, bool);
  void				setObject();

  int                           getEnd() const;
  void				setEnd(int end);
  void				setTime(double time);
  double			getTime();
  void				updateExplosion(gdl::GameClock const & gameClock, gdl::Input & input);
  void				endExplosion();
  void				addExplosion(int nbr);
  void				setExplosion(std::vector<Explosion *>::iterator ex, Case *cs);
  void				Explose();
  void				updateObj(Case *cs, AObject *obj, gdl::GameClock const & gameClock, gdl::Input & input);
  void				loopUpdate(gdl::GameClock const & gameClock, gdl::Input & input, std::vector<std::pair<int, int> >::iterator it);
  int				getX() const;
  int				getY() const;

  //UNSAFE
  void				unsafeX(int y, int x, bool);
  void				unsafeY(int y, int x, bool);
  void				setPosition(int y, int x, bool *lim, bool);

  //END
  bool				setDefaultCaseBomb();
  void				setDefaultCaseExplosion(int y, int x);

  //POWER
  void				addPower(int);
  AObject			*getReplace() const;
  IEvent        *createEvent(gdl::GameClock const &, gdl::Input &);

  void				addPoints(Case *, int);
  int				getPower() const;
  void				setPower(int power);
};

#endif
