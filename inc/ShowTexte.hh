//
// ShowTexte.hh for ShowTexte in /home/lecoq_j//BombermanDepot/bomberman/inc
// 
// Made by jean-charles lecoq
// Login   <lecoq_j@epitech.net>
// 
// Started on  Fri Jun  7 23:59:55 2013 jean-charles lecoq
// Last update Sat Jun  8 00:36:13 2013 jean-charles lecoq
//

#ifndef		SHOWTEXTE_H
# define	SHOWTEXTE_H

#include	"Input.hpp"
#include	"Texture.hh"

class	ShowTexte
{
private:
  std::map<char, Texture::idTexture>		_char;
  std::map<int, Texture::idTexture>		_number;
  std::map<gdl::Keys::Key, Texture::idTexture>	_key;

public:
  ShowTexte(Texture*, int);
  ~ShowTexte();

};

#endif
