//
// Camera.hh for  in /home/grizet_j//C++/bomberman/inc
// 
// Made by jean grizet
// Login   <grizet_j@epitech.net>
// 
// Started on  Mon Apr 29 12:23:27 2013 jean grizet
// Last update Fri May 31 16:54:35 2013 jean grizet
//

#ifndef __CAMERA_HH__
#define __CAMERA_HH__

#include <GL/gl.h>
#include <GL/glu.h>

#include "Option.hh"
#include "GameClock.hpp"
#include "Input.hpp"
#include "Vector3f.hpp"

class Camera
{
  int		_x;
  int		_y;
  double	_xd;
  double	_yd;
  int		_player;
public:
  Camera(void);
  void  initialize(Option&);
  void  update(gdl::GameClock const &, gdl::Input &, int);
  void	setPosition(int, int, int);
  void	setLook(int, int, int);
  void	setMode();
private:
  Vector3f position_;
  Vector3f rotation_;
  Vector3f look_;
};

#endif
