//
// Sound.hh for  in /home/grizet_j//C++/bomberman/inc
// 
// Made by jean grizet
// Login   <grizet_j@epitech.net>
// 
// Started on  Sat Jun  1 14:51:46 2013 jean grizet
// Last update Sat Jun  8 17:04:37 2013 jean grizet
//

#ifndef __GAME_SOUND_HH__
#define __GAME_SOUND_HH__

#include <SFML/Audio.hpp>
#include <vector>
#include <iostream>

class		GameSound
{
public:
  enum eEffectID
    {
      EXPLOSION = 0,
      BONUS,
      WALK1,
      WALK2,
      GONG
    };
  enum eMusicID
    {
      GAME = 0,
      MENU
    };
  class		GameMusic : public sf::Music
  {
    const std::string&	_file;
    bool		_run;
    int			_volume;
  public:
    GameMusic(const std::string &);
    ~GameMusic();
    void	play();
    void	stop();
    void	setVolume(int);
  };
private:
  int				_vol_mu;
  int				_vol_eff;
  sf::Listener			_listener;
  std::vector<sf::Sound *>	_effect;
  std::vector<GameMusic *>	_music;
  bool				_run;

public:
  GameSound();
  ~GameSound();
  GameSound(const GameSound&);
  GameSound&	operator=(const GameSound&);

  void		init(void);
  void		playEffect(eEffectID);
  void		stopEffect(eEffectID);
  void		playMusic(eMusicID);
  void		stopMusic(eMusicID);
  void		setVolumeMusic(int);
  void		setVolumeEffect(int);
  int		getVolumeMusic(void) const;
  int		getVolumeEffect(void) const;
};

#endif
