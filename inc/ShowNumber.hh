//
// ShowNumber.hh for ShowNumber in /home/lecoq_j//Dropbox/ProjetTech2/Bomberman/inc
// 
// Made by jean-charles lecoq
// Login   <lecoq_j@epitech.net>
// 
// Started on  Sun May 26 15:40:51 2013 jean-charles lecoq
// Last update Sat Jun  8 12:07:41 2013 jean-charles lecoq
//

#ifndef		SHOWNUMBER_HH
# define	SHOWNUMBER_HH

#include        <utility>
#include        <string>
#include        <sstream>
#include        <iostream>
#include	<vector>
#include	<stdlib.h>
#include        "Plan.hh"
#include	"Texture.hh"
#include	"LinkTexture.hh"


class		ShowNumber
{
  std::vector<Plan*>	plan_;
  int			number_;
  int			nbMax_;

  int			i_;
  LinkTexture		_linkTexture;
  Texture		_texture;
public:
  ShowNumber(Texture&);
  ~ShowNumber();

  /* number decimal max|x first| y first| z | first | len*/
  void		initialize(int, float, float, float, float, float, float, int);
  void		update(int);
  void		draw();
};

#endif		/* !SHOWNUMBER_HH */
