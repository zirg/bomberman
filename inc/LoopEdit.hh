//
// Bomber.hh for  in /home/grizet_j//C++/bomberman/inc
// 
// Made by jean grizet
// Login   <grizet_j@epitech.net>
// 
// Started on  Mon Apr 29 11:57:53 2013 jean grizet
// Last update Fri Jun  7 04:49:49 2013 jean grizet
//

#ifndef __LOOP_EDIT__HH__
#define __LOOP_EDIT__HH__

#include <iostream>
#include <GL/gl.h>
#include <GL/glu.h>
#include <list>
#include <sstream>

#include "Option.hh"
#include "GameSound.hh"
#include "Game.hpp"
#include "Camera.hh"
#include "AObject.hh"
#include "Box.hh"
#include "Map.hh"
#include "Image.hpp"
#include "Text.hpp"
#include "MapEditor.hh"
#include "Viseur.hh"
#include "SaveMap.hh"

class	LoopEdit
{
private:
  Option&				_option;
  std::string				_path;
  gdl::GameClock&			gameClock_;
  gdl::Input&				input_;
  MapEditor				*_map;
  Camera				_cam;
  float					_updateTime;
  float					_drawTime;
  gdl::Image				*_back;
  Viseur*				_viseur;
  bool					_loaded;
  bool					_end;
public:
  LoopEdit(Option&, std::string const & path,		\
	   gdl::GameClock& gc, gdl::Input &in);
  ~LoopEdit();
  void	initialize(void);
  void	draw(void);
  void	unload(void);
  void	update(void);
  bool	isLoaded() const;
  bool	isEnded() const;
  void	setName(const std::string&);
private:
  void	drawBack(void);
};


#endif
