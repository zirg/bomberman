//
// IA.hh for  in /home/payen_a//Project/C++/bomberman/src
// 
// Made by alexandre payen
// Login   <payen_a@epitech.net>
// 
// Started on  Wed May 22 10:28:58 2013 alexandre payen
// Last update Sun Jun  9 17:26:18 2013 alexandre payen
//

#ifndef __IA_HH__
#define __IA_HH__

#include "Map.hh"
#include "Bomb.hh"
#include "Model.hh"
#include "GameSound.hh"
#include "Global_IA.hh"
#include "BagOfBomb.hh"
#include <lua.hpp>
#include <vector>
#include <cstring>
#include <utility>
#include <stack>

class	IA : public Model::Bomberman
{
public:
  typedef void (IA::*callFun)(lua_State*);

private:
  Map					*_map;
  Global_IA				*_glob;
  std::vector<Model::Bomberman*>	*_play;
  gdl::GameClock const			*_gameClock;
  lua_State				*_l;
  int					_lua_status;
  int					_count;
  APlayer::eDirection			_dir;
  std::map<std::string, callFun>	_tab;
  int					**_tabMap;
  int					_sizeTab;
  std::vector<int>			*_alea;
  std::stack<int>			*_way;
  std::stack<int>			*_best;
  int					_escape;
  int					_escapeB;
  int					_j;

public:
  IA(int, int, int,
     Map *,
     Global_IA *Glob,
     std::vector<Model::Bomberman *> *,
     GameSound&,
     gdl::GameClock const *);
  
  ~IA();
  void					update(gdl::GameClock const &, gdl::Input &);
  void					assignThis(lua_State *state, const char *func);

private:
  int					haveBetterStrat(int);
  int					canMoove();
  void					allocTab(int x);
  void					updateMap(lua_State *state);
  int					recurs(int max, int acc, int x, int y, int );
  void					calcMap(lua_State *state);
  void					updateUnsafe(lua_State *state);
  void					becomeRand(int x, int y);
  void					initTab();
  int					verifRecurs(int u, int flag);
  int					pushWay(int x, int y, int xTmp, int yTmp);
  int					verifLoop(int flag, int, int, int, int);
  void					movePlease();
  void					setAlea(int, int);

};

#endif
