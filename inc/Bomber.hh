//
// Bomber.hh for  in /home/grizet_j//C++/bomberman/inc
// 
// Made by jean grizet
// Login   <grizet_j@epitech.net>
// 
// Started on  Mon Apr 29 11:57:53 2013 jean grizet
// Last update Fri Jun  7 09:46:51 2013 jean grizet
//

#ifndef __BOMBER__HH__
#define __BOMBER__HH__

#include <iostream>
#include <GL/gl.h>
#include <GL/glu.h>
#include <list>
#include <sstream>

#include "Option.hh"
#include "GameSound.hh"
#include "Game.hpp"
#include "Camera.hh"
#include "AObject.hh"
#include "Model.hh"
#include "Box.hh"
#include "IPool.hh"
#include "Pool.hh"
#include "IEventQueue.hh"
#include "EventQueue.hh"
#include "CondVar.hh"
#include "Mutex.hh"
#include "IThread.hh"
#include "Thread.hh"
#include "IEvent.hh"
#include "Event.hh"
#include "Consumer.hh"
#include "Map.hh"
#include "Global_IA.hh"
#include "IA.hh"
#include "Image.hpp"
#include "LoadGame.hh"
#include "SaveGame.hh"
#include "MenuInGame.hh"
#include "UserInterface.hh"

class	Bomber
{
private:
  Option&				_option;
  GameSound&				_sound;
  std::vector<IThread *>		_thread;
  EventQueue<IEvent>			_in;
  EventQueue<IEvent>			_out;
  IPool<IEvent, IEvent>			*_pool;
  gdl::GameClock&			gameClock_;
  gdl::Input&				input_;
  UserInterface				_hud;
  Map					*_map;
  Camera				_cam;
  Global_IA				*_glob;
  Model::Bomberman			*_player1;
  Model::Bomberman			*_player2;
  std::list<AObject *>			_obj;
  std::list<IEvent *>			_event;
  float					_updateTime;
  std::vector<Model::Bomberman *>	_players;
  gdl::Image				*_back;
  gdl::Image				*_score;
  bool					_loaded;
  int					_end;
  time_t				_timer;
  MenuInGame				_menu;
  SaveGame				_save;

public:
  Bomber(Option&, GameSound&, gdl::GameClock&, gdl::Input&);
  ~Bomber();
  void	initialize(void);
  void	initialize(const std::string&);
  void	draw(void);
  void	unload(void);
  void	update(void);
  bool	isLoaded() const;
  bool	isEnded() const;
private:
  void	drawPlayer(void);
  void	drawBack(void);
  void	drawMiddle(void);
  void	drawScore(void);
  void	initPool(void);
  void	initPlayer(void);
  void	initIA(void);
  void	initEvent(void);
  void	manageEndGame(void);
  void	manageMenu(void);
};

bool    compare(Model::Bomberman *, Model::Bomberman *);

#endif
