//
// LinkTexture.hh for LinkTexture in /home/lecoq_j//BombermanDepot/bomberman/inc
// 
// Made by jean-charles lecoq
// Login   <lecoq_j@epitech.net>
// 
// Started on  Sat Jun  8 11:37:47 2013 jean-charles lecoq
// Last update Sat Jun  8 11:51:11 2013 jean-charles lecoq
//

#ifndef		LINKTEXTURE_H
# define	LINKTEXTURE_H

#include	<map>

#include	"Input.hpp"
#include	"Texture.hh"

class	LinkTexture
{
  std::map<char, Texture::idTexture>	_char;
  std::map<int, Texture::idTexture>	_number;
  std::map<gdl::Keys::Key, Texture::idTexture>	_keys;
public:
  LinkTexture();
  ~LinkTexture();

  Texture::idTexture      getChar(char);
  Texture::idTexture      getNumber(int);
  Texture::idTexture      getKeys(gdl::Keys::Key);
};

#endif
