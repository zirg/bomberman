//
// NewGame.hh for NewGame in /home/lecoq_j//Tech2/ProjetC++/Bomberman/inc
// 
// Made by jean-charles lecoq
// Login   <lecoq_j@epitech.net>
// 
// Started on  Wed May  8 22:59:23 2013 jean-charles lecoq
// Last update Sun Jun  9 11:40:48 2013 brian grillard
//

#ifndef		MENULOADGAME_HH
# define	MENULOADGAME_HH

#include	<utility>
#include	<string>
#include	<sstream>
#include	<iostream>
#include	<map>
#include	"Image.hpp"
#include        "Model.hpp"
#include        "Color.hpp"
#include	"Input.hpp"
#include	"GameClock.hpp"
#include	<vector>
#include	"Plan.hh"
#include	"ShowString.hh"
#include	"Texture.hh"

class	 MenuLoadGame
{
public:
  MenuLoadGame(Texture&, std::vector<std::string>&, std::vector<std::string>::iterator&);
  ~MenuLoadGame(void);
  void initialize();
  void update(gdl::GameClock const &, gdl::Input &);
  void draw(void);

  void      drawTitle();

  void    zoomTitle(float, float,
                    float, float,
                    float, float);
  std::vector<std::string>::iterator& getPosListGame() const;
  std::vector<std::string>&	      getListGame() const;

private:
  
  Texture			_texture;
  ShowString			*_showSave;
  float				 time_;
  std::vector<std::string>&	_listGame;
  std::vector<std::string>::iterator& _posListGame;

  float x1Title_;
  float x2Title_;
  float y1Title_;
  float y2Title_;
  float z1Title_;
  float z2Title_;

  float x1_;
  float x2_;
  float y1_;
  float y2_;
  float z1_;
  float z2_;
};

#endif
