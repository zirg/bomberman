//
// Ui.hh for  in /home/grizet_j//C++/bomberman/inc
// 
// Made by jean grizet
// Login   <grizet_j@epitech.net>
// 
// Started on  Fri Jun  7 08:53:00 2013 jean grizet
// Last update Fri Jun  7 10:35:13 2013 jean grizet
//

#ifndef __USER_INTERFACE_HH__
#define __USER_INTERFACE_HH__

#include <sstream>
#include <iostream>

#include "Image.hpp"
#include "Option.hh"
#include "Model.hh"
#include "Text.hpp"
#include "Color.hpp"

class	UserInterface
{
  Option&	_option;
  gdl::Image	*_ui;
public:
  UserInterface(Option&);
  ~UserInterface();

private:
  void	drawPlayer(const Model::Bomberman *, int, int);
public:
  void	draw();
  void	drawText(Model::Bomberman *j1,		\
		 Model::Bomberman *j2);
  void	drawMenu();
};



#endif
